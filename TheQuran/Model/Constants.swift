//
//  Constants.swift
//  TheQuran
//
//  Created by engitech on 05/03/2020.
//

import Foundation
import SystemConfiguration

class Constants {
    
     var siparayArray : [[String:Any]] = [
        ["number": "1", "SiparayEnglishName": "Alif Lam Meem", "SiparayNameArabic":"الم"],
        ["number": "2", "SiparayEnglishName": "Sayaqool", "SiparayNameArabic": "سَيَقُولُ"],             //2
        ["number": "3", "SiparayEnglishName": "Tilkal Rusull","SiparayNameArabic": "تِلْكَ الرُّسُلُ"] ,   //3
        ["number": "4", "SiparayEnglishName": "Lan Tana Loo", "SiparayNameArabic": "لَنْ تَنَالُوا"],     //4
        ["number": "5", "SiparayEnglishName": "Wal Mohsanat", "SiparayNameArabic": "وَالْمُحْصَنَاتُ"],     //5


            ["number": "6", "SiparayEnglishName": "La Yuhibbullah", "SiparayNameArabic": "لَا يُحِبُّ اللَّهُ"], //6
            ["number": "7", "SiparayEnglishName": "Wa Iza Samiu", "SiparayNameArabic": "وَإِذَا سَمِعُوا"] ,  //7
            ["number": "8", "SiparayEnglishName": "Wa Lau Annana", "SiparayNameArabic": "وَلَوْ أَنَّنَا"] ,  //8
            ["number": "9", "SiparayEnglishName": "Qalal Malao", "SiparayNameArabic": "قَالَ الْمَلَأُ"] ,    //9
            ["number" :"10", "SiparayEnglishName": "Wa A'lamu", "SiparayNameArabic": "وَاعْلَمُوا"] ,       //10


            ["number" :"11", "SiparayEnglishName": "Yatazeroon", "SiparayNameArabic": "يَعْتَذِرُون"]   ,       //11
            ["number" :"12", "SiparayEnglishName": "Wa Mamin Da'abat", "SiparayNameArabic": "وَمَا مِنْ دَابَّةٍ"],   //12
            ["number" :"13", "SiparayEnglishName": "Wa Ma Ubrioo", "SiparayNameArabic": "وَمَا أُبَرِّئُ"]   ,      //13
            ["number" :"14", "SiparayEnglishName": "Rubama", "SiparayNameArabic": "رُبَمَا"] ,                   //14
            ["number" :"15", "SiparayEnglishName": "Subhanallazi", "SiparayNameArabic": "سُبْحَانَ الَّذِي"],        //15


            ["number" :"16", "SiparayEnglishName": "Qal Alam", "SiparayNameArabic": "قَالَ أَلَمْ"] , //16
            ["number" :"17", "SiparayEnglishName": "Aqtarabo", "SiparayNameArabic": "اقْتَرَب"] , //17
            ["number" :"18", "SiparayEnglishName": "Qadd Aflaha", "SiparayNameArabic": "قَدْ أَفْلَح"] , //18
            ["number" :"19", "SiparayEnglishName": "Wa Qalallazina", "SiparayNameArabic": "وَقَالَ الَّذِينَ"],  //19
            ["number" :"20", "SiparayEnglishName": "A'man Khalaq", "SiparayNameArabic": "أَمَّنْ خَلَق"] , //20


            ["number" :"21", "SiparayEnglishName": "Utlu Ma Oohi", "SiparayNameArabic": "اتْلُ مَا أُوحِيَ"],  //21
            ["number" :"22", "SiparayEnglishName": "Wa Manyaqnut", "SiparayNameArabic": "وَمَنْ يَقْنُت"] , //22
            ["number" :"23", "SiparayEnglishName": "Wa Mali", "SiparayNameArabic": "وَمَا لِيَ"] , //23
            ["number" :"24", "SiparayEnglishName": "Faman Azlam", "SiparayNameArabic": "فَمَنْ أَظْلَم"],  //24
            ["number" :"25", "SiparayEnglishName": "Elahe Yuruddo", "SiparayNameArabic": "إِلَيْهِ يُرَدُّ"],  //25

            ["number" :"26", "SiparayEnglishName": "Ha'a Meem", "SiparayNameArabic": "حم"],  //26
            ["number" :"27", "SiparayEnglishName": "Qala Fama Khatbukum", "SiparayNameArabic": "قَالَ فَمَا خَطْبُكُمْ"],  //27
            ["number" :"28", "SiparayEnglishName": "Qadd Sami Allah", "SiparayNameArabic": "قَدْ سَمِعَ اللَّهُ"] , //28
            ["number" :"29", "SiparayEnglishName": "Tabarakallazi", "SiparayNameArabic": "تَبَارَكَ الَّذِي"],  //29
            ["number" :"30", "SiparayEnglishName": "Amma Yatasa'aloon", "SiparayNameArabic": "عَمَّ يَتَسَاءَلُون"]] //30
    
    
    
      var suratArray : [[String:Any]] = [
    
        /* 1*/   ["number" : "1", "suratName" : "Al-Fatihah",  "suratVerses" : "7 verses - Meccan"   , "suratMeaning" : "(the Opening)"],
        /* 2*/   ["number" : "2", "suratName" : "Al-Baqarah",  "suratVerses" : "286 verses - Medinan", "suratMeaning" : "(the Cow)"],
        /* 3*/   ["number" : "3", "suratName" : "Aali Imran",  "suratVerses" : "200 verses - Medinan", "suratMeaning" : "(the Family of Imran)"],
        /* 4*/   ["number" : "4", "suratName" : "An-Nisa’"  ,  "suratVerses" : "176 verses - Medinan", "suratMeaning" : "(the Women)"],
        /* 5*/   ["number" : "5", "suratName" : "Al-Ma’idah",  "suratVerses" : "120 verses - Medinan", "suratMeaning" : "(the Table)"],
    
        /* 6*/   ["number" : "6", "suratName" : "Al-An’am"  ,  "suratVerses" : "165 verses - Meccan" , "suratMeaning" : "(the Cattle)"],
        /* 7*/   ["number" : "7", "suratName" : "Al-A’raf"  ,  "suratVerses" : "206 verses - Meccan" , "suratMeaning" : "(the Heights)"],
        /* 8*/   ["number" : "8", "suratName" : "Al-Anfal"  ,  "suratVerses" : "75 verses - Medinan" , "suratMeaning" : "(the Spoils of War)"],
        /* 9*/   ["number" : "9", "suratName" : "At-Taubah" ,  "suratVerses" : "129 verses - Medinan", "suratMeaning" : "(the Repentance)"],
    
        /* 10*/  ["number" :"10", "suratName" : "Yunus"     , "suratVerses" :  "109 verses - Meccan"     , "suratMeaning" : "(Yunus)"],
        /* 11*/  ["number" :"11", "suratName" : "Hud"       , "suratVerses" :  "123 verses - Meccan"     , "suratMeaning" : "(Hud)"],
        /* 12*/  ["number" :"12", "suratName" : "Yusuf"     , "suratVerses" :  "111 verses - Meccan"     , "suratMeaning" : "(Yusuf)"],
        /* 13*/  ["number" :"13", "suratName" : "Ar-Ra’d"   , "suratVerses" :  "43 verses - Medinan"     , "suratMeaning" : "(the Thunder)"],
        /* 14*/  ["number" :"14", "suratName" : "Ibrahim"   , "suratVerses" :  "52 verses - Meccan"      , "suratMeaning" : "(Ibrahim)"],
        /* 15*/  ["number" :"15", "suratName" : "Al-Hijr"   , "suratVerses" :  "99 verses - Meccan"      , "suratMeaning" : "(the Rocky Tract)"],
        /* 16*/  ["number" :"16", "suratName" : "An-Nahl"   , "suratVerses" :  " verses - Meccan"        , "suratMeaning" : "(the Bees)"],
        /* 17*/  ["number" :"17", "suratName" : "Al-Isra’"  , "suratVerses" :  " verses - Meccan"        , "suratMeaning" : "(the Night Journey)"],
        /* 18*/  ["number" :"18", "suratName" : "Al-Kahf"   , "suratVerses" :  " verses - Meccan"        , "suratMeaning" : "(the Cave)"],
        /* 19*/  ["number" :"19", "suratName" : "Maryam"    , "suratVerses" :  " verses - Meccan"        , "suratMeaning" : "(Maryam)"],
    
    /* 20*/  ["number" :"20", "suratName" : "Ta-Ha", "suratVerses" : " verses - Meccan"           , "suratMeaning" : "(Ta-Ha)"],
    /* 21*/  ["number" :"21", "suratName" : "Al-Anbiya’", "suratVerses" :   " verses - Meccan"    , "suratMeaning" : "(the Prophets)"],
    /* 22*/  ["number" :"22", "suratName" : "Al-Haj", "suratVerses" :   " verses - Medinan"       , "suratMeaning" : "(the Pilgrimage)"],
    /* 23*/  ["number" :"23", "suratName" : "Al-Mu’minun", "suratVerses" :   " verses - Meccan"   , "suratMeaning" : "(the Believers)"],
    /* 24*/  ["number" :"24", "suratName" : "An-Nur", "suratVerses" :   " verses - Medinan"       , "suratMeaning" : "(the Light)"],
    /* 25*/  ["number" :"25", "suratName" : "Al-Furqan", "suratVerses" :   " verses - Meccan"     , "suratMeaning" : "(the Criterion)"],
    /* 26*/  ["number" :"26", "suratName" : "Ash-Shu’ara’", "suratVerses" :   " verses - Meccan"  , "suratMeaning" : "(the Poets)"],
    /* 27*/  ["number" :"27", "suratName" : "An-Naml", "suratVerses" :   " verses - Meccan"       , "suratMeaning" : "(the Ants)"],
    /* 28*/  ["number" :"28", "suratName" : "Al-Qasas", "suratVerses" :   " verses - Meccan"      , "suratMeaning" : "(the Stories)"],
    /* 29*/  ["number" :"29", "suratName" : "Al-Ankabut", "suratVerses" :   " verses - Meccan"    , "suratMeaning" : "(the Spider)"],
    
    /* 30*/  ["number" :"30", "suratName" : "Ar-Rum", "suratVerses" :   " verses - Meccan"        , "suratMeaning" : "(the Romans)"],
    /* 31*/  ["number" :"31", "suratName" : "Luqman", "suratVerses" :   " verses - Meccan"        , "suratMeaning" : "(Luqman)"],
    /* 32*/  ["number" :"32", "suratName" : "As-Sajdah", "suratVerses" :   " verses - Meccan"     , "suratMeaning" : "(the Prostration)"],
    /* 33*/  ["number" :"33", "suratName" : "Al-Ahzab", "suratVerses" :   " verses - Medinan"     , "suratMeaning" : "(the Combined Forces)"],
    /* 34*/  ["number" :"34", "suratName" : "Saba’", "suratVerses" :   " verses - Meccan"         , "suratMeaning" : "(the Sabeans)"],
    /* 35*/  ["number" :"35", "suratName" : "Al-Fatir", "suratVerses" :   " verses - Meccan"      , "suratMeaning" : "(the Originator)"],
    /* 36*/  ["number" :"36", "suratName" : "Ya-Sin", "suratVerses" :   " verses - Meccan"        , "suratMeaning" : "(Ya-Sin)"],
    /* 37*/  ["number" :"37", "suratName" : "As-Saffah", "suratVerses" :   " verses - Meccan"     , "suratMeaning" : "(Those Ranges in Ranks)"],
    /* 38*/  ["number" :"38", "suratName" : "Sad", "suratVerses" :   " verses - Meccan "          , "suratMeaning" : "(Sad)"],
    /* 39*/  ["number" :"39", "suratName" : "Az-Zumar", "suratVerses" :   " verses - Meccan"      , "suratMeaning" : "(the Groups)"],
    
    /* 40*/ ["number" :"40", "suratName" : "Ghafar", "suratVerses" :   " verses - Meccan"         , "suratMeaning" : "(the Forgiver)"],
    /* 41*/ ["number" :"41", "suratName" : "Fusilat", "suratVerses" :   " verses - Meccan"        , "suratMeaning" : "(Distinguished)"],
    /* 42*/ ["number" :"42", "suratName" : "Ash-Shura", "suratVerses" :   " verses - Meccan"      , "suratMeaning" : "(the Consultation)"],
    /* 43*/ ["number" :"43", "suratName" : "Az-Zukhruf", "suratVerses" :   " verses - Meccan"     , "suratMeaning" : "(the Gold)"],
    /* 44*/ ["number" :"44", "suratName" : "Ad-Dukhan", "suratVerses" :   " verses - Meccan"      , "suratMeaning" : "(the Smoke)"],
    /* 45*/ ["number" :"45", "suratName" : "Al-Jathiyah", "suratVerses" :   " verses - Meccan"    , "suratMeaning" : "(the Kneeling)"],
    /* 46*/ ["number" :"46", "suratName" : "Al-Ahqaf", "suratVerses" :   " verses - Meccan"       , "suratMeaning" : "(the Valley)"],
    /* 47*/ ["number" :"47", "suratName" : "Muhammad", "suratVerses" :   " verses - Medinan"      , "suratMeaning" : "(Muhammad)"],
    /* 48*/ ["number" :"48", "suratName" : "Al-Fat’h", "suratVerses" :   " verses - Medinan"      , "suratMeaning" : "(the Victory)"],
    /* 49*/ ["number" :"49", "suratName" : "Al-Hujurat", "suratVerses" :   " verses - Medinan"    , "suratMeaning" : "(the Dwellings)"],
    
    /* 50*/ ["number" :"50", "suratName" : "Qaf", "suratVerses" :   "60 verses - Meccan"          , "suratMeaning" : "(Qaf)"],
    /* 51*/ ["number" :"51", "suratName" : "Adz-Dzariyah", "suratVerses" :   "49 verses - Meccan" , "suratMeaning" : "(the Scatterers)"],
    /* 52*/ ["number" :"52", "suratName" : "At-Tur", "suratVerses" :   "62 verses - Meccan"       , "suratMeaning" : "(the Mount)"],
    /* 53*/ ["number" :"53", "suratName" : "An-Najm", "suratVerses" :   "55 verses - Meccan"      , "suratMeaning" : "(the Star)"],
    /* 54*/ ["number" :"54", "suratName" : "Al-Qamar", "suratVerses" :   "78 verses - Meccan"     , "suratMeaning" : "(the Moon)"],
    /* 55*/ ["number" :"55", "suratName" : "Ar-Rahman", "suratVerses" :   "96 verses - Medinan"   , "suratMeaning" : "(the Most Gracious)"],
    /* 56*/ ["number" :"56", "suratName" : "Al-Waqi’ah", "suratVerses" :   "29 verses - Meccan"   , "suratMeaning" : "(the Event)"],
    /* 57*/ ["number" :"57", "suratName" : "Al-Hadid", "suratVerses" :   " verses - Medinan"      , "suratMeaning" : "(the Iron)"],
    /* 58*/ ["number" :"58", "suratName" : "Al-Mujadilah", "suratVerses" :   "22 verses - Medinan", "suratMeaning" : "(the Reasoning)"],
    /* 59*/ ["number" :"59", "suratName" : "Al-Hashr",  "suratVerses" :  "24 verses - Medinan"    , "suratMeaning" : "(the Gathering)"],
    
    
    /* 60*/ ["number" :"60", "suratName" : "Al-Mumtahanah", "suratVerses" : "13 verses - Medinan" , "suratMeaning" : "(the Tested)"],
    /* 61*/ ["number" :"61", "suratName" : "As-Saf", "suratVerses" :   " verses - Medinan"        , "suratMeaning" : "(the Row)"],
    /* 62*/ ["number" :"62", "suratName" : "Al-Jum’ah", "suratVerses" :   " verses - Medinan"     , "suratMeaning" : "(Friday)"],
    /* 63*/ ["number" :"63", "suratName" : "Al-Munafiqun", "suratVerses" :   " verses - Medinan"  , "suratMeaning" : "(the Hypocrites)"],
    /* 64*/ ["number" :"64", "suratName" : "At-Taghabun", "suratVerses" :   " verses - Medinan"   , "suratMeaning" : "(the Loss & Gain)"],
    /* 65*/ ["number" :"65", "suratName" : "At-Talaq", "suratVerses" :   " verses - Medinan"      , "suratMeaning" : "(the Divorce)"],
    /* 66*/ ["number" :"66", "suratName" : "At-Tahrim", "suratVerses" :   " verses - Medinan"     , "suratMeaning" : "(the Prohibition)"],
    /* 67*/ ["number" :"67", "suratName" : "Al-Mulk", "suratVerses" :   " verses - Meccan"        , "suratMeaning" : "(the Kingdom)"],
    /* 68*/ ["number" :"68", "suratName" : "Al-Qalam", "suratVerses" :   " verses - Meccan"       , "suratMeaning" : "(the Pen)"],
    /* 69*/ ["number" :"69", "suratName" : "Al-Haqqah", "suratVerses" :   " verses - Meccan"      , "suratMeaning" : "(the Inevitable)"],
    
    
    /* 70*/ ["number" :"70", "suratName" : "Al-Ma’arij", "suratVerses" :   " verses - Meccan"     , "suratMeaning" : "(the Elevated Passages)"],
    /* 71*/ ["number" :"71", "suratName" : "Nuh", "suratVerses" :   " verses - Meccan"            , "suratMeaning" : "(Nuh)"],
    /* 72*/ ["number" :"72", "suratName" : "Al-Jinn", "suratVerses" :   " verses - Meccan"        , "suratMeaning" : "(the Jinn)"],
    /* 73*/ ["number" :"73", "suratName" : "Al-Muzammil", "suratVerses" :   " verses - Meccan"    , "suratMeaning" : "(the Wrapped)"],
    /* 74*/ ["number" :"74", "suratName" : "Al-Mudaththir", "suratVerses" :   " verses - Meccan"  , "suratMeaning" : "(the Cloaked)"],
    /* 75*/ ["number" :"75", "suratName" : "Al-Qiyamah", "suratVerses" :   " verses - Meccan"     , "suratMeaning" : "(the Resurrection)"],
    /* 76*/ ["number" :"76", "suratName" : "Al-Insan", "suratVerses" : " verses - Medinan"        , "suratMeaning" : "(the Human)"],
    /* 77*/ ["number" :"77", "suratName" : "Al-Mursalat", "suratVerses" :   " verses - Meccan"    , "suratMeaning" : "(Those Sent Forth)"],
    /* 78*/ ["number" :"78", "suratName" : "An-Naba’", "suratVerses" :   " verses - Meccan"       , "suratMeaning" : "(the Great News)"],
    /* 79*/ ["number" :"79", "suratName" : "An-Nazi’at", "suratVerses" :   " verses - Meccan"     , "suratMeaning" : "(Those Who Pull Out)"],
    
    /* 80*/ ["number" :"80", "suratName" : "‘Abasa", "suratVerses" :   " verses - Meccan"        , "suratMeaning" : "(He Frowned)"],
    /* 81*/ ["number" :"81", "suratName" : "At-Takwir", "suratVerses" :   " verses - Meccan"     , "suratMeaning" : "(the Overthrowing)"],
    /* 82*/ ["number" :"82", "suratName" : "Al-Infitar", "suratVerses" :   " verses - Meccan"    , "suratMeaning" : "(the Cleaving)"],
    /* 83*/ ["number" :"83", "suratName" : "Al-Mutaffifin", "suratVerses" :   " verses - Meccan" , "suratMeaning" : "(Those Who Deal in Fraud)"],
    /* 84*/ ["number" :"84", "suratName" : "Al-Inshiqaq", "suratVerses" :   " verses - Meccan"   , "suratMeaning" : "(the Splitting Asunder)"],
    /* 85*/ ["number" :"85", "suratName" : "Al-Buruj", "suratVerses" :   " verses - Meccan"      , "suratMeaning" : "(the Stars)"],
    /* 86*/ ["number" :"86", "suratName" : "At-Tariq", "suratVerses" :   " verses - Meccan"      , "suratMeaning" : "(the Nightcomer)"],
    /* 87*/ ["number" :"87", "suratName" : "Al-A’la", "suratVerses" :   " verses - Meccan"       , "suratMeaning" : "(the Most High)"],
    /* 88*/ ["number" :"88", "suratName" : "Al-Ghashiyah", "suratVerses" :   " verses - Meccan"  , "suratMeaning" : "(the Overwhelming)"],
    /* 89*/ ["number" :"89", "suratName" : "Al-Fajr", "suratVerses" :   " verses - Meccan"       , "suratMeaning" : "(the Dawn)"],
    
    
    /* 90*/ ["number" :"90", "suratName" : "Al-Balad", "suratVerses" :   " verses - Meccan"      , "suratMeaning" : "(the City)"],
    /* 91*/ ["number" :"91", "suratName" : "Ash-Shams", "suratVerses" :   " verses - Meccan"     , "suratMeaning" : "(the Sun)"],
    /* 92*/ ["number" :"92", "suratName" : "Al-Layl", "suratVerses" :   " verses - Meccan"       , "suratMeaning" : "(the Night)"],
    /* 93*/ ["number" :"93", "suratName" : "Adh-Dhuha", "suratVerses" :   " verses - Meccan"     , "suratMeaning" : "(the Forenoon)"],
    /* 94*/ ["number" :"94", "suratName" : "Al-Inshirah", "suratVerses" :   " verses - Meccan"   , "suratMeaning" : "(the Opening Forth)"],
    /* 95*/ ["number" :"95", "suratName" : "At-Tin", "suratVerses" :   " verses - Meccan"        , "suratMeaning" : "(the Fig)"],
    /* 96*/ ["number" :"96", "suratName" : "Al-‘Alaq", "suratVerses" :   " verses - Meccan"      , "suratMeaning" : "(the Clot)"],
    /* 97*/ ["number" :"97", "suratName" : "Al-Qadar", "suratVerses" :   " verses - Meccan"      , "suratMeaning" : "(the Night of Decree)"],
    /* 98*/ ["number" :"98", "suratName" : "Al-Bayinah", "suratVerses" :   " verses - Medinan"   , "suratMeaning" : "(the Proof)"],
    /* 99*/ ["number" :"99", "suratName" : "Az-Zalzalah", "suratVerses" :   " verses - Medinan"  , "suratMeaning" : "(the Earthquake)"],
    
    /* 100*/["number" :"100", "suratName" : "Al-‘Adiyah", "suratVerses" :   " verses - Meccan"   , "suratMeaning" : "(the Runners)"],
    
    /* 101*/["number" :"101", "suratName" : "Al-Qari’ah", "suratVerses" :   " verses - Meccan"   , "suratMeaning" : "(the Striking Hour)"],
    /* 102*/["number" :"102", "suratName" : "At-Takathur", "suratVerses" :   " verses - Meccan"  , "suratMeaning" : "(the Piling Up)"],
    /* 103*/["number" :"103", "suratName" : "Al-‘Asr", "suratVerses" :   " verses - Meccan"      , "suratMeaning" : "(the Time)"],
    /* 104*/["number" :"104", "suratName" : "Al-Humazah", "suratVerses" :   " verses - Meccan"   , "suratMeaning" : "(the Slanderer)"],
    /* 105*/["number" :"105", "suratName" : "Al-Fil", "suratVerses" :   " verses - Meccan"       , "suratMeaning" : "(the Elephant)"],
    /* 106*/["number" :"106", "suratName" : "Quraish", "suratVerses" :   " verses - Meccan"      , "suratMeaning" : "(Quraish)"],
    /* 107*/["number" :"107", "suratName" : "Al-Ma’un", "suratVerses" :   " verses - Meccan"     , "suratMeaning" : "(the Assistance)"],
    /* 108*/["number" :"108", "suratName" : "Al-Kauthar", "suratVerses" :   " verses - Meccan"   , "suratMeaning" : "(the River of Abundance)"],
    /* 109*/["number" :"109", "suratName" : "Al-Kafirun", "suratVerses" :   " verses - Meccan"   , "suratMeaning" : "(the Disbelievers)"],
    
    /* 110*/["number" :"110", "suratName" : "An-Nasr", "suratVerses" :   " verses - Medinan"     , "suratMeaning" : "(the Help)"],
    /* 111*/["number" :"111", "suratName" : "Al-Masad", "suratVerses" :   " verses - Medinan"    , "suratMeaning" : "(the Palm Fiber)"],
    /* 112*/["number" :"112", "suratName" : "Al-Ikhlas", "suratVerses" :   " verses - Meccan"    , "suratMeaning" : "(the Sincerity)"],
    /* 113*/["number" :"113", "suratName" : "Al-Falaq", "suratVerses" :   " verses - Meccan"     , "suratMeaning" : "(the Daybreak)"],
    /* 114*/["number" :"114", "suratName" : "An-Nas", "suratVerses" :   " verses - Meccan"       , "suratMeaning" : "(Mankind)"]]
    

    

}


public class Reachability {
    public func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)

        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }

        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        if flags.isEmpty {
            return false
        }

        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)

        print(isReachable)
        print(needsConnection)
        
        
        return (isReachable && !needsConnection)
    }
    
    func isInternetAvailable() -> Bool
       {
           var zeroAddress = sockaddr_in()
           zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
           zeroAddress.sin_family = sa_family_t(AF_INET)
           
           let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
               $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                   SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
               }
           }
           
           var flags = SCNetworkReachabilityFlags()
           if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
               return false
           }
           let isReachable = flags.contains(.reachable)
           let needsConnection = flags.contains(.connectionRequired)
           return (isReachable && !needsConnection)
       }
    
    
    
    
    
}
