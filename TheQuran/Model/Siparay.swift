//
//  Siparay.swift
//  TheQuran
//
//  Created by engitech on 05/03/2020.
//

import Foundation

struct Siparay {
    
    var number : String
    var SiparayEnglishName : String
    var SiparayNameArabic : String
    
    init(number :String, EnglishName : String, ArabicName: String) {
        self.number = number
        self.SiparayEnglishName = EnglishName
        self.SiparayNameArabic = ArabicName
    }
}
