//
//  SuratTableViewCell.swift
//  TheQuran
//
//  Created by haya on 22/03/2020.
//

import UIKit

class SuratTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    @IBOutlet weak var btnAudio: UIButton!
    

      @IBOutlet weak var numberLabel: UILabel!
      
      @IBOutlet weak var suratNameLabel: UILabel!
      
      @IBOutlet weak var suratVersesLabel: UILabel!
      
      @IBOutlet weak var suratMeaningLabel: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
