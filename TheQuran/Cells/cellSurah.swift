//
//  cellSurah.swift
//  TheQuran
//
//  Created by haya on 24/03/2020.
//

import UIKit

class cellSurah: UITableViewCell {
    @IBOutlet weak var lblArabic: UILabel!
    
    @IBOutlet weak var btnBookMark: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var lblEng: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
