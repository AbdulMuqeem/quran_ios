//
//  BookmarkTableViewCell.swift
//  TheQuran
//
//  Created by engitech on 05/04/2020.
//

import UIKit

class BookmarkTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var bookMarkName: UILabel!
    
    @IBOutlet weak var ayatLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

  
}
