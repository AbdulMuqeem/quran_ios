//
//  SearchViewController.swift
//  TheQuran
//
//  Created by engitech on 04/03/2020.
//

import UIKit
import XLPagerTabStrip
import iProgressHUD
class SearchViewController: UIViewController,UITableViewDataSource,UITableViewDelegate ,UISearchBarDelegate,UITextFieldDelegate{
  let iprogress: iProgressHUD = iProgressHUD()
    var isSearching : Bool = false
    @IBOutlet weak var txtSearch: UITextField!
    
    var SearchNewArray = [Search]()
       var newArar = [Search]()

    var surahArrA : [String] = []
    var surahArrE : NSMutableArray!
    var searchArr = [Search]()
    var surahArrC : [String] = []

    @IBOutlet weak var tblSearch: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearch.delegate = self
        tblSearch.delegate = self
        tblSearch.dataSource = self
        tblSearch.register(UINib(nibName: "cellSurah", bundle: nil), forCellReuseIdentifier: "cellSurah")
        


    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        isSearching = true
//        searchArr = surahArrE as! [Search]
//        searchArr = surahArrE
//        tblSearch.reloadData()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print(surahArrE)
    searchArr = []
        for i in newArar{
            print(i.strEng)
//            let dict = i as! String
            if i.strEng.contains(txtSearch.text!){
                searchArr.append(i)
//                 searchArr.add(i)
                //
                                txtSearch.resignFirstResponder()
                                tblSearch.reloadData()
                 
            }
            else{
                txtSearch.resignFirstResponder()
            }
            
            
        }

        return true
        }
   
    private func setSearch(){
        
    }
  
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
       
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            return searchArr.count
        }
        else{
            return newArar.count

        }
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                      let cell = tableView.dequeueReusableCell(withIdentifier: "cellSurah", for: indexPath) as! cellSurah
        if isSearching{
            let b = searchArr[indexPath.row]
            cell.lblEng.text = b.strEng
            cell.lblArabic.text = b.strArb
            return cell
//        cell.lblEng.text = b.strEng
//            cell.lblArabic.text = b.strArb
        }
        else{
            let b = newArar[indexPath.row]
            cell.lblEng.text = b.strEng
            cell.lblArabic.text = b.strArb
//                   cell.lblEng.text  = String(describing: b).replacingOccurrences(of: "[", with: "").replacingOccurrences(of: "]", with: "")
            
            return cell
                              
        }
//        let a = surahArrA[indexPath.row] as! [Any]
//                   cell.lblArabic.text  = String(describing: a).replacingOccurrences(of: "[", with: "").replacingOccurrences(of: "]", with: "")
       
return  cell
      }
    override func viewWillAppear(_ animated: Bool) {
        surahArrE = []
        surahArrA = []
        setData()
        
    }
       func setData(){
               
      if let path = Bundle.main.path(forResource: "quran-simple-enhanced ", ofType: "txt") {
        
         if let path1 = Bundle.main.path(forResource: "en.wahiduddin", ofType: "txt") {
            do {
             let dataE = try String(contentsOfFile: path1, encoding: .utf8)

            let myStringsE = dataE.components(separatedBy: .newlines)
                
                
                
                let dataA = try String(contentsOfFile: path, encoding: .utf8)
                var myStringsA = dataA.components(separatedBy: "\r")
              
                for i in 0..<myStringsE.count{
                    var  newArr = Search(strEng: "", strArb: "")
                    var search = Search(strEng: "", strArb: "")
                    let se = myStringsE[i].split(separator: "|")
                    print(se)
                    let seA = myStringsA[i].split(separator: "|")
                                       print(seA)
                   
                    if seA == ["\n"]{
                          newArr = Search(strEng: "", strArb: myStringsA[i])
                        
                    }
                    else{
//                        if se[1] == seA[1]{
                                                newArr = Search(strEng: myStringsE[i], strArb: myStringsA[i])
                                               
                            newArar.append(newArr)
//                                           }
                    }

                    if myStringsA[i].isEmpty{

//                        search = Search(strEng: myStringsE[i], strArb: myStringsA[i + 1])
//
                    }
//                    if myStringsA[i].isEmpty {
//                        search = Search(strEng: " ", strArb: myStringsA[i])
////                                               SearchNewArray.append(search)"
//                    }
                    else{
                        search = Search(strEng: myStringsE[i], strArb: myStringsA[i])
                      
                    }
                    
                    SearchNewArray.append(search)
                }
                  
                print(newArar)

                print(SearchNewArray)
            }
            
            catch{
                
            }
               

//        print(SearchNewArray[6])

                }
//
//            if let path = Bundle.main.path(forResource: "en.wahiduddin", ofType: "txt") {
//                         do {
//                             let data = try String(contentsOfFile: path, encoding: .utf8)
//
//                            let myStrings = data.components(separatedBy: .newlines)
//
//                          //  print(myStrings)
//
//
//                                      for  i in myStrings{
//                                        print(i)
//                                         // print(myStrings[startInd])
//                                          if i.isEmpty{
//                                              print("gap")
//                                          }
//                                          else{
//
////                                            let newObj = i.split(separator: ",")
//                                            surahArrE.add(i)
////                                              print(newObj)
//
//
//
//                                          }}
//
////                            searchArr = surahArrE
//
//                         } catch {
//                             print(error)
//                         }
//                print(surahArrE.count)
//
//                        }
        
//        for i in 0..<surahArrE.count {
//
//            let search = Search(strEng: surahArrE![i] as! String, strArb: surahArrA[i])
//
//            SearchNewArray.append(search)
//   print(SearchNewArray)
////            if let index = surahArrA.index(of: "gap" ) {
////                SearchNewArray.remove(at: index)
////            }
////            print(surahArrA) // ["cats", "dogs", "moose"]
//
//       //     let abc = SearchNewArray.filter { $0 != "gap" }
//
//
//           // var number = Number(A: arrayA[i], B: arrayB[i])
//           // newArray.append(number)
//        }
//
//
//        print(SearchNewArray)
//        }
        }}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func showSpinner(view : UIView) {
       
        
        
        iprogress.isShowModal = true
        iprogress.isShowCaption = true
        iprogress.isTouchDismiss = false
        iprogress.indicatorStyle = .circleStrokeSpin
        iprogress.indicatorColor = .white
        iprogress.iprogressStyle = .horizontal
        iprogress.indicatorView.startAnimating()
        iprogress.attachProgress(toView: view)
        view.showProgress()
        
    }
    func hideSpinner(view : UIView) {
        iprogress.indicatorView.stopAnimating()
        view.dismissProgress()
    }
}

extension SearchViewController: IndicatorInfoProvider{
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "SEARCH")
    }
    
    
}
struct  Search {
    var strEng:String
    var strArb:String
    
}
