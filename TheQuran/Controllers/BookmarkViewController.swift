//
//  BookmarkViewController.swift
//  TheQuran
//
//  Created by engitech on 05/04/2020.
//

import UIKit
import CoreData

class BookmarkViewController: UIViewController {

    @IBOutlet weak var bookMarkTableView: UITableView!
    //var bookMarkArray = [Any]()
    var item = String()
    
    var bookmarkArray: [NSManagedObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.bookMarkTableView.delegate = self
        self.bookMarkTableView.dataSource = self
        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool){
        
        retrieveData()
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension BookmarkViewController : UITableViewDelegate,UITableViewDataSource{
     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
          let date = bookmarkArray[indexPath.row]
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }

                 let context = appDelegate.persistentContainer.viewContext
                 let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Bookmarks")
                     
                     let managedContext = appDelegate.persistentContainer.viewContext
                     
               if editingStyle == .delete {
                      let commit = bookmarkArray[indexPath.row]
                      context.delete(commit)
                      bookmarkArray.remove(at: indexPath.row)
                      tableView.deleteRows(at: [indexPath], with: .fade)
                do {
                             try managedContext.save()
                             
                            
                             //  bookmark.append(person)
                         } catch let error as NSError {
                             print("Could not save. \(error), \(error.userInfo)")
                         }
                         //                      createData(name: <#T##String#>, ayat: <#T##String#>)()
                  }
          
//            bookmarkArray.remove(at: indexPath.row)
            
//            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookmarkArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let vc = self.storyboard?.instantiateViewController(withIdentifier: "specSuratVC") as! specSuratViewController
        
        
          vc.modalPresentationStyle = .fullScreen
        
        vc.fromBookmark = true
        
         let bookmark = bookmarkArray[indexPath.row]
        
        vc.index = bookmark.value(forKey: "tag") as! Int
        
        if bookmark.value(forKey: "fromVC") as! String == "Surat" {
            vc.fromVC = "Surat"
            
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "001"{
                        
                  
                    vc.ayatCount = 7
                    vc.suratNumber = "001"
                    vc.startInd = 0
                    vc.endInd = 6
                    vc.current = 1
                  
                    //            vc.startIndexForEnglish = 0
                    //            vc.endIndexForEnglish = 6
                    
                }
              if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "002"{
                        
                    vc.ayatCount = 286
                    vc.suratNumber = "002"
                    vc.current = 2
                    vc.startInd = 7
                    vc.endInd = 293
                    
                    //            vc.startIndexForEnglish = 6
                    //            vc.endIndexForEnglish = 293
                    
                    
                }
              if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "003"{
                        
                        vc.ayatCount = 200
                        vc.suratNumber = "003"
                        vc.current = 3
                        vc.startInd = 294
                        vc.endInd = 494
                  
                  
                  
                        
                  
                 
                        //            vc.startIndexForEnglish = 295
                        //            vc.endIndexForEnglish = 496
                        
              }
              if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "004"{
                        
                    
                    vc.ayatCount = 176
                    vc.suratNumber = "004"
                    vc.current = 4
                    
                    vc.startInd = 495
                    vc.endInd = 671
                    
                    //            vc.startIndexForEnglish = 497
                    //            vc.endIndexForEnglish = 674
                    
                }
                if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "005"{
                    
                    vc.ayatCount = 120
                    vc.suratNumber = "005"
                    vc.current = 5
                    
                    vc.startInd = 672
                    vc.endInd = 792
                    
                    //            vc.startIndexForEnglish = 675
                    //            vc.endIndexForEnglish = 797
                    
                }
                    
                
                if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "006" {
                    
                    vc.ayatCount = 165
                    vc.suratNumber = "006"
                    vc.current = 6
                    
                    vc.startInd = 793
                    vc.endInd = 958
                    
                    //            vc.startIndexForEnglish = 798
                    //            vc.endIndexForEnglish = 963
                }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "007" {
                
                vc.ayatCount = 206
                vc.suratNumber = "007"
                vc.current = 7
                
                vc.startInd = 959
                vc.endInd = 1165
                
                //            vc.startIndexForEnglish = 964
                //            vc.endIndexForEnglish = 1171
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "008" {
                
                
                vc.ayatCount = 75
                vc.suratNumber = "008"
                vc.current = 8
                
                vc.startInd = 1166
                vc.endInd = 1241
                
                //            vc.startIndexForEnglish = 1172
                //            vc.endIndexForEnglish = 1248
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "009" {
                
                vc.ayatCount = 129
                vc.suratNumber = "009"
                vc.current = 9
                
                vc.startInd = 1242
                vc.endInd = 1370
                
                //            vc.startIndexForEnglish = 1249
                //            vc.endIndexForEnglish = 1378
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "010" {
                
                vc.current = 10
                
                vc.ayatCount = 109
                vc.suratNumber = "010"
                
                vc.startInd = 1371
                vc.endInd = 1480
                
                //            vc.startIndexForEnglish = 1379
                //            vc.endIndexForEnglish = 1489
                
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "011" {
                vc.current = 11
                
                vc.ayatCount = 123
                vc.suratNumber = "011"
                
                vc.startInd = 1481
                vc.endInd = 1604
                
                //            vc.startIndexForEnglish = 1490
                //            vc.endIndexForEnglish = 1614
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "012" {
                vc.current = 12
                
                vc.ayatCount = 111
                vc.suratNumber = "012"
                
                vc.startInd = 1605
                vc.endInd = 1716
                
                //            vc.startIndexForEnglish = 1615
                //            vc.endIndexForEnglish = 1727
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "013" {
                
                vc.current = 13
                
                vc.ayatCount = 43
                vc.suratNumber = "013"
                
                vc.startInd = 3436
                vc.endInd = 3524
                
                //            vc.startIndexForEnglish = 1717
                //            vc.endIndexForEnglish = 1760
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "014" {
                vc.current = 14
                
                vc.ayatCount = 52
                vc.suratNumber = "014"
                
                vc.startInd = 1761
                vc.endInd = 1813
                
                //            vc.startIndexForEnglish = 1773
                //            vc.endIndexForEnglish = 1826
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "015" {
                vc.current = 15
                
                vc.ayatCount = 99
                vc.suratNumber = "015"
                
                vc.startInd = 1814
                vc.endInd = 1913
                
                //            vc.startIndexForEnglish = 1827
                //            vc.endIndexForEnglish = 1927
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "016" {
                vc.current = 16
                
                vc.ayatCount = 128
                vc.suratNumber = "016"
                
                vc.startInd = 1914
                vc.endInd = 2042
                
                //            vc.startIndexForEnglish = 1928
                //            vc.endIndexForEnglish = 2057
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "017" {
                
                vc.current = 17
                
                vc.ayatCount = 111
                vc.suratNumber = "017"
                
                vc.startInd = 2043
                vc.endInd = 2154
                
                //            vc.startIndexForEnglish = 2058
                //            vc.endIndexForEnglish = 2170
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "018" {
                
                vc.ayatCount = 110
                vc.suratNumber = "018"
                vc.current = 18
                
                vc.startInd = 2155
                vc.endInd = 2265
                
                //            vc.startIndexForEnglish = 2171
                //            vc.endIndexForEnglish = 2282
                //
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "019" {
                vc.current = 19
                
                
                vc.ayatCount = 98
                vc.suratNumber = "019"
                
                vc.startInd = 2266
                vc.endInd = 2364
                
                //            vc.startIndexForEnglish = 2283
                //            vc.endIndexForEnglish = 2382
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "020" {
                
                vc.current = 20
                
                vc.ayatCount = 135
                vc.suratNumber = "020"
                
                vc.startInd = 2365
                vc.endInd = 2500
                
                //            vc.startIndexForEnglish = 2383
                //            vc.endIndexForEnglish = 2519
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "021" {
                vc.current = 21
                
                vc.ayatCount = 112
                vc.suratNumber = "021"
                
                vc.startInd = 2501
                vc.endInd = 2613
                
                //            vc.startIndexForEnglish = 2520
                //            vc.endIndexForEnglish = 2633
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "022" {
                vc.current = 22
                
                vc.ayatCount = 78
                vc.suratNumber = "022"
                
                vc.startInd = 2614
                vc.endInd = 2692
                
                //            vc.startIndexForEnglish = 2634
                //            vc.endIndexForEnglish = 2713
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "023" {
                
                vc.ayatCount = 118
                vc.suratNumber = "023"
                vc.current = 23
                
                vc.startInd = 2693
                vc.endInd = 2811
                
                //            vc.startIndexForEnglish = 2714
                //            vc.endIndexForEnglish = 2833
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "024" {
                
                vc.ayatCount = 64
                vc.suratNumber = "024"
                vc.current = 24
                
                vc.startInd = 2812
                vc.endInd = 2876
                
                //            vc.startIndexForEnglish = 2834
                //            vc.endIndexForEnglish = 2899
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "025" {
                
                vc.ayatCount = 77
                vc.suratNumber = "025"
                vc.current = 25
                
                vc.startInd = 2877
                vc.endInd = 2954
                
                //            vc.startIndexForEnglish = 2900
                //            vc.endIndexForEnglish = 2978
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "026" {
                vc.current = 26
                
                vc.ayatCount = 227
                vc.suratNumber = "026"
                
                vc.startInd = 2955
                vc.endInd = 3182
                
                //            vc.startIndexForEnglish = 2979
                //            vc.endIndexForEnglish = 3207
                //
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "027" {
                
                vc.current = 27
                
                vc.ayatCount = 93
                vc.suratNumber = "027"
                
                
                vc.startInd = 3183
                vc.endInd = 3276
                
                //            vc.startIndexForEnglish = 3208
                //            vc.endIndexForEnglish = 3302
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "028" {
                vc.current = 28
                
                vc.ayatCount = 88
                vc.suratNumber = "028"
                
                vc.startInd = 3277
                vc.endInd = 3365
                
                //            vc.startIndexForEnglish = 3303
                //            vc.endIndexForEnglish = 3392
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "029" {
                vc.current = 29
                
                vc.ayatCount = 69
                vc.suratNumber = "029"
                
                vc.startInd = 3366
                vc.endInd = 3435
                
                //            vc.startIndexForEnglish = 3393
                //            vc.endIndexForEnglish = 3463
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "030" {
                
                vc.current = 30
                
                vc.ayatCount = 60
                vc.suratNumber = "030"
                vc.startInd = 3436
                vc.endInd = 3496
                
                //            vc.startIndexForEnglish = 3464
                //            vc.endIndexForEnglish = 3525
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "031" {
                vc.current = 31
                
                vc.ayatCount = 34
                vc.suratNumber = "031"
                
                vc.startInd = 3497
                vc.endInd = 3531
                
                //            vc.startIndexForEnglish = 3526
                //            vc.endIndexForEnglish = 3561
                
            }
            
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "032" {
                vc.current = 32
                
                vc.ayatCount = 30
                vc.suratNumber = "032"
                
                vc.startInd = 3532
                vc.endInd = 3562
                
                //            vc.startIndexForEnglish = 3562
                //            vc.endIndexForEnglish = 3593
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "033" {
                vc.current = 33
                
                vc.ayatCount = 73
                vc.suratNumber = "033"
                
                vc.startInd = 3563
                vc.endInd = 3636
                
                //            vc.startIndexForEnglish = 3594
                //            vc.endIndexForEnglish = 3668
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "034" {
                vc.current = 34
                
                vc.ayatCount = 54
                vc.suratNumber = "034"
                
                vc.startInd = 3637
                vc.endInd = 3691
                
                //            vc.startIndexForEnglish = 3669
                //            vc.endIndexForEnglish = 3724
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "035" {
                vc.current = 35
                
                vc.ayatCount = 45
                vc.suratNumber = "035"
                
                vc.startInd = 3692
                vc.endInd = 3737
                
                //            vc.startIndexForEnglish = 3725
                //            vc.endIndexForEnglish = 3771
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "036" {
                vc.current = 36
                
                vc.ayatCount = 83
                vc.suratNumber = "036"
                
                vc.startInd = 3738
                vc.endInd = 3821
                
                //            vc.startIndexForEnglish = 3772
                //            vc.endIndexForEnglish = 3856
                //
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "037"{
                vc.current = indexPath.row + 1
                vc.ayatCount = 182
                vc.suratNumber = "037"
                
                vc.startInd = 3822
                vc.endInd = 4004
                
                //            vc.startIndexForEnglish = 3857
                //            vc.endIndexForEnglish = 4040
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "038" {
                vc.current = indexPath.row + 1
                
                
                vc.ayatCount = 88
                vc.suratNumber = "038"
                vc.startInd = 4005
                vc.endInd = 4093
                
                //            vc.startIndexForEnglish = 4041
                //            vc.endIndexForEnglish = 4130
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "039" {
                
                vc.ayatCount = 75
                vc.current = indexPath.row + 1
                
                vc.suratNumber = "039"
                
                vc.startInd = 4094
                vc.endInd = 4169
                
                //            vc.startIndexForEnglish = 4131
                //            vc.endIndexForEnglish = 4207
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "040" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 85
                vc.suratNumber = "040"
                
                vc.startInd = 4170
                vc.endInd = 4255
                
                //            vc.startIndexForEnglish = 4208
                //            vc.endIndexForEnglish = 4294
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "041" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 54
                vc.suratNumber = "041"
                
                vc.startInd = 4256
                vc.endInd = 4310
                
                //            vc.startIndexForEnglish = 4295
                //            vc.endIndexForEnglish = 4350
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "042" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 53
                vc.suratNumber = "042"
                
                vc.startInd = 4311
                vc.endInd = 4364
                //
                //            vc.startIndexForEnglish = 4351
                //            vc.endIndexForEnglish = 4405
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "043" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 89
                vc.suratNumber = "043"
                vc.startInd = 4365
                vc.endInd = 4454
                
                //            vc.startIndexForEnglish = 4406
                //            vc.endIndexForEnglish = 4496
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "044" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 59
                vc.suratNumber = "044"
                
                vc.startInd = 4455
                vc.endInd = 4514
                
                //            vc.startIndexForEnglish = 4497
                //            vc.endIndexForEnglish = 4557
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "045" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 37
                vc.suratNumber = "045"
                
                vc.startInd = 4515
                vc.endInd = 4552
                
                //            vc.startIndexForEnglish = 4558
                //            vc.endIndexForEnglish = 4596
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "046" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 35
                vc.suratNumber = "046"
                
                vc.startInd = 4553
                vc.endInd = 4588
                
                //            vc.startIndexForEnglish = 4597
                //            vc.endIndexForEnglish = 4633
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "047"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 38
                vc.suratNumber = "047"
                
                vc.startInd = 4589
                vc.endInd = 4627
                
                //            vc.startIndexForEnglish = 4634
                //            vc.endIndexForEnglish = 4673
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "048" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 29
                vc.suratNumber = "048"
                
                vc.startInd = 4628
                vc.endInd = 4657
                
                //            vc.startIndexForEnglish = 4674
                //            vc.endIndexForEnglish = 4704
                //
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "049" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 18
                vc.suratNumber = "049"
                vc.startInd = 4658
                vc.endInd = 4676
                
                //            vc.startIndexForEnglish = 4705
                //            vc.endIndexForEnglish = 4724
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "050" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 45
                vc.suratNumber = "050"
                
                vc.startInd = 4677
                vc.endInd = 4722
                
                //            vc.startIndexForEnglish = 4725
                //            vc.endIndexForEnglish = 4771
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "051"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 60
                vc.suratNumber = "051"
                
                vc.startInd = 4723
                vc.endInd = 4783
                
                //            vc.startIndexForEnglish = 4772
                //            vc.endIndexForEnglish = 4833
                //
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "052" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 49
                vc.suratNumber = "052"
                
                vc.startInd = 4784
                vc.endInd = 4833
                
                //            vc.startIndexForEnglish = 4834
                //            vc.endIndexForEnglish = 4884
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "053" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 62
                vc.suratNumber = "053"
                
                vc.startInd = 4834
                vc.endInd = 4896
                
                //            vc.startIndexForEnglish = 4885
                //            vc.endIndexForEnglish = 4948
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "054" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 55
                vc.suratNumber = "054"
                
                vc.startInd = 4897
                vc.endInd = 4952
                
                //            vc.startIndexForEnglish = 4949
                //            vc.endIndexForEnglish = 5005
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "055" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 78
                vc.suratNumber = "055"
                
                vc.startInd = 4953
                vc.endInd = 5031
                //
                //            vc.startIndexForEnglish = 5006
                //            vc.endIndexForEnglish = 5085
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "056" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 96
                vc.suratNumber = "056"
                
                vc.startInd = 5032
                vc.endInd = 5128
                
                //            vc.startIndexForEnglish = 5086
                //            vc.endIndexForEnglish = 5183
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "057" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 29
                vc.suratNumber = "057"
                
                vc.startInd = 5129
                vc.endInd = 5158
                //
                //            vc.startIndexForEnglish = 5184
                //            vc.endIndexForEnglish = 5214
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "058" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 22
                vc.suratNumber = "058"
                
                vc.startInd = 5159
                vc.endInd = 5181
                
                //            vc.startIndexForEnglish = 5215
                //            vc.endIndexForEnglish = 5238
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "059" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 24
                vc.suratNumber = "059"
                
                vc.startInd = 5182
                vc.endInd = 5206
                
                //            vc.startIndexForEnglish = 5239
                //            vc.endIndexForEnglish = 5264
                //
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "060" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 13
                vc.suratNumber = "060"
                
                vc.startInd = 5207
                vc.endInd = 5220
                
                //            vc.startIndexForEnglish = 5265
                //            vc.endIndexForEnglish = 5279
                
                //
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "061" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 14
                vc.suratNumber = "061"
                
                vc.startInd = 5221
                vc.endInd = 5235
                
                //            vc.startIndexForEnglish = 5280
                //            vc.endIndexForEnglish = 5295
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "062" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 11
                vc.suratNumber = "062"
                vc.startInd = 5236
                vc.endInd = 5247
                
                //            vc.startIndexForEnglish = 5296
                //            vc.endIndexForEnglish = 5308
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "063" {
                vc.current = indexPath.row + 1
                
                
                vc.ayatCount = 11
                vc.suratNumber = "063"
                vc.startInd = 5248
                vc.endInd = 5259
                
                //            vc.startIndexForEnglish = 5309
                //            vc.endIndexForEnglish = 5321
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "064" {
                vc.current = indexPath.row + 1
                
                
                vc.ayatCount = 18
                vc.suratNumber = "064"
                vc.startInd = 5260
                vc.endInd = 5278
                
                //            vc.startIndexForEnglish = 5322
                //            vc.endIndexForEnglish = 5341
                //
                //                                                 }
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "065" {
                vc.current = indexPath.row + 1
                
                
                vc.ayatCount = 12
                vc.suratNumber = "065"
                
                vc.startInd = 5279
                vc.endInd = 5291
                
                //            vc.startIndexForEnglish = 5342
                //            vc.endIndexForEnglish = 5355
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "066" {
                
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 12
                vc.suratNumber = "066"
                vc.startInd = 5292
                vc.endInd = 5304
                
                //            vc.startIndexForEnglish = 5356
                //            vc.endIndexForEnglish = 5369
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "067" {
                
                vc.ayatCount = 30
                vc.suratNumber = "067"
                
                vc.current = indexPath.row + 1
                vc.startInd = 5305
                vc.endInd = 5335
                
                //            vc.startIndexForEnglish = 5370
                //            vc.endIndexForEnglish = 5401
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "068"{
                
                vc.ayatCount = 52
                vc.suratNumber = "068"
                vc.current = indexPath.row + 1
                
                vc.startInd = 5336
                vc.endInd = 5388
                
                //            vc.startIndexForEnglish = 5402
                //            vc.endIndexForEnglish = 5455
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "069" {
                vc.current = indexPath.row + 1
                
                
                vc.ayatCount = 52
                vc.suratNumber = "069"
                vc.startInd = 5389
                vc.endInd = 5441
                
                //            vc.startIndexForEnglish = 5456
                //            vc.endIndexForEnglish = 5509
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "070" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 44
                vc.suratNumber = "070"
                vc.startInd = 5442
                vc.endInd = 5486
                
                //            vc.startIndexForEnglish = 5510
                //            vc.endIndexForEnglish = 5555
                //
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "071"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 28
                vc.suratNumber = "071"
                vc.startInd = 5487
                vc.endInd = 5515
                
                //            vc.startIndexForEnglish = 5556
                //            vc.endIndexForEnglish = 5585
                //
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "072" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 28
                vc.suratNumber = "072"
                
                vc.startInd = 5516
                vc.endInd = 5544
                
                
                //            vc.startIndexForEnglish = 5586
                //            vc.endIndexForEnglish = 5615
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "073" {
                
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 20
                vc.suratNumber = "073"
                vc.startInd = 5545
                vc.endInd = 5565
                
                //            vc.startIndexForEnglish = 5616
                //            vc.endIndexForEnglish = 5637
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "074" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 56
                vc.suratNumber = "074"
                
                vc.startInd = 5566
                vc.endInd = 5622
                
                //            vc.startIndexForEnglish = 5638
                //            vc.endIndexForEnglish = 5695
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "075" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 40
                vc.suratNumber = "075"
                vc.startInd = 5623
                vc.endInd = 5663
                //
                //            vc.startIndexForEnglish = 5696
                //            vc.endIndexForEnglish = 5737
                //
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "076" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 31
                vc.suratNumber = "076"
                vc.startInd = 5664
                vc.endInd = 5695
                
                //            vc.startIndexForEnglish = 5738
                //            vc.endIndexForEnglish = 5770
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "077" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 50
                vc.suratNumber = "077"
                
                vc.startInd = 5696
                vc.endInd = 5746
                
                //            vc.startIndexForEnglish = 5771
                //            vc.endIndexForEnglish = 5822
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "078" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 40
                vc.suratNumber = "078"
                vc.startInd = 5747
                vc.endInd = 5787
                
                //            vc.startIndexForEnglish = 5823
                //            vc.endIndexForEnglish = 5864
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "079"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 46
                vc.suratNumber = "079"
                
                vc.startInd = 5788
                vc.endInd = 5834
                
                //            vc.startIndexForEnglish = 5865
                //            vc.endIndexForEnglish = 5912
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "080" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 42
                vc.suratNumber = "080"
                
                vc.startInd = 5835
                vc.endInd = 5877
                
                //            vc.startIndexForEnglish = 5913
                //            vc.endIndexForEnglish = 5956
                
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "081" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 29
                vc.suratNumber = "081"
                
                vc.startInd = 5878
                vc.endInd = 5907
                
                //            vc.startIndexForEnglish = 5957
                //            vc.endIndexForEnglish = 5987
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "082"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 19
                vc.suratNumber = "082"
                
                vc.startInd = 5908
                vc.endInd = 5927
                
                //            vc.startIndexForEnglish = 5988
                //            vc.endIndexForEnglish = 6008
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "083" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 36
                vc.suratNumber = "083"
                vc.startInd = 5928
                vc.endInd = 5964
                
                //            vc.startIndexForEnglish = 6009
                //            vc.endIndexForEnglish = 6046
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "084" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 25
                vc.suratNumber = "084"
                
                vc.startInd = 5965
                vc.endInd = 5990
                
                //            vc.startIndexForEnglish = 6047
                //            vc.endIndexForEnglish = 6073
                //
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "085" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 22
                vc.suratNumber = "085"
                
                vc.startInd = 5991
                vc.endInd = 6013
                
                //            vc.startIndexForEnglish = 6074
                //            vc.endIndexForEnglish = 6097
                //
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "086" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 17
                vc.suratNumber = "086"
                
                vc.startInd = 6014
                vc.endInd = 6031
                
                //            vc.startIndexForEnglish = 6098
                //            vc.endIndexForEnglish = 6116
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "087" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 19
                vc.suratNumber = "087"
                
                vc.startInd = 6032
                vc.endInd = 6051
                
                //            vc.startIndexForEnglish = 6117
                //            vc.endIndexForEnglish = 6137
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "088" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 26
                vc.suratNumber = "088"
                vc.startInd = 6052
                vc.endInd = 6078
                //
                //            vc.startIndexForEnglish = 6138
                //            vc.endIndexForEnglish = 6165
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "089" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 30
                vc.suratNumber = "089"
                vc.startInd = 6079
                vc.endInd = 6109
                
                //            vc.startIndexForEnglish = 6166
                //            vc.endIndexForEnglish = 6197
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "090"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 20
                vc.suratNumber = "090"
                
                vc.startInd = 6110
                vc.endInd = 6130
                //
                //            vc.startIndexForEnglish = 6198
                //            vc.endIndexForEnglish = 6219
                //
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "091"{
                vc.current = indexPath.row + 1
                
                
                vc.ayatCount = 15
                vc.suratNumber = "091"
                vc.startInd = 6131
                vc.endInd = 6146
                
                //            vc.startIndexForEnglish = 6220
                //            vc.endIndexForEnglish = 6236
                //
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "092"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 21
                vc.suratNumber = "092"
                
                vc.startInd = 6147
                vc.endInd = 6168
                
                //            vc.startIndexForEnglish = 6237
                //            vc.endIndexForEnglish = 6259
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "093"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 11
                vc.suratNumber = "093"
                
                vc.startInd = 6169
                vc.endInd = 6180
                
                //            vc.startIndexForEnglish = 6260
                //            vc.endIndexForEnglish = 6272
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "094"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 8
                vc.suratNumber = "094"
                
                vc.startInd = 6181
                vc.endInd = 6189
                
                //            vc.startIndexForEnglish = 6273
                //            vc.endIndexForEnglish = 6282
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "095" {
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 8
                vc.suratNumber = "095"
                vc.startInd = 6190
                vc.endInd = 6198
                
                //            vc.startIndexForEnglish = 6283
                //            vc.endIndexForEnglish = 6292
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "096"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 19
                vc.suratNumber = "096"
                
                vc.startInd = 6199
                vc.endInd = 6218
                
                //            vc.startIndexForEnglish = 6293
                //            vc.endIndexForEnglish = 6313
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "097"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 5
                vc.suratNumber = "097"
                vc.startInd = 6219
                vc.endInd = 6224
                
                //            vc.startIndexForEnglish = 6314
                //            vc.endIndexForEnglish = 6320
                //
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "098"{
                
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 8
                vc.suratNumber = "098"
                vc.startInd = 6225
                vc.endInd = 6233
                
                //            vc.startIndexForEnglish = 6321
                //            vc.endIndexForEnglish = 6330
                //
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "099"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 8
                vc.suratNumber = "099"
                
                vc.startInd = 6234
                vc.endInd = 6242
                
                //            vc.startIndexForEnglish = 6331
                //            vc.endIndexForEnglish = 6340
                //
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "100"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 11
                vc.suratNumber = "100"
                
                vc.startInd = 6243
                vc.endInd = 6254
                
                //            vc.startIndexForEnglish = 6341
                //            vc.endIndexForEnglish = 6353
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "101"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 11
                vc.suratNumber = "101"
                
                vc.startInd = 6255
                vc.endInd = 6266
                
                //            vc.startIndexForEnglish = 6354
                //            vc.endIndexForEnglish = 6366
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "102"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 8
                vc.suratNumber = "102"
                
                vc.startInd = 6267
                vc.endInd = 6275
                
                //            vc.startIndexForEnglish = 6367
                //            vc.endIndexForEnglish = 6376
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "103"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 3
                vc.suratNumber = "103"
                
                vc.startInd = 6276
                vc.endInd = 6279
                
                //            vc.startIndexForEnglish = 6377
                //            vc.endIndexForEnglish = 6381
                //
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "104"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 9
                vc.suratNumber = "104"
                
                vc.startInd = 6280
                vc.endInd = 6289
                
                //            vc.startIndexForEnglish = 6382
                //            vc.endIndexForEnglish = 6392
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "105"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 5
                vc.suratNumber = "105"
                
                vc.startInd = 6290
                vc.endInd = 6295
                
                //            vc.startIndexForEnglish = 6393
                //            vc.endIndexForEnglish = 6399
                //
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "106"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 4
                vc.suratNumber = "106"
                
                vc.startInd = 6296
                vc.endInd = 6300
                
                //                  vc.startIndexForEnglish = 6400
                //                  vc.endIndexForEnglish = 6405
                //
            }
            
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "107"{
                
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 7
                vc.suratNumber = "107"
                vc.startInd = 6301
                vc.endInd = 6308
                
                //            vc.startIndexForEnglish = 6406
                //            vc.endIndexForEnglish = 6414
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "108"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 3
                vc.suratNumber = "108"
                
                vc.startInd = 6309
                vc.endInd = 6312
                
                //            vc.startIndexForEnglish = 6415
                //            vc.endIndexForEnglish = 6419
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "109"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 6
                vc.suratNumber = "109"
                
                vc.startInd = 6313
                vc.endInd = 6319
                
                //            vc.startIndexForEnglish = 6420
                //            vc.endIndexForEnglish = 6427
                //
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "110"{
                
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 3
                vc.suratNumber = "110"
                vc.startInd = 6320
                vc.endInd = 6323
                
                //            vc.startIndexForEnglish = 6428
                //            vc.endIndexForEnglish = 6432
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "111"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 5
                vc.suratNumber = "111"
                vc.startInd = 6324
                vc.endInd = 6329
                //
                //                  vc.startIndexForEnglish = 6433
                //                  vc.endIndexForEnglish = 6439
                //
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "112"{
                vc.current = indexPath.row + 1
                
                
                vc.ayatCount = 4
                vc.suratNumber = "112"
                vc.startInd = 6330
                vc.endInd = 6334
                
                //            vc.startIndexForEnglish = 6440
                //            vc.endIndexForEnglish = 6445
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "113"{
                vc.current = indexPath.row + 1
                
                vc.ayatCount = 5
                vc.suratNumber = "113"
                
                vc.startInd = 6335
                vc.endInd = 6340
                
                //            vc.startIndexForEnglish = 6446
                //            vc.endIndexForEnglish = 6452
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "114"{
                vc.current = indexPath.row + 1
                
                
                vc.ayatCount = 6
                vc.suratNumber = "114"
                vc.startInd = 6341
                vc.endInd = 6347
                
                //            vc.startIndexForEnglish = 6453
                //            vc.endIndexForEnglish = 6460
                //
            }
        }
        
        else{
            vc.fromVC = "Sipary"
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "001" {
                vc.startInd = 0
                vc.endInd = 148
                vc.suratCount = 2
                vc.current = 1
                 vc.currentEng = 1
                vc.startAyat = 0
                vc.ArrSurah.append(7)
                vc.ArrSurah.append(141)
                vc.siparaNumber = ["001","002"]
                vc.siparaAyatCount = vc.endInd - vc.startInd
              
            }
            
            
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "002" {
                vc.suratCount = 1
                vc.ArrSurah.append(252)
                vc.current = 2
                vc.currentEng = 2
                vc.count = 2
                vc.startInd = 149
                vc.endInd = 259
                vc.suratCount  = 1
                vc.startAyat = 142
                vc.siparaNumber = ["002"]
                vc.siparaAyatCount = vc.endInd - vc.startInd
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "003" {
                vc.suratCount = 2
                vc.ArrSurah.append(286)
                vc.ArrSurah.append(91)
                vc.current =  2
                vc.currentEng = 2
                vc.startAyat = 253
                vc.startInd = 260
                vc.endInd = 385
                
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "004" {
                
                
                vc.suratCount = 2
                vc.ArrSurah.append(200)
                vc.ArrSurah.append(23)
                vc.current = 3
                vc.startAyat = 92
                vc.startInd = 386
                vc.endInd = 518
                vc.currentEng = 3
              
                
                
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "005" {
                vc.suratCount = 1
                vc.startAyat = 24
                vc.ArrSurah.append(147)
                vc.current = 4
                vc.startInd = 519
                vc.endInd = 642
                vc.currentEng = 4
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "006" {
                vc.suratCount = 2
                vc.startAyat = 148
                vc.ArrSurah.append(176)
                vc.ArrSurah.append(82)
                vc.current = 4
                vc.startInd = 643
                vc.endInd = 754
                vc.currentEng = 4
                
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "007" {
                vc.suratCount = 2
                vc.startAyat = 83
                vc.ArrSurah.append(120)
                vc.ArrSurah.append(110)
                vc.current = 5
                vc.startInd = 755
                vc.endInd = 903
                vc.currentEng = 5
               
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "008" {
                vc.suratCount = 2
                vc.startAyat = 111
                vc.ArrSurah.append(165)
                vc.ArrSurah.append(87)
                vc.current = 7
                vc.startInd = 904
                vc.endInd = 1048
                vc.currentEng = 7
                
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "009" {
                vc.suratCount = 2
                vc.startAyat = 88
                vc.ArrSurah.append(206)
                vc.ArrSurah.append(40)
                vc.current = 7
                vc.currentEng = 7
                vc.startInd = 1049
                vc.endInd = 1206
                
             
                
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "010" {
                
                vc.suratCount  = 2
                vc.startAyat = 41
                vc.ArrSurah.append(75)
                vc.ArrSurah.append(93)
                vc.current = 8
                vc.currentEng = 8
                vc.startInd = 1207
                vc.endInd = 1334
                
                
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "011" {
                vc.current = 9
                vc.suratCount = 3
                vc.ArrSurah.append(129)
                vc.ArrSurah.append(109)
                vc.ArrSurah.append(5)
                vc.startAyat = 94
                vc.currentEng = 9
                vc.startInd = 1335
                vc.endInd = 1486
                
             
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "012" {
                
                
                vc.startAyat = 6
                vc.current = 11
                vc.ArrSurah.append(123)
                vc.ArrSurah.append(52)
                vc.suratCount = 2
                vc.startInd = 1487
                vc.endInd = 1657
                vc.currentEng = 11
             
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "013" {
                vc.startAyat = 53
                vc.suratCount = 4
                vc.ArrSurah.append(111)
                vc.ArrSurah.append(43)
                vc.ArrSurah.append(52)
                vc.ArrSurah.append(1)
                vc.current = 12
                vc.startInd = 1658
                vc.endInd = 1815
                vc.currentEng = 12
             
                
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "014" {
                vc.startAyat = 2
                vc.current  = 15
                vc.suratCount = 2
                vc.ArrSurah.append(99)
                vc.ArrSurah.append(128)
                vc.startInd = 1816
                vc.endInd = 2043
               vc.currentEng = 15
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "015" {
                vc.current = 17
                vc.suratCount = 2
                vc.startAyat = 1
                vc.ArrSurah.append(111)
                vc.ArrSurah.append(74)
                
                
                vc.startInd = 2044
                vc.endInd = 2229
                vc.currentEng = 17
              
                
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "016" {
                vc.startAyat = 75
                vc.suratCount = 3
                vc.ArrSurah.append(110)
                vc.ArrSurah.append(98)
                vc.ArrSurah.append(135)
                vc.current = 18
                vc.startInd = 2230
                vc.endInd = 2500
                
             vc.currentEng = 18
                
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "017" {
                vc.current = 21
                vc.startAyat = 1
                vc.suratCount = 2
                vc.ArrSurah.append(112)
                vc.ArrSurah.append(78)
                
                vc.startInd = 2501
                vc.endInd = 2692
                vc.currentEng = 21
               
                
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "018" {
                
                vc.current = 23
                vc.startAyat = 0
                vc.suratCount = 3
                vc.ArrSurah.append(118)
                vc.ArrSurah.append(64)
                vc.ArrSurah.append(20)
                vc.startInd = 2693
                vc.endInd = 2897
                
              vc.currentEng = 23
                
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "019" {
                vc.current = 25
                vc.suratCount = 3
                vc.startAyat = 21
                vc.ArrSurah.append(77)
                vc.ArrSurah.append(227)
                vc.ArrSurah.append(59)
                vc.startInd = 2898
                vc.endInd = 3242
                vc.currentEng = 25
               
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "020" {
                vc.current = 27
                vc.suratCount = 3
                vc.startAyat = 60
                vc.ArrSurah.append(93)
                vc.ArrSurah.append(88)
                vc.ArrSurah.append(44)
                vc.startInd = 3243
                vc.endInd = 3410
                vc.currentEng = 27
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "021" {
                
                vc.current = 29
                vc.startAyat = 45
                vc.suratCount = 5
                vc.ArrSurah.append(69)
                vc.ArrSurah.append(60)
                vc.ArrSurah.append(34)
                vc.ArrSurah.append(30)
                vc.ArrSurah.append(30)
                vc.currentEng = 29
                vc.startInd = 3411
                vc.endInd = 3593
                
             
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "022" {
                vc.current = 33
                vc.startAyat = 31
                vc.suratCount = 4
                vc.ArrSurah.append(73)
                vc.ArrSurah.append(54)
                vc.ArrSurah.append(45)
                vc.ArrSurah.append(21)
                vc.startInd = 3594
                vc.endInd = 3759
                vc.currentEng = 33
             
                
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "023" {
                vc.current = 36
                vc.startAyat = 22
                vc.suratCount = 4
                vc.ArrSurah.append(83)
                vc.ArrSurah.append(182)
                vc.ArrSurah.append(88)
                vc.ArrSurah.append(31)
                vc.currentEng = 36
                
                
                vc.startInd = 3760
                vc.endInd = 4125
                
              vc.currentEng = 36
                
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "024" {
                
                
                
                vc.current = 39
                vc.startAyat = 32
                vc.ArrSurah.append(75)
                vc.ArrSurah.append(85)
                vc.ArrSurah.append(42)
                vc.suratCount = 3
                vc.startInd = 4126
                vc.endInd = 4302
                vc.currentEng = 39
               
                
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "025" {
                vc.current = 42
                vc.startAyat = 1
                vc.ArrSurah.append(53)
                vc.ArrSurah.append(89)
                vc.ArrSurah.append(59)
                vc.ArrSurah.append(37)
                vc.suratCount = 4
                vc.currentEng = 42
                vc.startInd = 4301
                vc.endInd = 4552
                
             
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "026" {
                vc.currentEng = 46
                vc.startAyat = 1
                vc.current = 46
                vc.ArrSurah.append(35)
                vc.ArrSurah.append(38)
                vc.ArrSurah.append(29)
                vc.ArrSurah.append(18)
                vc.ArrSurah.append(45)
                vc.ArrSurah.append(30)
                vc.suratCount = 6
                vc.startInd = 4553
                vc.endInd = 4753
                
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "027" {
                vc.startAyat = 31
                vc.current = 51
                vc.ArrSurah.append(60)
                vc.ArrSurah.append(49)
                vc.ArrSurah.append(62)
                vc.ArrSurah.append(55)
                vc.ArrSurah.append(78)
                vc.ArrSurah.append(96)
                vc.ArrSurah.append(29)
                vc.suratCount = 7
                
                
                vc.currentEng = 51
                vc.startInd = 4754
                vc.endInd = 5158
                
              
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "028" {
                
                vc.startAyat = 0
                vc.current = 58
                vc.ArrSurah.append(22)
                vc.ArrSurah.append(24)
                vc.ArrSurah.append(13)
                vc.ArrSurah.append(14)
                vc.ArrSurah.append(11)
                vc.ArrSurah.append(11)
                vc.ArrSurah.append(18)
                vc.ArrSurah.append(12)
                vc.ArrSurah.append(12)
                vc.suratCount = 9
                vc.startInd = 5159
                vc.endInd = 5304
                
                vc.currentEng = 58
            }
            
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "029" {
                
                vc.currentEng = 67
                vc.startAyat = 0
                vc.current = 67
                vc.ArrSurah.append(30)
                vc.ArrSurah.append(52)
                vc.ArrSurah.append(52)
                vc.ArrSurah.append(44)
                vc.ArrSurah.append(28)
                vc.ArrSurah.append(28)
                vc.ArrSurah.append(20)
                vc.ArrSurah.append(56)
                vc.ArrSurah.append(40)
                vc.ArrSurah.append(31)
                vc.ArrSurah.append(50)
                vc.suratCount = 11
                vc.startInd = 5305
                vc.endInd = 5746
                
                
            }
            if bookmark.value(forKey: "surahOrSiparahNumber") as! String == "030" {
                vc.current = 78
                vc.currentEng = 78
                vc.startAyat = 0
                vc.ArrSurah.append(40)
                vc.ArrSurah.append(46)
                vc.ArrSurah.append(42)
                vc.ArrSurah.append(29)
                vc.ArrSurah.append(19)
                vc.ArrSurah.append(36)
                vc.ArrSurah.append(25)
                vc.ArrSurah.append(22)
                vc.ArrSurah.append(17)
                vc.ArrSurah.append(19)
                vc.ArrSurah.append(26)
                vc.ArrSurah.append(30)
                vc.ArrSurah.append(20)
                vc.ArrSurah.append(15)
                vc.ArrSurah.append(21)
                vc.ArrSurah.append(11)
                vc.ArrSurah.append(8)
                vc.ArrSurah.append(8)
                vc.ArrSurah.append(19)
                vc.ArrSurah.append(5)
                vc.ArrSurah.append(8)
                vc.ArrSurah.append(8)
                vc.ArrSurah.append(11)
                vc.ArrSurah.append(11)
                vc.ArrSurah.append(8)
                vc.ArrSurah.append(3)
                vc.ArrSurah.append(9)
                vc.ArrSurah.append(5)
                vc.ArrSurah.append(4)
                vc.ArrSurah.append(7)
                vc.ArrSurah.append(3)
                vc.ArrSurah.append(6)
                vc.ArrSurah.append(3)
                vc.ArrSurah.append(5)
                vc.ArrSurah.append(4)
                vc.ArrSurah.append(5)
                vc.ArrSurah.append(6)
                vc.suratCount = 37







                       vc.startInd = 5747
                       vc.endInd = 6348
                
                
                   }
        }
        
        
        
        
       
        
        
         self.navigationController?.pushViewController(vc, animated: true)
        
       // performSegue(withIdentifier: "bookmarkToSpecSuratVC", sender: self)
    }
    
    
    
    func createData(name: String, ayat: String) {
           
           //create data
           guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
           
           let managedContext = appDelegate.persistentContainer.viewContext
           
           let userEntity = NSEntityDescription.entity(forEntityName: "Bookmarks",
                                                       in: managedContext)!
           
           let bookmark = NSManagedObject(entity: userEntity,
                                          insertInto: managedContext)
           
           
           bookmark.setValue(name, forKeyPath: "name")
           bookmark.setValue(ayat, forKeyPath: "ayat")
           
           //  bookmark.setValue("second bookmark", forKeyPath: "name")
           
           
           
           
           do {
               try managedContext.save()
               
               let vc = BookmarkViewController()
               vc.bookmarkArray.append(bookmark)
               //  bookmark.append(person)
           } catch let error as NSError {
               print("Could not save. \(error), \(error.userInfo)")
           }
           
       }
    
    
      func removeLocalNotification(timeArr : [Any]) {
       
          
      }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bookmarkCell", for: indexPath) as! BookmarkTableViewCell
        print(bookmarkArray)
        let bookmark = bookmarkArray[indexPath.row]
//        let data = readArray[indexPath.row]
//
        cell.lblTitle.text = bookmark.value(forKey: "title") as? String
        cell.bookMarkName.text = bookmark.value(forKey: "name") as? String
        cell.ayatLabel.text = (bookmark.value(forKey: "ayat") as! String)
//
        //readArray.removeAll()
        
        return cell
    }
    
    
}

extension BookmarkViewController{
    
    func retrieveData() {
        //1
         guard let appDelegate =
           UIApplication.shared.delegate as? AppDelegate else {
             return
         }
         
         let managedContext =
           appDelegate.persistentContainer.viewContext
         
         //2
         let fetchRequest =
           NSFetchRequest<NSManagedObject>(entityName: "Bookmarks")
         
         //3
         do {
            bookmarkArray = try managedContext.fetch(fetchRequest)
//           bookMarkArray = try managedContext.fetch(fetchRequest)
//            for data in bookMarkArray as! [NSManagedObject]{
//                print(data.value(forKey: "name") as! String)
//            }
            
         } catch let error as NSError {
           print("Could not fetch. \(error), \(error.userInfo)")
         }
    }
}





//if let data = UserDefaults.standard.value(forKey:"bookmark") as? Data {
//        var bookmark = try? PropertyListDecoder().decode(Array<Bookmark>.self, from: data)
//
//        readArray = bookmark!
//
//    }
//
