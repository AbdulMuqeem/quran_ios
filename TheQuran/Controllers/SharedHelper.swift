//
//  SharedHelper.swift
//  TheQuran
//
//  Created by haya on 04/04/2020.
//

import Foundation
import UIKit
import iProgressHUD

let iprogress: iProgressHUD = iProgressHUD()



func showSpinner(view : UIView) {
    
     
     
     iprogress.isShowModal = true
     iprogress.isShowCaption = true
     iprogress.isTouchDismiss = false
     iprogress.indicatorStyle = .circleStrokeSpin
     iprogress.indicatorColor = .white
     iprogress.iprogressStyle = .horizontal
     iprogress.indicatorView.startAnimating()
     iprogress.attachProgress(toView: view)
     view.showProgress()
     
 }
 func hideSpinner(view : UIView) {
     iprogress.indicatorView.stopAnimating()
     view.dismissProgress()
 }

enum ToastlabelAlignment : CGFloat{
    case center
    case bottom
    case top
}


func showToast(message : String, viewController : UIViewController) {
//    var xPosition : CGFloat = 0.0
//    var yPosition : CGFloat = 0.0
  //  let toastLabel = UILabel()
    
//    switch align {
//    case .center:
//        xPosition = viewController.view.frame.width / 2
//        yPosition = viewController.view.frame.height / 2
//
//    case .bottom:
//        xPosition = viewController.view.frame.size.width/2 - 75
//        yPosition = viewController.view.frame.size.height - 100
//
//    default:
//        break
//
//    }
    
//    let toastLabel = UILabel(frame: CGRect(x: viewController.view.frame.size.width/2 - 75, y: viewController.view.frame.size.height-100, width: 150, height: 35))
    let toastLabel = UILabel(frame: CGRect(x: 0, y: viewController.view.center.y, width: 150, height: 35))
    toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.8)
    toastLabel.textColor = UIColor.white
    toastLabel.textAlignment = .center;
    toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
    toastLabel.text = message
    toastLabel.alpha = 1.0
    toastLabel.layer.cornerRadius = 10;
    toastLabel.clipsToBounds  =  true
    toastLabel.center = viewController.view.center
    viewController.view.addSubview(toastLabel)
    UIView.animate(withDuration: 5.0, delay: 0.10, options: .curveEaseOut, animations: {
        toastLabel.alpha = 0.0
    }, completion: {(isCompleted) in
        toastLabel.removeFromSuperview()
    })
}
 
