//
//  SiparayViewController.swift
//  TheQuran
//
//  Created by engitech on 04/03/2020.
//

import UIKit
import XLPagerTabStrip
import CoreData
class SiparayViewController: UIViewController {
    
    
    var quranUrls = [String]()
 
    
    var constant = Constants()
    //   var surahArr : [Any] = []
    var SiparayArray = [Siparay]()
    var surahArrA : [Any] = []
    var surahArrE : [Any] = []

    var arrSipary : [Any] = []
    @IBOutlet weak var SiparayTableView: UITableView!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        SiparayTableView.delegate = self
        SiparayTableView.dataSource = self
        
        
    }

}
extension SiparayViewController: IndicatorInfoProvider{
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "PARA")
    }
    
    
}
// implement tableview delegate and datasource methods
extension SiparayViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return constant.siparayArray.count
        //return arrSipary.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //        let cell = tableView.dequeueReusableCell(withIdentifier: "SiparayCell") as! SiparayTableViewCell
        //        let dict = arrSipary[indexPath.row] as! NSDictionary
        //        cell.siparayNameLabel.text = (dict["surahName"] as! String)
        //
        //        return cell
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SiparayCell", for: indexPath) as! SiparayTableViewCell
        let dict = constant.siparayArray[indexPath.row] as! Dictionary<String,Any>
        
      
        cell.siparayEnglishNameLabel.text = dict["SiparayEnglishName"] as! String
        cell.siparayNameLabel.text = (dict["SiparayNameArabic"] as! String)
        cell.serialNumberLabel.text = (dict["number"] as! String)
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "specSuratVC") as! specSuratViewController
        
        
        let cell = tableView.cellForRow(at: indexPath) as! SiparayTableViewCell
        vc.navigationItem.title = cell.siparayEnglishNameLabel.text!
        vc.modalPresentationStyle = .fullScreen
        
        vc.fromVC = "Sipary"
        if indexPath.row == 0 {
            
            vc.paraNumber = "001"
            vc.startInd = 0
            vc.endInd = 148
            vc.suratCount = 2
            vc.current = 1
             vc.currentEng = 1
            vc.startAyat = 0
            vc.ArrSurah.append(7)
            vc.ArrSurah.append(141)
            vc.siparaNumber = ["001","002"]
            vc.siparaAyatCount = vc.endInd - vc.startInd
          
        }
        
        
        
        if indexPath.row == 1 {
            
            vc.paraNumber = "002"
            vc.suratCount = 1
            vc.ArrSurah.append(252)
            vc.current = 2
            vc.currentEng = 2
            vc.count = 2
            vc.startInd = 149
            vc.endInd = 259
            vc.suratCount  = 1
            vc.startAyat = 142
            vc.siparaNumber = ["002"]
            vc.siparaAyatCount = vc.endInd - vc.startInd
        }
        
        if indexPath.row == 2 {
            
            vc.paraNumber = "003"
            vc.suratCount = 2
            vc.ArrSurah.append(286)
            vc.ArrSurah.append(91)
            vc.current =  2
            vc.currentEng = 2
            vc.startAyat = 253
            vc.startInd = 260
            vc.endInd = 385
            
        }
        
        if indexPath.row == 3 {
            
            vc.paraNumber = "004"
            vc.suratCount = 2
            vc.ArrSurah.append(200)
            vc.ArrSurah.append(23)
            vc.current = 3
            vc.startAyat = 92
            vc.startInd = 386
            vc.endInd = 518
            vc.currentEng = 3
          
            
            
        }
        
        if indexPath.row == 4 {
            
            vc.paraNumber = "005"
            vc.suratCount = 1
            vc.startAyat = 24
            vc.ArrSurah.append(147)
            vc.current = 4
            vc.startInd = 519
            vc.endInd = 642
            vc.currentEng = 4
        }
        
        if indexPath.row == 5 {
            
            vc.paraNumber = "006"
            vc.suratCount = 2
            vc.startAyat = 148
            vc.ArrSurah.append(176)
            vc.ArrSurah.append(82)
            vc.current = 4
            vc.startInd = 643
            vc.endInd = 754
            vc.currentEng = 4
            
        }
        
        if indexPath.row == 6 {
            
            vc.paraNumber = "007"
            vc.suratCount = 2
            vc.startAyat = 83
            vc.ArrSurah.append(120)
            vc.ArrSurah.append(110)
            vc.current = 5
            vc.startInd = 755
            vc.endInd = 903
            vc.currentEng = 5
           
        }
        
        if indexPath.row == 7 {
            
            vc.paraNumber = "008"
            vc.suratCount = 2
            vc.startAyat = 111
            vc.ArrSurah.append(165)
            vc.ArrSurah.append(87)
            vc.current = 7
            vc.startInd = 904
            vc.endInd = 1048
            vc.currentEng = 7
            
        }
        
        if indexPath.row == 8 {
            
            vc.paraNumber = "009"
            vc.suratCount = 2
            vc.startAyat = 88
            vc.ArrSurah.append(206)
            vc.ArrSurah.append(40)
            vc.current = 7
            vc.currentEng = 7
            vc.startInd = 1049
            vc.endInd = 1206
            
         
            
        }
        
        if indexPath.row == 9 {
            
            vc.paraNumber = "010"
            
            vc.suratCount  = 2
            vc.startAyat = 41
            vc.ArrSurah.append(75)
            vc.ArrSurah.append(93)
            vc.current = 8
            vc.currentEng = 8
            vc.startInd = 1207
            vc.endInd = 1334
            
            
        }
        
        if indexPath.row == 10 {
            
            
            vc.paraNumber = "011"
            vc.current = 9
            vc.suratCount = 3
            vc.ArrSurah.append(129)
            vc.ArrSurah.append(109)
            vc.ArrSurah.append(5)
            vc.startAyat = 94
            vc.currentEng = 9
            vc.startInd = 1335
            vc.endInd = 1486
            
         
        }
        
        if indexPath.row == 11 {
            
            vc.paraNumber = "012"
            vc.startAyat = 6
            vc.current = 11
            vc.ArrSurah.append(123)
            vc.ArrSurah.append(52)
            vc.suratCount = 2
            vc.startInd = 1487
            vc.endInd = 1657
            vc.currentEng = 11
         
        }
        
        if indexPath.row == 12 {
            
            vc.paraNumber = "013"
            vc.startAyat = 53
            vc.suratCount = 4
            vc.ArrSurah.append(111)
            vc.ArrSurah.append(43)
            vc.ArrSurah.append(52)
            vc.ArrSurah.append(1)
            vc.current = 12
            vc.startInd = 1658
            vc.endInd = 1815
            vc.currentEng = 12
         
            
        }
        
        if indexPath.row == 13 {
            
            vc.paraNumber = "014"
            vc.startAyat = 2
            vc.current  = 15
            vc.suratCount = 2
            vc.ArrSurah.append(99)
            vc.ArrSurah.append(128)
            vc.startInd = 1816
            vc.endInd = 2043
           vc.currentEng = 15
        }
        
        if indexPath.row == 14 {
            
            vc.paraNumber = "015"
            vc.current = 17
            vc.suratCount = 2
            vc.startAyat = 1
            vc.ArrSurah.append(111)
            vc.ArrSurah.append(74)
            
            
            vc.startInd = 2044
            vc.endInd = 2229
            vc.currentEng = 17
          
            
        }
        
        if indexPath.row == 15 {
            
            vc.paraNumber = "016"
            vc.startAyat = 75
            vc.suratCount = 3
            vc.ArrSurah.append(110)
            vc.ArrSurah.append(98)
            vc.ArrSurah.append(135)
            vc.current = 18
            vc.startInd = 2230
            vc.endInd = 2500
            
         vc.currentEng = 18
            
        }
        
        if indexPath.row == 16 {
            
            vc.paraNumber = "017"
            vc.current = 21
            vc.startAyat = 1
            vc.suratCount = 2
            vc.ArrSurah.append(112)
            vc.ArrSurah.append(78)
            
            vc.startInd = 2501
            vc.endInd = 2692
            vc.currentEng = 21
           
            
        }
        
        if indexPath.row == 17 {
            
            vc.paraNumber = "018"
            
            vc.current = 23
            vc.startAyat = 0
            vc.suratCount = 3
            vc.ArrSurah.append(118)
            vc.ArrSurah.append(64)
            vc.ArrSurah.append(20)
            vc.startInd = 2693
            vc.endInd = 2897
            
          vc.currentEng = 23
            
        }
        
        if indexPath.row == 18 {
            
            vc.paraNumber = "019"
            vc.current = 25
            vc.suratCount = 3
            vc.startAyat = 21
            vc.ArrSurah.append(77)
            vc.ArrSurah.append(227)
            vc.ArrSurah.append(59)
            vc.startInd = 2898
            vc.endInd = 3242
            vc.currentEng = 25
           
        }
        
        if indexPath.row == 19 {
            
            vc.paraNumber = "020"
            vc.current = 27
            vc.suratCount = 3
            vc.startAyat = 60
            vc.ArrSurah.append(93)
            vc.ArrSurah.append(88)
            vc.ArrSurah.append(44)
            vc.startInd = 3243
            vc.endInd = 3410
            vc.currentEng = 27
        }
        
        if indexPath.row == 20 {
            
            vc.paraNumber = "021"
            
            vc.current = 29
            vc.startAyat = 45
            vc.suratCount = 5
            vc.ArrSurah.append(69)
            vc.ArrSurah.append(60)
            vc.ArrSurah.append(34)
            vc.ArrSurah.append(30)
            vc.ArrSurah.append(30)
            vc.currentEng = 29
            vc.startInd = 3411
            vc.endInd = 3593
            
         
        }
        
        if indexPath.row == 21 {
            
            vc.paraNumber = "022"
            vc.current = 33
            vc.startAyat = 31
            vc.suratCount = 4
            vc.ArrSurah.append(73)
            vc.ArrSurah.append(54)
            vc.ArrSurah.append(45)
            vc.ArrSurah.append(21)
            vc.startInd = 3594
            vc.endInd = 3759
            vc.currentEng = 33
         
            
        }
        
        if indexPath.row == 22 {
            
            vc.paraNumber = "023"
            vc.current = 36
            vc.startAyat = 22
            vc.suratCount = 4
            vc.ArrSurah.append(83)
            vc.ArrSurah.append(182)
            vc.ArrSurah.append(88)
            vc.ArrSurah.append(31)
            vc.currentEng = 36
            
            
            vc.startInd = 3760
            vc.endInd = 4125
            
          vc.currentEng = 36
            
        }
        
        if indexPath.row == 23 {
            
            
            vc.paraNumber = "024"
            vc.current = 39
            vc.startAyat = 32
            vc.ArrSurah.append(75)
            vc.ArrSurah.append(85)
            vc.ArrSurah.append(42)
            vc.suratCount = 3
            vc.startInd = 4126
            vc.endInd = 4302
            vc.currentEng = 39
           
            
        }
        
        if indexPath.row == 24 {
            
            vc.paraNumber = "025"
            vc.current = 42
            vc.startAyat = 1
            vc.ArrSurah.append(53)
            vc.ArrSurah.append(89)
            vc.ArrSurah.append(59)
            vc.ArrSurah.append(37)
            vc.suratCount = 4
            vc.currentEng = 42
            vc.startInd = 4301
            vc.endInd = 4552
            
         
        }
        
        if indexPath.row == 25 {
            
            vc.paraNumber = "026"
            vc.currentEng = 46
            vc.startAyat = 1
            vc.current = 46
            vc.ArrSurah.append(35)
            vc.ArrSurah.append(38)
            vc.ArrSurah.append(29)
            vc.ArrSurah.append(18)
            vc.ArrSurah.append(45)
            vc.ArrSurah.append(30)
            vc.suratCount = 6
            vc.startInd = 4553
            vc.endInd = 4753
            
        }
        
        if indexPath.row == 26 {
            
            
            vc.paraNumber = "027"
            vc.startAyat = 31
            vc.current = 51
            vc.ArrSurah.append(60)
            vc.ArrSurah.append(49)
            vc.ArrSurah.append(62)
            vc.ArrSurah.append(55)
            vc.ArrSurah.append(78)
            vc.ArrSurah.append(96)
            vc.ArrSurah.append(29)
            vc.suratCount = 7
            
            
            vc.currentEng = 51
            vc.startInd = 4754
            vc.endInd = 5158
            
          
        }
        
        if indexPath.row == 27 {
            
            vc.paraNumber = "028"
            
            vc.startAyat = 0
            vc.current = 58
            vc.ArrSurah.append(22)
            vc.ArrSurah.append(24)
            vc.ArrSurah.append(13)
            vc.ArrSurah.append(14)
            vc.ArrSurah.append(11)
            vc.ArrSurah.append(11)
            vc.ArrSurah.append(18)
            vc.ArrSurah.append(12)
            vc.ArrSurah.append(12)
            vc.suratCount = 9
            vc.startInd = 5159
            vc.endInd = 5304
            
            vc.currentEng = 58
        }
        
        if indexPath.row == 28 {
            
            vc.paraNumber = "029"
            
            vc.currentEng = 67
            vc.startAyat = 0
            vc.current = 67
            vc.ArrSurah.append(30)
            vc.ArrSurah.append(52)
            vc.ArrSurah.append(52)
            vc.ArrSurah.append(44)
            vc.ArrSurah.append(28)
            vc.ArrSurah.append(28)
            vc.ArrSurah.append(20)
            vc.ArrSurah.append(56)
            vc.ArrSurah.append(40)
            vc.ArrSurah.append(31)
            vc.ArrSurah.append(50)
            vc.suratCount = 11
            vc.startInd = 5305
            vc.endInd = 5746
            
            
        }
        if indexPath.row == 29 {
            
            
            vc.paraNumber = "030"
            vc.current = 78
            vc.currentEng = 78
            vc.startAyat = 0
            vc.ArrSurah.append(40)
            vc.ArrSurah.append(46)
            vc.ArrSurah.append(42)
            vc.ArrSurah.append(29)
            vc.ArrSurah.append(19)
            vc.ArrSurah.append(36)
            vc.ArrSurah.append(25)
            vc.ArrSurah.append(22)
            vc.ArrSurah.append(17)
            vc.ArrSurah.append(19)
            vc.ArrSurah.append(26)
            vc.ArrSurah.append(30)
            vc.ArrSurah.append(20)
            vc.ArrSurah.append(15)
            vc.ArrSurah.append(21)
            vc.ArrSurah.append(11)
            vc.ArrSurah.append(8)
            vc.ArrSurah.append(8)
            vc.ArrSurah.append(19)
            vc.ArrSurah.append(5)
            vc.ArrSurah.append(8)
            vc.ArrSurah.append(8)
            vc.ArrSurah.append(11)
            vc.ArrSurah.append(11)
            vc.ArrSurah.append(8)
            vc.ArrSurah.append(3)
            vc.ArrSurah.append(9)
            vc.ArrSurah.append(5)
            vc.ArrSurah.append(4)
            vc.ArrSurah.append(7)
            vc.ArrSurah.append(3)
            vc.ArrSurah.append(6)
            vc.ArrSurah.append(3)
            vc.ArrSurah.append(5)
            vc.ArrSurah.append(4)
            vc.ArrSurah.append(5)
            vc.ArrSurah.append(6)
            vc.suratCount = 37







                   vc.startInd = 5747
                   vc.endInd = 6348
            
            
               }
               
               
        
        
        
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

//extension SiparayViewController {
//
//    func getSiparayUrls() {
//            for i in 1..<6348 + 1{
//
//                var siparaNumber = ""
//                var url :String
//                if i < 7 + 1 {
//
//                    siparaNumber = "001"
//
//                    if i < 10{
//                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=00\(i)"
//                        quranUrls.append(url)
//
//                    }
//                    if i>=10 && i<100{
//                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=0\(i)"
//
//                        quranUrls.append(url)
//                    }
//
//                    if i >= 100{
//                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=\(i)"
//
//                           quranUrls.append(url)
//                    }
//
//
//
//                }
//
//                else if i < 286 + 1{
//
//                    siparaNumber = "002"
//
//                    if i < 10{
//                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=00\(i)"
//                        quranUrls.append(url)
//
//                    }
//                    if i>=10 && i<100{
//                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=0\(i)"
//
//                        quranUrls.append(url)
//                    }
//
//                    if i >= 100{
//                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=\(i)"
//
//                           quranUrls.append(url)
//                    }
//
//                }
//                else if i < 200 + 1{
//
//                   siparaNumber = "003"
//
//                   if i < 10{
//                       url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=00\(i)"
//                       quranUrls.append(url)
//
//                   }
//                   if i>=10 && i<100{
//                       url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=0\(i)"
//
//                       quranUrls.append(url)
//                   }
//
//                   if i >= 100{
//                       url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=\(i)"
//
//                          quranUrls.append(url)
//                   }
//
//               }
//                else if i < 176 + 1{
//
//                                   siparaNumber = "004"
//
//                                   if i < 10{
//                                       url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=00\(i)"
//                                       quranUrls.append(url)
//
//                                   }
//                                   if i>=10 && i<100{
//                                       url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=0\(i)"
//
//                                       quranUrls.append(url)
//                                   }
//
//                                   if i >= 100{
//                                       url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=\(i)"
//
//                                          quranUrls.append(url)
//                                   }
//
//                               }
//                else if i < 120 + 1{
//
//                      siparaNumber = "005"
//
//                      if i < 10{
//                          url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=00\(i)"
//                          quranUrls.append(url)
//
//                      }
//                      if i>=10 && i<100{
//                          url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=0\(i)"
//
//                          quranUrls.append(url)
//                      }
//
//                      if i >= 100{
//                          url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=\(i)"
//
//                             quranUrls.append(url)
//                      }
//
//                  }
//
//                else if i < 165 + 1{
//
//
//                                                  siparaNumber = "006"
//
//                                                  if i < 10{
//                                                      url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=00\(i)"
//                                                      quranUrls.append(url)
//
//                                                  }
//                                                  if i>=10 && i<100{
//                                                      url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=0\(i)"
//
//                                                      quranUrls.append(url)
//                                                  }
//
//                                                  if i >= 100{
//                                                      url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=\(i)"
//
//                                                         quranUrls.append(url)
//                                                  }
//
//                                              }
//              else if i < 206 + 1{
//
//
//                                                siparaNumber = "007"
//
//                                                if i < 10{
//                                                    url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=00\(i)"
//                                                    quranUrls.append(url)
//
//                                                }
//                                                if i>=10 && i<100{
//                                                    url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=0\(i)"
//
//                                                    quranUrls.append(url)
//                                                }
//
//                                                if i >= 100{
//                                                    url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=\(i)"
//
//                                                       quranUrls.append(url)
//                                                }
//
//                                            }
//
//                else if i < 75 + 1{
//
//                    siparaNumber = "008"
//
//                    if i < 10{
//                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=00\(i)"
//                        quranUrls.append(url)
//
//                    }
//                    if i>=10 && i<100{
//                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=0\(i)"
//
//                        quranUrls.append(url)
//                    }
//
//                    if i >= 100{
//                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=\(i)"
//
//                           quranUrls.append(url)
//                    }
//
//                }
//
//                else if i < 129 + 1{
//
//
//                    siparaNumber = "009"
//
//                    if i < 10{
//                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=00\(i)"
//                        quranUrls.append(url)
//
//                    }
//                    if i>=10 && i<100{
//                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=0\(i)"
//
//                        quranUrls.append(url)
//                    }
//
//                    if i >= 100{
//                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=\(i)"
//
//                           quranUrls.append(url)
//                    }
//
//                }
//
//                else if i < 109 + 1{
//
//
//                    siparaNumber = "010"
//
//                    if i < 10{
//                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=00\(i)"
//                        quranUrls.append(url)
//
//                    }
//                    if i>=10 && i<100{
//                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=0\(i)"
//
//                        quranUrls.append(url)
//                    }
//
//                    if i >= 100{
//                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=\(i)"
//
//                           quranUrls.append(url)
//                    }
//
//                }
//
//                else if i < 123 + 1{
//
//
//                                   siparaNumber = "011"
//
//                                   if i < 10{
//                                       url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=00\(i)"
//                                       quranUrls.append(url)
//
//                                   }
//                                   if i>=10 && i<100{
//                                       url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=0\(i)"
//
//                                       quranUrls.append(url)
//                                   }
//
//                                   if i >= 100{
//                                       url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=\(i)"
//
//                                          quranUrls.append(url)
//                                   }
//
//                               }
//
//                else if i < 111 + 1{
//
//
//                    siparaNumber = "012"
//
//                    if i < 10{
//                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=00\(i)"
//                        quranUrls.append(url)
//
//                    }
//                    if i>=10 && i<100{
//                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=0\(i)"
//
//                        quranUrls.append(url)
//                    }
//
//                    if i >= 100{
//                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=\(i)"
//
//                           quranUrls.append(url)
//                    }
//
//                               }
//
//                else if i < 43 + 1{
//                     siparaNumber = "013"
//
//                                                    if i < 10{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=00\(i)"
//                                                        quranUrls.append(url)
//
//                                                    }
//                                                    if i>=10 && i<100{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=0\(i)"
//
//                                                        quranUrls.append(url)
//                                                    }
//
//                                                    if i >= 100{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=\(i)"
//
//                                                           quranUrls.append(url)
//                                                    }
//                               }
//
//                else if i < 52 + 1{
//                               siparaNumber = "014"
//
//                                                    if i < 10{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=00\(i)"
//                                                        quranUrls.append(url)
//
//                                                    }
//                                                    if i>=10 && i<100{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=0\(i)"
//
//                                                        quranUrls.append(url)
//                                                    }
//
//                                                    if i >= 100{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=\(i)"
//
//                                                           quranUrls.append(url)
//                                                    }
//                               }
//                else if i < 99 + 1{
//                      siparaNumber = "015"
//
//                                                    if i < 10{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=00\(i)"
//                                                        quranUrls.append(url)
//
//                                                    }
//                                                    if i>=10 && i<100{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=0\(i)"
//
//                                                        quranUrls.append(url)
//                                                    }
//
//                                                    if i >= 100{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=\(i)"
//
//                                                           quranUrls.append(url)
//                                                    }
//                               }
//
//                else if i < 128 + 1{
//                          siparaNumber = "016"
//
//                                                    if i < 10{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=00\(i)"
//                                                        quranUrls.append(url)
//
//                                                    }
//                                                    if i>=10 && i<100{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=0\(i)"
//
//                                                        quranUrls.append(url)
//                                                    }
//
//                                                    if i >= 100{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=\(i)"
//
//                                                           quranUrls.append(url)
//                                                    }
//                               }
//                else if i < 111 + 1{
//                         siparaNumber = "017"
//
//                                                    if i < 10{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=00\(i)"
//                                                        quranUrls.append(url)
//
//                                                    }
//                                                    if i>=10 && i<100{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=0\(i)"
//
//                                                        quranUrls.append(url)
//                                                    }
//
//                                                    if i >= 100{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=\(i)"
//
//                                                           quranUrls.append(url)
//                                                    }
//                               }
//
//                else if i < 110 + 1{
//                      siparaNumber = "018"
//
//                                                    if i < 10{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=00\(i)"
//                                                        quranUrls.append(url)
//
//                                                    }
//                                                    if i>=10 && i<100{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=0\(i)"
//
//                                                        quranUrls.append(url)
//                                                    }
//
//                                                    if i >= 100{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=\(i)"
//
//                                                           quranUrls.append(url)
//                                                    }
//                               }
//
//                else if i < 98 + 1{
//                   siparaNumber = "019"
//
//                                                    if i < 10{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=00\(i)"
//                                                        quranUrls.append(url)
//
//                                                    }
//                                                    if i>=10 && i<100{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=0\(i)"
//
//                                                        quranUrls.append(url)
//                                                    }
//
//                                                    if i >= 100{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=\(i)"
//
//                                                           quranUrls.append(url)
//                                                    }
//                               }
//
//                else if i < 135 + 1{
//                      siparaNumber = "020"
//
//                                                    if i < 10{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=00\(i)"
//                                                        quranUrls.append(url)
//
//                                                    }
//                                                    if i>=10 && i<100{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=0\(i)"
//
//                                                        quranUrls.append(url)
//                                                    }
//
//                                                    if i >= 100{
//                                                        url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=\(siparaNumber)&verse=\(i)"
//
//                                                           quranUrls.append(url)
//                                                    }
//                               }
//
//                else if i < 112 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=021&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//
//                else if i < 78 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=022&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//                 else if i < 118 + 1{
//                                    url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=023&verse=00\(i)"
//                                    quranUrls.append(url)
//                                }
//
//                else if i < 64 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=024&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//
//                else if i < 77 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=025&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//
//                else if i < 227 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=026&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//                else if i < 93 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=027&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//                else if i < 88 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=028&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//                else if i < 69 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=029&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//                else if i < 60 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=030&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//                else if i < 34 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=031&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//                else if i < 30 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=032&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//                else if i < 73 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=033&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//                else if i < 54 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=034&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//                else if i < 45 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=035&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//                else if i < 83 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=036&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//                else if i < 182 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=037&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//                else if i < 88 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=038&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//                else if i < 75 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=039&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//                else if i < 85 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=040&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//                else if i < 54 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=041&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//                else if i < 53 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=042&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//                else if i < 89 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=043&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//                else if i < 59 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=044&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//                else if i < 37 + 1{
//                                   url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=045&verse=00\(i)"
//                                   quranUrls.append(url)
//                               }
//
//            }
//        }
//
//
//}
