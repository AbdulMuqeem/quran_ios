//
//  ViewController.swift
//  TheQuran
//
//  Created by engitech on 03/03/2020.
//

import UIKit
import XLPagerTabStrip
import CoreData
import iProgressHUD

class ViewController: ButtonBarPagerTabStripViewController {
 let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var surahArr : [Any] = []
    let iprogress: iProgressHUD = iProgressHUD()

    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    let barColor = UIColor(red: 91/255.0, green: 37/255.0, blue: 145/255.0, alpha: 1)
    

    override func viewDidLoad() {
         self.configureButtonBar()
    
        super.viewDidLoad()
       
        
        self.navigationController?.navigationBar.barTintColor = barColor
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
   
        self.navigationController?.navigationBar.tintColor = .white
        
    }
//        let url = URL(string: "quran-simple-enhanced.txt")
//
//
//             let task = URLSession.shared.downloadTask(with: url!) { (urlresponse, response, error) in
//
//                 guard let orginalUrl = urlresponse else {return}
//
//                 do {
//                     //get path to directory
//                     let path = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
//                     //Giving name to file
//                     let newUrl = path.appendingPathComponent("myTextFile")
//                     //Move file from old to new url
//                     try FileManager.default.moveItem(at: orginalUrl, to: newUrl)
//                 } catch {print(error.localizedDescription); return}
//             }
//             task.resume()
        
     
 
                
        
        //self.loadDesign()
        
//        let line = UIView(frame: CGRect(x: 0, y: buttonBarView.frame.size.height - 1, width: buttonBarView.frame.size.height, height: 1))
//        line.backgroundColor = UIColor.green
//
//        buttonBarView.addSubview(line)
        
        
        // Do any additional setup after loading the view.
 


    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "VC1") as SiparayViewController
        let child2 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "VC2") as SuratViewController
        let child3 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "VC3") as SearchViewController
        return [child1,child2,child3]
    }

    func configureButtonBar() {
         // Sets the background colour of the pager strip and the pager strip item
        //settings.style.buttonBarBackgroundColor = .blue
         settings.style.buttonBarItemBackgroundColor = .white
        

         // Sets the pager strip item font and font color
       
        settings.style.buttonBarItemFont = .systemFont(ofSize: 17.0, weight: .regular)// UIFont(name: "Helvetica", size: 17.0)!
         settings.style.buttonBarItemTitleColor = .gray

         // Sets the pager strip item offsets
        settings.style.buttonBarMinimumLineSpacing = 0
         settings.style.buttonBarItemsShouldFillAvailableWidth = true
         settings.style.buttonBarLeftContentInset = 0
         settings.style.buttonBarRightContentInset = 0

         // Sets the height and colour of the slider bar of the selected pager tab
         settings.style.selectedBarHeight = 2
         settings.style.selectedBarBackgroundColor = .purple

         // Changing item text color on swipe
         changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
               guard changeCurrentIndex == true else { return }
            print(self!.buttonBarView)
               oldCell?.label.textColor = .gray
               newCell?.label.textColor = .purple
            
         }
    }
    
    
    func loadDesign()
    { // change selected bar color
    settings.style.buttonBarBackgroundColor = .white
    settings.style.buttonBarItemBackgroundColor = .white
    settings.style.selectedBarBackgroundColor = .purple
        settings.style.buttonBarMinimumInteritemSpacing = 0
    settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
    settings.style.selectedBarHeight = 0
    settings.style.buttonBarMinimumLineSpacing = 0
    settings.style.buttonBarItemTitleColor = barColor
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
   
    settings.style.buttonBarLeftContentInset = 0
    settings.style.buttonBarRightContentInset = 0
        
    changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
    guard changeCurrentIndex == true else { return }
    oldCell?.label.textColor = .black
        self!.showSpinner(view: self!.view)
        newCell?.label.textColor = .purple
    }
}
       func showSpinner(view : UIView) {
           
            
            
            iprogress.isShowModal = true
            iprogress.isShowCaption = true
            iprogress.isTouchDismiss = false
            iprogress.indicatorStyle = .circleStrokeSpin
            iprogress.indicatorColor = .white
            iprogress.iprogressStyle = .horizontal
            iprogress.indicatorView.startAnimating()
            iprogress.attachProgress(toView: view)
            view.showProgress()
            
        }
        func hideSpinner(view : UIView) {
            iprogress.indicatorView.stopAnimating()
            view.dismissProgress()
        }
    

}
