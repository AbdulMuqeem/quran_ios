//
//  SuratViewController.swift
//  TheQuran
//
//  Created by engitech on 04/03/2020.
//

import UIKit
import XLPagerTabStrip
import AVFoundation
import AVKit
import Alamofire

class SuratViewController: UIViewController,AVAudioPlayerDelegate {
    
    @IBOutlet weak var tblSurah: UITableView!
    var audioPlayer = AVPlayer()
    var audioFile:String!
    var surahArr : [Any] = []
    var constant = Constants()
    override func viewDidLoad() {
        
        super.viewDidLoad()
        tblSurah.delegate = self
        tblSurah.dataSource = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        //getAllMedia()
    }
    //    responseData(completionHandler: { (dataResponse: AFDataResponse<Data>) in }
    
    
    public func Request_Api(url:String, methodType : HTTPMethod, completion: @escaping (_ result: AFDataResponse<Data>) -> Void) {
        
        
        
        let request = AF.request(url)
        // 2
        request.responseJSON { (data) in
            print(data)
            let responseData = data.value as! NSDictionary
            print(responseData)
            let audioDict = responseData["data"] as! NSDictionary
            let audio = audioDict["audio"] as! String
            self.audioFile = audio
            
        }
        
        
    }
    func getAllMedia(){
        
        
        
        //        let url = "http://anaxdesigns.website/quran/?get=verse&lang=english&reader=default&chapter=001&verse=001"
        
        let url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=001&verse=001"
        
        print(url)
        
        
        
        self.Request_Api(url: url, methodType: .get)    {
            response in
            print(response)
            if response.value == nil {
                print("No response")
                
                return
            }
            else {
                let responseData = response.value as! NSDictionary
                print(responseData)
                
            }
            
        }
        
    }
    
}

extension SuratViewController: IndicatorInfoProvider{
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "SURAH")
    }
    
    
}
// implement tableview delegate and datasource methods
extension SuratViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return constant.suratArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuratCell", for: indexPath) as! SuratTableViewCell
        let dict = constant.suratArray[indexPath.row]
        cell.numberLabel.text = (dict["number"] as! String)
        cell.suratNameLabel.text = (dict["suratName"] as! String)
        cell.suratVersesLabel.text = (dict["suratVerses"] as! String)
        cell.suratMeaningLabel.text = (dict["suratMeaning"] as! String)
        cell.btnAudio.tag = indexPath.row
        cell.btnAudio.addTarget(self, action: #selector(btnplay(sender:)), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
        
    }
    @objc func btnplay(sender: UIButton){
        
        //        let dict = "http://anaxdesigns.website/quran/?get=verse&lang=english&reader=default&chapter=001&verse=001"
        
        let Dict = NSURL(string: self.audioFile)
        
        let playerItem =  AVPlayerItem(url: Dict! as URL)
        
        audioPlayer = AVPlayer(playerItem: playerItem)
        audioPlayer.volume = 300.0
        audioPlayer.play()
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "specSuratVC") as! specSuratViewController
        
        //let indexPath = IndexPath(row: 0, section: 0)
        let cell = tableView.cellForRow(at: indexPath) as! SuratTableViewCell
        
       
        vc.navigationItem.title = "Surah " + cell.suratNameLabel.text!
        vc.modalPresentationStyle = .fullScreen
        
        vc.fromVC = "Surat"
        if indexPath.row == 0{
            
            vc.ayatCount = 7
            vc.suratNumber = "001"
            vc.startInd = 0
            vc.endInd = 6
            vc.current = 1
            //            vc.startIndexForEnglish = 0
            //            vc.endIndexForEnglish = 6
            
        }
        if indexPath.row == 1{
            
            vc.ayatCount = 286
            vc.suratNumber = "002"
            vc.current = 2
            vc.startInd = 7
            vc.endInd = 293
            
            //            vc.startIndexForEnglish = 6
            //            vc.endIndexForEnglish = 293
            
            
        }
        if indexPath.row == 2{
            
            vc.ayatCount = 200
            vc.suratNumber = "003"
            vc.current = 3
            vc.startInd = 294
            vc.endInd = 494
            
            //            vc.startIndexForEnglish = 295
            //            vc.endIndexForEnglish = 496
            
        }
        if indexPath.row == 3{
            
            
            vc.ayatCount = 176
            vc.suratNumber = "004"
            vc.current = 4
            
            vc.startInd = 495
            vc.endInd = 671
            
            //            vc.startIndexForEnglish = 497
            //            vc.endIndexForEnglish = 674
            
        }
        if indexPath.row == 4{
            
            vc.ayatCount = 120
            vc.suratNumber = "005"
            vc.current = 5
            
            vc.startInd = 672
            vc.endInd = 792
            
            //            vc.startIndexForEnglish = 675
            //            vc.endIndexForEnglish = 797
            
        }
        
        
        if indexPath.row == 5{
            
            vc.ayatCount = 165
            vc.suratNumber = "006"
            vc.current = 6
            
            vc.startInd = 793
            vc.endInd = 958
            
            //            vc.startIndexForEnglish = 798
            //            vc.endIndexForEnglish = 963
        }
        if indexPath.row == 6{
            
            vc.ayatCount = 206
            vc.suratNumber = "007"
            vc.current = 7
            
            vc.startInd = 959
            vc.endInd = 1165
            
            //            vc.startIndexForEnglish = 964
            //            vc.endIndexForEnglish = 1171
        }
        if indexPath.row == 7{
            
            
            vc.ayatCount = 75
            vc.suratNumber = "008"
            vc.current = 8
            
            vc.startInd = 1166
            vc.endInd = 1241
            
            //            vc.startIndexForEnglish = 1172
            //            vc.endIndexForEnglish = 1248
            
        }
        if indexPath.row == 8{
            
            vc.ayatCount = 129
            vc.suratNumber = "009"
            vc.current = 9
            
            vc.startInd = 1242
            vc.endInd = 1370
            
            //            vc.startIndexForEnglish = 1249
            //            vc.endIndexForEnglish = 1378
            
        }
        if indexPath.row == 9{
            
            vc.current = 10
            
            vc.ayatCount = 109
            vc.suratNumber = "010"
            
            vc.startInd = 1371
            vc.endInd = 1480
            
            //            vc.startIndexForEnglish = 1379
            //            vc.endIndexForEnglish = 1489
            
            
        }
        if indexPath.row == 10{
            vc.current = 11
            
            vc.ayatCount = 123
            vc.suratNumber = "011"
            
            vc.startInd = 1481
            vc.endInd = 1604
            
            //            vc.startIndexForEnglish = 1490
            //            vc.endIndexForEnglish = 1614
            
        }
        if indexPath.row == 11{
            vc.current = 12
            
            vc.ayatCount = 111
            vc.suratNumber = "012"
            
            vc.startInd = 1605
            vc.endInd = 1716
            
            //            vc.startIndexForEnglish = 1615
            //            vc.endIndexForEnglish = 1727
            
        }
        if indexPath.row == 12{
            
            vc.current = 13
            
            vc.ayatCount = 43
            vc.suratNumber = "013"
            
            vc.startInd = 3436
            vc.endInd = 3524
            
            //            vc.startIndexForEnglish = 1717
            //            vc.endIndexForEnglish = 1760
            
        }
        if indexPath.row == 13{
            vc.current = 14
            
            vc.ayatCount = 52
            vc.suratNumber = "014"
            
            vc.startInd = 1761
            vc.endInd = 1813
            
            //            vc.startIndexForEnglish = 1773
            //            vc.endIndexForEnglish = 1826
            
        }
        if indexPath.row == 14{
            vc.current = 15
            
            vc.ayatCount = 99
            vc.suratNumber = "015"
            
            vc.startInd = 1814
            vc.endInd = 1913
            
            //            vc.startIndexForEnglish = 1827
            //            vc.endIndexForEnglish = 1927
            
        }
        if indexPath.row == 15{
            vc.current = 16
            
            vc.ayatCount = 128
            vc.suratNumber = "016"
            
            vc.startInd = 1914
            vc.endInd = 2042
            
            //            vc.startIndexForEnglish = 1928
            //            vc.endIndexForEnglish = 2057
            
        }
        if indexPath.row == 16{
            
            vc.current = 17
            
            vc.ayatCount = 111
            vc.suratNumber = "017"
            
            vc.startInd = 2043
            vc.endInd = 2154
            
            //            vc.startIndexForEnglish = 2058
            //            vc.endIndexForEnglish = 2170
            
        }
        if indexPath.row == 17{
            
            vc.ayatCount = 110
            vc.suratNumber = "018"
            vc.current = 18
            
            vc.startInd = 2155
            vc.endInd = 2265
            
            //            vc.startIndexForEnglish = 2171
            //            vc.endIndexForEnglish = 2282
            //
        }
        if indexPath.row == 18{
            vc.current = 19
            
            
            vc.ayatCount = 98
            vc.suratNumber = "019"
            
            vc.startInd = 2266
            vc.endInd = 2364
            
            //            vc.startIndexForEnglish = 2283
            //            vc.endIndexForEnglish = 2382
            
        }
        if indexPath.row == 19{
            
            vc.current = 20
            
            vc.ayatCount = 135
            vc.suratNumber = "020"
            
            vc.startInd = 2365
            vc.endInd = 2500
            
            //            vc.startIndexForEnglish = 2383
            //            vc.endIndexForEnglish = 2519
            
        }
        if indexPath.row == 20{
            vc.current = 21
            
            vc.ayatCount = 112
            vc.suratNumber = "021"
            
            vc.startInd = 2501
            vc.endInd = 2613
            
            //            vc.startIndexForEnglish = 2520
            //            vc.endIndexForEnglish = 2633
            
        }
        if indexPath.row == 21{
            vc.current = 22
            
            vc.ayatCount = 78
            vc.suratNumber = "022"
            
            vc.startInd = 2614
            vc.endInd = 2692
            
            //            vc.startIndexForEnglish = 2634
            //            vc.endIndexForEnglish = 2713
            
        }
        if indexPath.row == 22{
            
            vc.ayatCount = 118
            vc.suratNumber = "023"
            vc.current = 23
            
            vc.startInd = 2693
            vc.endInd = 2811
            
            //            vc.startIndexForEnglish = 2714
            //            vc.endIndexForEnglish = 2833
            
        }
        if indexPath.row == 23{
            
            vc.ayatCount = 64
            vc.suratNumber = "024"
            vc.current = 24
            
            vc.startInd = 2812
            vc.endInd = 2876
            
            //            vc.startIndexForEnglish = 2834
            //            vc.endIndexForEnglish = 2899
            
        }
        if indexPath.row == 24{
            
            vc.ayatCount = 77
            vc.suratNumber = "025"
            vc.current = 25
            
            vc.startInd = 2877
            vc.endInd = 2954
            
            //            vc.startIndexForEnglish = 2900
            //            vc.endIndexForEnglish = 2978
            
        }
        if indexPath.row == 25{
            vc.current = 26
            
            vc.ayatCount = 227
            vc.suratNumber = "026"
            
            vc.startInd = 2955
            vc.endInd = 3182
            
            //            vc.startIndexForEnglish = 2979
            //            vc.endIndexForEnglish = 3207
            //
        }
        if indexPath.row == 26{
            
            vc.current = 27
            
            vc.ayatCount = 93
            vc.suratNumber = "027"
            
            
            vc.startInd = 3183
            vc.endInd = 3276
            
            //            vc.startIndexForEnglish = 3208
            //            vc.endIndexForEnglish = 3302
            
        }
        if indexPath.row == 27{
            vc.current = 28
            
            vc.ayatCount = 88
            vc.suratNumber = "028"
            
            vc.startInd = 3277
            vc.endInd = 3365
            
            //            vc.startIndexForEnglish = 3303
            //            vc.endIndexForEnglish = 3392
            
        }
        if indexPath.row == 28{
            vc.current = 29
            
            vc.ayatCount = 69
            vc.suratNumber = "029"
            
            vc.startInd = 3366
            vc.endInd = 3435
            
            //            vc.startIndexForEnglish = 3393
            //            vc.endIndexForEnglish = 3463
            
        }
        if indexPath.row == 29{
            
            vc.current = 30
            
            vc.ayatCount = 60
            vc.suratNumber = "030"
            vc.startInd = 3436
            vc.endInd = 3496
            
            //            vc.startIndexForEnglish = 3464
            //            vc.endIndexForEnglish = 3525
            
        }
        if indexPath.row == 30{
            vc.current = 31
            
            vc.ayatCount = 34
            vc.suratNumber = "031"
            
            vc.startInd = 3497
            vc.endInd = 3531
            
            //            vc.startIndexForEnglish = 3526
            //            vc.endIndexForEnglish = 3561
            
        }
        
        
        if indexPath.row == 31{
            vc.current = 32
            
            vc.ayatCount = 30
            vc.suratNumber = "032"
            
            vc.startInd = 3532
            vc.endInd = 3562
            
            //            vc.startIndexForEnglish = 3562
            //            vc.endIndexForEnglish = 3593
            
        }
        if indexPath.row == 32{
            vc.current = 33
            
            vc.ayatCount = 73
            vc.suratNumber = "033"
            
            vc.startInd = 3563
            vc.endInd = 3636
            
            //            vc.startIndexForEnglish = 3594
            //            vc.endIndexForEnglish = 3668
            
        }
        if indexPath.row == 33{
            vc.current = 34
            
            vc.ayatCount = 54
            vc.suratNumber = "034"
            
            vc.startInd = 3637
            vc.endInd = 3691
            
            //            vc.startIndexForEnglish = 3669
            //            vc.endIndexForEnglish = 3724
            
        }
        if indexPath.row == 34{
            vc.current = 35
            
            vc.ayatCount = 45
            vc.suratNumber = "035"
            
            vc.startInd = 3692
            vc.endInd = 3737
            
            //            vc.startIndexForEnglish = 3725
            //            vc.endIndexForEnglish = 3771
            
        }
        if indexPath.row == 35{
            vc.current = 36
            
            vc.ayatCount = 83
            vc.suratNumber = "036"
            
            vc.startInd = 3738
            vc.endInd = 3821
            
            //            vc.startIndexForEnglish = 3772
            //            vc.endIndexForEnglish = 3856
            //
        }
        if indexPath.row == 36{
            vc.current = indexPath.row + 1
            vc.ayatCount = 182
            vc.suratNumber = "037"
            
            vc.startInd = 3822
            vc.endInd = 4004
            
            //            vc.startIndexForEnglish = 3857
            //            vc.endIndexForEnglish = 4040
            
        }
        if indexPath.row == 37{
            vc.current = indexPath.row + 1
            
            
            vc.ayatCount = 88
            vc.suratNumber = "038"
            vc.startInd = 4005
            vc.endInd = 4093
            
            //            vc.startIndexForEnglish = 4041
            //            vc.endIndexForEnglish = 4130
            
        }
        if indexPath.row == 38{
            
            vc.ayatCount = 75
            vc.current = indexPath.row + 1
            
            vc.suratNumber = "039"
            
            vc.startInd = 4094
            vc.endInd = 4169
            
            //            vc.startIndexForEnglish = 4131
            //            vc.endIndexForEnglish = 4207
            
        }
        if indexPath.row == 39{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 85
            vc.suratNumber = "040"
            
            vc.startInd = 4170
            vc.endInd = 4255
            
            //            vc.startIndexForEnglish = 4208
            //            vc.endIndexForEnglish = 4294
            
        }
        if indexPath.row == 40{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 54
            vc.suratNumber = "041"
            
            vc.startInd = 4256
            vc.endInd = 4310
            
            //            vc.startIndexForEnglish = 4295
            //            vc.endIndexForEnglish = 4350
            
        }
        if indexPath.row == 41{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 53
            vc.suratNumber = "042"
            
            vc.startInd = 4311
            vc.endInd = 4364
            //
            //            vc.startIndexForEnglish = 4351
            //            vc.endIndexForEnglish = 4405
            
        }
        if indexPath.row == 42{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 89
            vc.suratNumber = "043"
            vc.startInd = 4365
            vc.endInd = 4454
            
            //            vc.startIndexForEnglish = 4406
            //            vc.endIndexForEnglish = 4496
            
        }
        if indexPath.row == 43{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 59
            vc.suratNumber = "044"
            
            vc.startInd = 4455
            vc.endInd = 4514
            
            //            vc.startIndexForEnglish = 4497
            //            vc.endIndexForEnglish = 4557
            
        }
        if indexPath.row == 44{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 37
            vc.suratNumber = "045"
            
            vc.startInd = 4515
            vc.endInd = 4552
            
            //            vc.startIndexForEnglish = 4558
            //            vc.endIndexForEnglish = 4596
            
        }
        if indexPath.row == 45{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 35
            vc.suratNumber = "046"
            
            vc.startInd = 4553
            vc.endInd = 4588
            
            //            vc.startIndexForEnglish = 4597
            //            vc.endIndexForEnglish = 4633
            
        }
        if indexPath.row == 46{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 38
            vc.suratNumber = "047"
            
            vc.startInd = 4589
            vc.endInd = 4627
            
            //            vc.startIndexForEnglish = 4634
            //            vc.endIndexForEnglish = 4673
            
        }
        if indexPath.row == 47{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 29
            vc.suratNumber = "048"
            
            vc.startInd = 4628
            vc.endInd = 4657
            
            //            vc.startIndexForEnglish = 4674
            //            vc.endIndexForEnglish = 4704
            //
        }
        if indexPath.row == 48{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 18
            vc.suratNumber = "049"
            vc.startInd = 4658
            vc.endInd = 4676
            
            //            vc.startIndexForEnglish = 4705
            //            vc.endIndexForEnglish = 4724
            
        }
        if indexPath.row == 49{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 45
            vc.suratNumber = "050"
            
            vc.startInd = 4677
            vc.endInd = 4722
            
            //            vc.startIndexForEnglish = 4725
            //            vc.endIndexForEnglish = 4771
            
        }
        if indexPath.row == 50{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 60
            vc.suratNumber = "051"
            
            vc.startInd = 4723
            vc.endInd = 4783
            
            //            vc.startIndexForEnglish = 4772
            //            vc.endIndexForEnglish = 4833
            //
        }
        if indexPath.row == 51{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 49
            vc.suratNumber = "052"
            
            vc.startInd = 4784
            vc.endInd = 4833
            
            //            vc.startIndexForEnglish = 4834
            //            vc.endIndexForEnglish = 4884
            
        }
        if indexPath.row == 52{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 62
            vc.suratNumber = "053"
            
            vc.startInd = 4834
            vc.endInd = 4896
            
            //            vc.startIndexForEnglish = 4885
            //            vc.endIndexForEnglish = 4948
            
        }
        if indexPath.row == 53{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 55
            vc.suratNumber = "054"
            
            vc.startInd = 4897
            vc.endInd = 4952
            
            //            vc.startIndexForEnglish = 4949
            //            vc.endIndexForEnglish = 5005
            
        }
        if indexPath.row == 54{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 78
            vc.suratNumber = "055"
            
            vc.startInd = 4953
            vc.endInd = 5031
            //
            //            vc.startIndexForEnglish = 5006
            //            vc.endIndexForEnglish = 5085
            
        }
        if indexPath.row == 55{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 96
            vc.suratNumber = "056"
            
            vc.startInd = 5032
            vc.endInd = 5128
            
            //            vc.startIndexForEnglish = 5086
            //            vc.endIndexForEnglish = 5183
            
        }
        if indexPath.row == 56{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 29
            vc.suratNumber = "057"
            
            vc.startInd = 5129
            vc.endInd = 5158
            //
            //            vc.startIndexForEnglish = 5184
            //            vc.endIndexForEnglish = 5214
            
        }
        if indexPath.row == 57{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 22
            vc.suratNumber = "058"
            
            vc.startInd = 5159
            vc.endInd = 5181
            
            //            vc.startIndexForEnglish = 5215
            //            vc.endIndexForEnglish = 5238
            
        }
        if indexPath.row == 58{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 24
            vc.suratNumber = "059"
            
            vc.startInd = 5182
            vc.endInd = 5206
            
            //            vc.startIndexForEnglish = 5239
            //            vc.endIndexForEnglish = 5264
            //
        }
        if indexPath.row == 59{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 13
            vc.suratNumber = "060"
            
            vc.startInd = 5207
            vc.endInd = 5220
            
            //            vc.startIndexForEnglish = 5265
            //            vc.endIndexForEnglish = 5279
            
            //
            
        }
        if indexPath.row == 60{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 14
            vc.suratNumber = "061"
            
            vc.startInd = 5221
            vc.endInd = 5235
            
            //            vc.startIndexForEnglish = 5280
            //            vc.endIndexForEnglish = 5295
            
        }
        if indexPath.row == 61{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 11
            vc.suratNumber = "062"
            vc.startInd = 5236
            vc.endInd = 5247
            
            //            vc.startIndexForEnglish = 5296
            //            vc.endIndexForEnglish = 5308
            
        }
        if indexPath.row == 62{
            vc.current = indexPath.row + 1
            
            
            vc.ayatCount = 11
            vc.suratNumber = "063"
            vc.startInd = 5248
            vc.endInd = 5259
            
            //            vc.startIndexForEnglish = 5309
            //            vc.endIndexForEnglish = 5321
            
        }
        if indexPath.row == 63{
            vc.current = indexPath.row + 1
            
            
            vc.ayatCount = 18
            vc.suratNumber = "064"
            vc.startInd = 5260
            vc.endInd = 5278
            
            //            vc.startIndexForEnglish = 5322
            //            vc.endIndexForEnglish = 5341
            //
            //                                                 }
        }
        if indexPath.row == 64{
            vc.current = indexPath.row + 1
            
            
            vc.ayatCount = 12
            vc.suratNumber = "065"
            
            vc.startInd = 5279
            vc.endInd = 5291
            
            //            vc.startIndexForEnglish = 5342
            //            vc.endIndexForEnglish = 5355
            
        }
        if indexPath.row == 65{
            
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 12
            vc.suratNumber = "066"
            vc.startInd = 5292
            vc.endInd = 5304
            
            //            vc.startIndexForEnglish = 5356
            //            vc.endIndexForEnglish = 5369
            
        }
        if indexPath.row == 66{
            
            vc.ayatCount = 30
            vc.suratNumber = "067"
            
            vc.current = indexPath.row + 1
            vc.startInd = 5305
            vc.endInd = 5335
            
            //            vc.startIndexForEnglish = 5370
            //            vc.endIndexForEnglish = 5401
            
        }
        if indexPath.row == 67{
            
            vc.ayatCount = 52
            vc.suratNumber = "068"
            vc.current = indexPath.row + 1
            
            vc.startInd = 5336
            vc.endInd = 5388
            
            //            vc.startIndexForEnglish = 5402
            //            vc.endIndexForEnglish = 5455
            
        }
        if indexPath.row == 68{
            vc.current = indexPath.row + 1
            
            
            vc.ayatCount = 52
            vc.suratNumber = "069"
            vc.startInd = 5389
            vc.endInd = 5441
            
            //            vc.startIndexForEnglish = 5456
            //            vc.endIndexForEnglish = 5509
            
        }
        if indexPath.row == 69{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 44
            vc.suratNumber = "070"
            vc.startInd = 5442
            vc.endInd = 5486
            
            //            vc.startIndexForEnglish = 5510
            //            vc.endIndexForEnglish = 5555
            //
        }
        if indexPath.row == 70{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 28
            vc.suratNumber = "071"
            vc.startInd = 5487
            vc.endInd = 5515
            
            //            vc.startIndexForEnglish = 5556
            //            vc.endIndexForEnglish = 5585
            //
        }
        if indexPath.row == 71{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 28
            vc.suratNumber = "072"
            
            vc.startInd = 5516
            vc.endInd = 5544
            
            
            //            vc.startIndexForEnglish = 5586
            //            vc.endIndexForEnglish = 5615
            
        }
        if indexPath.row == 72{
            
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 20
            vc.suratNumber = "073"
            vc.startInd = 5545
            vc.endInd = 5565
            
            //            vc.startIndexForEnglish = 5616
            //            vc.endIndexForEnglish = 5637
            
        }
        if indexPath.row == 73{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 56
            vc.suratNumber = "074"
            
            vc.startInd = 5566
            vc.endInd = 5622
            
            //            vc.startIndexForEnglish = 5638
            //            vc.endIndexForEnglish = 5695
            
        }
        if indexPath.row == 74{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 40
            vc.suratNumber = "075"
            vc.startInd = 5623
            vc.endInd = 5663
            //
            //            vc.startIndexForEnglish = 5696
            //            vc.endIndexForEnglish = 5737
            //
        }
        if indexPath.row == 75{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 31
            vc.suratNumber = "076"
            vc.startInd = 5664
            vc.endInd = 5695
            
            //            vc.startIndexForEnglish = 5738
            //            vc.endIndexForEnglish = 5770
            
        }
        if indexPath.row == 76{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 50
            vc.suratNumber = "077"
            
            vc.startInd = 5696
            vc.endInd = 5746
            
            //            vc.startIndexForEnglish = 5771
            //            vc.endIndexForEnglish = 5822
            
        }
        if indexPath.row == 77{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 40
            vc.suratNumber = "078"
            vc.startInd = 5747
            vc.endInd = 5787
            
            //            vc.startIndexForEnglish = 5823
            //            vc.endIndexForEnglish = 5864
            
        }
        if indexPath.row == 78{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 46
            vc.suratNumber = "079"
            
            vc.startInd = 5788
            vc.endInd = 5834
            
            //            vc.startIndexForEnglish = 5865
            //            vc.endIndexForEnglish = 5912
            
        }
        if indexPath.row == 79{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 42
            vc.suratNumber = "080"
            
            vc.startInd = 5835
            vc.endInd = 5877
            
            //            vc.startIndexForEnglish = 5913
            //            vc.endIndexForEnglish = 5956
            
        }
        
        if indexPath.row == 80{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 29
            vc.suratNumber = "081"
            
            vc.startInd = 5878
            vc.endInd = 5907
            
            //            vc.startIndexForEnglish = 5957
            //            vc.endIndexForEnglish = 5987
            
        }
        if indexPath.row == 81{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 19
            vc.suratNumber = "082"
            
            vc.startInd = 5908
            vc.endInd = 5927
            
            //            vc.startIndexForEnglish = 5988
            //            vc.endIndexForEnglish = 6008
            
        }
        if indexPath.row == 82{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 36
            vc.suratNumber = "083"
            vc.startInd = 5928
            vc.endInd = 5964
            
            //            vc.startIndexForEnglish = 6009
            //            vc.endIndexForEnglish = 6046
            
        }
        if indexPath.row == 83{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 25
            vc.suratNumber = "084"
            
            vc.startInd = 5965
            vc.endInd = 5990
            
            //            vc.startIndexForEnglish = 6047
            //            vc.endIndexForEnglish = 6073
            //
        }
        if indexPath.row == 84{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 22
            vc.suratNumber = "085"
            
            vc.startInd = 5991
            vc.endInd = 6013
            
            //            vc.startIndexForEnglish = 6074
            //            vc.endIndexForEnglish = 6097
            //
        }
        if indexPath.row == 85{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 17
            vc.suratNumber = "086"
            
            vc.startInd = 6014
            vc.endInd = 6031
            
            //            vc.startIndexForEnglish = 6098
            //            vc.endIndexForEnglish = 6116
            
        }
        if indexPath.row == 86{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 19
            vc.suratNumber = "087"
            
            vc.startInd = 6032
            vc.endInd = 6051
            
            //            vc.startIndexForEnglish = 6117
            //            vc.endIndexForEnglish = 6137
            
        }
        if indexPath.row == 87{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 26
            vc.suratNumber = "088"
            vc.startInd = 6052
            vc.endInd = 6078
            //
            //            vc.startIndexForEnglish = 6138
            //            vc.endIndexForEnglish = 6165
            
        }
        if indexPath.row == 88{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 30
            vc.suratNumber = "089"
            vc.startInd = 6079
            vc.endInd = 6109
            
            //            vc.startIndexForEnglish = 6166
            //            vc.endIndexForEnglish = 6197
            
        }
        if indexPath.row == 89{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 20
            vc.suratNumber = "090"
            
            vc.startInd = 6110
            vc.endInd = 6130
            //
            //            vc.startIndexForEnglish = 6198
            //            vc.endIndexForEnglish = 6219
            //
        }
        if indexPath.row == 90{
            vc.current = indexPath.row + 1
            
            
            vc.ayatCount = 15
            vc.suratNumber = "091"
            vc.startInd = 6131
            vc.endInd = 6146
            
            //            vc.startIndexForEnglish = 6220
            //            vc.endIndexForEnglish = 6236
            //
        }
        if indexPath.row == 91{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 21
            vc.suratNumber = "092"
            
            vc.startInd = 6147
            vc.endInd = 6168
            
            //            vc.startIndexForEnglish = 6237
            //            vc.endIndexForEnglish = 6259
            
        }
        if indexPath.row == 92{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 11
            vc.suratNumber = "093"
            
            vc.startInd = 6169
            vc.endInd = 6180
            
            //            vc.startIndexForEnglish = 6260
            //            vc.endIndexForEnglish = 6272
            
        }
        if indexPath.row == 93{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 8
            vc.suratNumber = "094"
            
            vc.startInd = 6181
            vc.endInd = 6189
            
            //            vc.startIndexForEnglish = 6273
            //            vc.endIndexForEnglish = 6282
            
        }
        if indexPath.row == 94{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 8
            vc.suratNumber = "095"
            vc.startInd = 6190
            vc.endInd = 6198
            
            //            vc.startIndexForEnglish = 6283
            //            vc.endIndexForEnglish = 6292
            
        }
        if indexPath.row == 95{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 19
            vc.suratNumber = "096"
            
            vc.startInd = 6199
            vc.endInd = 6218
            
            //            vc.startIndexForEnglish = 6293
            //            vc.endIndexForEnglish = 6313
            
        }
        if indexPath.row == 96{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 5
            vc.suratNumber = "097"
            vc.startInd = 6219
            vc.endInd = 6224
            
            //            vc.startIndexForEnglish = 6314
            //            vc.endIndexForEnglish = 6320
            //
        }
        if indexPath.row == 97{
            
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 8
            vc.suratNumber = "098"
            vc.startInd = 6225
            vc.endInd = 6233
            
            //            vc.startIndexForEnglish = 6321
            //            vc.endIndexForEnglish = 6330
            //
        }
        if indexPath.row == 98{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 8
            vc.suratNumber = "099"
            
            vc.startInd = 6234
            vc.endInd = 6242
            
            //            vc.startIndexForEnglish = 6331
            //            vc.endIndexForEnglish = 6340
            //
        }
        if indexPath.row == 99{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 11
            vc.suratNumber = "100"
            
            vc.startInd = 6243
            vc.endInd = 6254
            
            //            vc.startIndexForEnglish = 6341
            //            vc.endIndexForEnglish = 6353
            
        }
        if indexPath.row == 100{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 11
            vc.suratNumber = "101"
            
            vc.startInd = 6255
            vc.endInd = 6266
            
            //            vc.startIndexForEnglish = 6354
            //            vc.endIndexForEnglish = 6366
            
        }
        if indexPath.row == 101{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 8
            vc.suratNumber = "102"
            
            vc.startInd = 6267
            vc.endInd = 6275
            
            //            vc.startIndexForEnglish = 6367
            //            vc.endIndexForEnglish = 6376
            
        }
        if indexPath.row == 102{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 3
            vc.suratNumber = "103"
            
            vc.startInd = 6276
            vc.endInd = 6279
            
            //            vc.startIndexForEnglish = 6377
            //            vc.endIndexForEnglish = 6381
            //
        }
        if indexPath.row == 103{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 9
            vc.suratNumber = "104"
            
            vc.startInd = 6280
            vc.endInd = 6289
            
            //            vc.startIndexForEnglish = 6382
            //            vc.endIndexForEnglish = 6392
            
        }
        if indexPath.row == 104{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 5
            vc.suratNumber = "105"
            
            vc.startInd = 6290
            vc.endInd = 6295
            
            //            vc.startIndexForEnglish = 6393
            //            vc.endIndexForEnglish = 6399
            //
        }
        if indexPath.row == 105{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 4
            vc.suratNumber = "106"
            
            vc.startInd = 6296
            vc.endInd = 6300
            
            //                  vc.startIndexForEnglish = 6400
            //                  vc.endIndexForEnglish = 6405
            //
        }
        
        
        if indexPath.row == 106{
            
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 7
            vc.suratNumber = "107"
            vc.startInd = 6301
            vc.endInd = 6308
            
            //            vc.startIndexForEnglish = 6406
            //            vc.endIndexForEnglish = 6414
            
        }
        if indexPath.row == 107{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 3
            vc.suratNumber = "108"
            
            vc.startInd = 6309
            vc.endInd = 6312
            
            //            vc.startIndexForEnglish = 6415
            //            vc.endIndexForEnglish = 6419
            
        }
        if indexPath.row == 108{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 6
            vc.suratNumber = "109"
            
            vc.startInd = 6313
            vc.endInd = 6319
            
            //            vc.startIndexForEnglish = 6420
            //            vc.endIndexForEnglish = 6427
            //
        }
        if indexPath.row == 109{
            
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 3
            vc.suratNumber = "110"
            vc.startInd = 6320
            vc.endInd = 6323
            
            //            vc.startIndexForEnglish = 6428
            //            vc.endIndexForEnglish = 6432
            
        }
        if indexPath.row == 110{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 5
            vc.suratNumber = "111"
            vc.startInd = 6324
            vc.endInd = 6329
            //
            //                  vc.startIndexForEnglish = 6433
            //                  vc.endIndexForEnglish = 6439
            //
        }
        if indexPath.row == 111{
            vc.current = indexPath.row + 1
            
            
            vc.ayatCount = 4
            vc.suratNumber = "112"
            vc.startInd = 6330
            vc.endInd = 6334
            
            //            vc.startIndexForEnglish = 6440
            //            vc.endIndexForEnglish = 6445
            
        }
        if indexPath.row == 112{
            vc.current = indexPath.row + 1
            
            vc.ayatCount = 5
            vc.suratNumber = "113"
            
            vc.startInd = 6335
            vc.endInd = 6340
            
            //            vc.startIndexForEnglish = 6446
            //            vc.endIndexForEnglish = 6452
            
        }
        if indexPath.row == 113{
            vc.current = indexPath.row + 1
            
            
            vc.ayatCount = 6
            vc.suratNumber = "114"
            vc.startInd = 6341
            vc.endInd = 6347
            
            //            vc.startIndexForEnglish = 6453
            //            vc.endIndexForEnglish = 6460
            //
        }
        
        
        
        
        
        
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        //        self.present(vc, animated: true, completion: nil)
        //                   self.navigationController?.pres(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}


//http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=001&verse=001
//
//
//http://anaxdesigns.website/quran/?get=languages
