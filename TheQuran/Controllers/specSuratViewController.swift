//
//  specSuratViewController.swift
//  TheQuran
//
//  Created by haya on 22/03/2020.
//

import UIKit
import CoreData
import Alamofire
import AVFoundation
import AVKit

import SystemConfiguration

//struct Bookmark: Codable{
//    var Name = String()
//    var Ayat = String()
//}


class specSuratViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource{
    
    @IBOutlet weak var qaraiLbl: UILabel!
    var fromBookmark : Bool = false
    var index = Int()
    var paraNumber = String()
    
    
    var suratCount : Int!
    var ArrSurah = [Int]()
    var bBtn:Bool!
    var current : Int! // This is not optional.
    var total:Int!
    var isArbPlaying:Bool!
    var currEng:Int!
    var siparaArrArabic : [String] = []
    var currVerse:Int!
    var isAlertStatus = true
    var startAyat:Int!
    var siparaArrEng : [String] = []
    var isEng:Bool!
    var SURAH:Int!
    var isMulti:Bool!
    var cArr = [CombineArr]()
    var curr:Int!
    var currentEng:Int!
    var toolBar = UIToolbar()
    var picker  = UIPickerView()
    var qariArrArb : [Any] = []
    var qariArrEng : [Any] = []
    var arbQariName:String!
    var combinedArr : [String] = []
    var selectedInd:Int!
    
    //var isConnected : Bool!
    
    let playImage = UIImage(systemName: "pause.fill")
    let pauseImage = UIImage(systemName: "play.fill")
    var activityView = UIActivityIndicatorView(style: .large)
    var count = 7
    //    var qariArr = ["mishary-rashid-alafas","mishary-rashid-alafas"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
       // check for internet connection
      //  isConnected = Reachability().isConnectedToNetwork()
        
        tblSurat.rowHeight = UITableView.automaticDimension
        tblSurat.estimatedRowHeight = UITableView.automaticDimension
        
        print(ArrSurah)
        //        getQari()
        currVerse = startAyat
        currEng = currentEng
      //  arbQariName = "abdul-rahman-al-sudais"
         arbQariName = "mishary-rashid-alafasy"
        
        
        self.qaraiLbl.text = arbQariName
        // createData()
        getQari()
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)
        //            startAyat = 1
        print(self.current)
        print(self.ayatCount)
        
        
        curr = current
        if  self.fromVC.elementsEqual("Surat"){
            getEnglishUrls(suratNumber: self.suratNumber, ayatCounts: self.ayatCount)
            getUrls(suratNumber: self.suratNumber, ayatCounts: self.ayatCount)
            //            getArabicAllMedia()
            //            getEnglishAllMedia()
            
        }
        else{
            total = ArrSurah.reduce(0, {$0 + $1})
            
            SURAH = 1
            //            getUrl()
            getFullSiparaArb()
            getFullSiparaEng()
            print(surahArrE.count)
            print(surahArrA.count)
            //            getUrlEnglish()
            //            getArabicAllMedia()
            //            getEnglishAllMedia()
        }
        
        //
        
        
        //        getAllMedia()
        
        self.tblSurat.delegate = self
        self.tblSurat.dataSource = self
        surahArrE = []
        surahArrA = []
        setData()
        print(surahArrE.count)
        print(surahArrA.count)
        print(ayatCount)
        print(startInd)
        
        tblSurat.register(UINib(nibName: "cellSurah", bundle: nil), forCellReuseIdentifier: "cellSurah")
        //           let arr = [1,2,3,4,5,6,7,8,9,10]
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if fromBookmark {
                   print(index)
                   
                   let cellIndex = IndexPath(row: index, section: 0)
             
                   //self.tblSurat.scrollToRow(at: cellIndex, at: .bottom, animated: true)
                   self.tblSurat.selectRow(at: cellIndex, animated: true, scrollPosition: .bottom)

               }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // check for internet connection
      //   isConnected = Reachability().isConnectedToNetwork()
       
       
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
//    func getUrl(){
//        var url :String
//        //count =  1
//        urlArray = []
//        print(current)
//        //[7,141]
//        //
//        for i in 0..<ArrSurah.count {
//            print(ArrSurah.count)
//            //0..141+7
//            //        for j in 0..<total{
//            //1..
//
//            print(startAyat)
//            print(ArrSurah[i] + 1)
//            //1..<41
//            //1..<
//            for  k in startAyat..<ArrSurah[i] + 1{
//
//
//                if k < 10 {
//                    if current < 10{
//                        url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=\(arbQariName!)&chapter=00\(current!)&verse=00\(k)"
//                        urlArray.append(url)
//
//                    }
//                    else if current >= 10 && current < 100{
//                        url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=\(arbQariName!)&chapter=0\(current!)&verse=00\(k)"
//                        urlArray.append(url)
//
//                    }
//
//
//                    else if current >= 100{
//                        url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=\(arbQariName!)&chapter=\(current!)&verse=00\(k)"
//                        urlArray.append(url)
//                    }
//
//
//                }
//                else  if k >= 10 && k < 100{
//                    if current < 10{
//                        url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=\(arbQariName!)&chapter=00\(current!)&verse=0\(k)"
//                        urlArray.append(url)
//                    }
//                    else if current >= 10 && current < 100{
//                        url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=\(arbQariName!)&chapter=0\(current!)&verse=0\(k)"
//                        urlArray.append(url)
//                    }
//                    if current >= 100{
//                        url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=\(arbQariName!)&chapter=\(current!)&verse=0\(k)"
//                        urlArray.append(url)
//                    }
//
//
//                }
//
//                if k >= 100 {
//                    if current < 10{
//                        url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=\(arbQariName!)&chapter=00\(current!)&verse=\(k)"
//                        urlArray.append(url)
//                    }
//                    if current >= 10 && current < 100{
//                        url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=\(arbQariName!)&chapter=0\(current!)&verse=\(k)"
//                        urlArray.append(url)
//                    }
//                    if current >= 100{
//                        url = "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=\(arbQariName!)&chapter=\(current!)&verse=\(k)"
//                        urlArray.append(url)
//
//                    }  }
//
//
//            }
//            print(current)
//            current = current + 1
//            startAyat = 1
//
//
//
//
//        }
//
//
//
//
//
//        print(urlArray.count)
//
//
//        print(urlArray)
//
//    }
    
    @IBAction func btnChoose(_ sender: Any) {
        self.urlArray.removeAll()
        self.siparaArrArabic.removeAll()
        
        picker = UIPickerView.init()
        picker.delegate = self
        picker.backgroundColor = UIColor.white
        picker.setValue(UIColor.black, forKey: "textColor")
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(picker)
        
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
        toolBar.barStyle = .default
        toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]
        self.view.addSubview(toolBar)
        
    }
    @objc func onDoneButtonTapped() {
        self.current = curr
        self.currentEng = currEng
        
        self.startAyat = currVerse
        siparaArrArabic.removeAll()
        audioPlayer.pause()
        audioArray.removeAll()
        print(current)
        //        current = current - 1
        //        startAyat = 1
        //        englishAudioArray.removeAll()
        
        print(arbQariName)
        if fromVC.elementsEqual("Surat"){
            getEnglishUrls(suratNumber: self.suratNumber, ayatCounts: self.ayatCount)
                       getUrls(suratNumber: self.suratNumber, ayatCounts: self.ayatCount)
//            getUrls(suratNumber: self.suratNumber, ayatCounts: self.ayatCount
//            getArabicAllMedia()
            //            getEnglishAllMedia()}
            view.endEditing(true)
        }
        else{
            
            
             getFullSiparaArb()
                      getFullSiparaEng()
            
            //                                          getUrlEnglish()
            //            getArabicAllMedia()
            //                                          getEnglishAllMedia()
            view.endEditing(true)
        }
        
        
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
        
        
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return qariArrArb.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let dict = qariArrArb[row] as! NSDictionary
        print(dict)
        let name = dict["reader"] as! String
        print(name)
        
        return name
        
        
        
    }
    func playOneSurat(_ index: Int,lang:String){
        print(index)
        var new : [String] = []
        audioCount = index
        if lang.elementsEqual("Arabic"){
            new = urlArray.sorted()
            print(current)
            print(new)
            
            let Dict = NSURL(string: new[index])
            let playerItem =  AVPlayerItem(url: Dict! as URL)
            print(playerItem)
            audioPlayer = AVPlayer(playerItem: playerItem)
            audioPlayer.volume = 300.0
            
            audioPlayer.play()
            self.playPauseBtn.setImage(playImage, for: .normal)
            print(audioCount)
            
        }
        else if lang.elementsEqual("English") {
            print(englishUrlArray)
            new = englishUrlArray.sorted()
            print(current)
            print(new)
            
            let Dict = NSURL(string: new[index])
            let playerItem =  AVPlayerItem(url: Dict! as URL)
            print(playerItem)
            audioPlayer = AVPlayer(playerItem: playerItem)
            audioPlayer.volume = 300.0
            audioPlayer.play()
            self.playPauseBtn.setImage(playImage, for: .normal)
            
            print(audioCount)
            
            
            
        }
        //        else{
        //
        //            for i in 0..<2{
        //            new = combinedArr
        //                 let item = new[i]
        //
        //            let Dict = NSURL(string: item)
        //                      let playerItem =  AVPlayerItem(url: Dict! as URL)
        //                      print(playerItem)
        //                      audioPlayer = AVPlayer(playerItem: playerItem)
        //                      audioPlayer.volume = 300.0
        //                      audioPlayer.play()
        //                      print(audioCount)
        //            }
        
        
        
        //        }
        
        
        
        
    }
    func playOne(_ index: Int,lang:String){
        
        print(index)
        var new : [String] = []
        print(siparaArrArabic)
        if lang.elementsEqual("Arabic"){
            new = siparaArrArabic.sorted()
            print(current)
            print(new)
            
            let Dict = NSURL(string: new[index])
            let playerItem =  AVPlayerItem(url: Dict! as URL)
            print(playerItem)
            audioPlayer = AVPlayer(playerItem: playerItem)
            audioPlayer.volume = 100.0
            audioPlayer.play()
            self.playPauseBtn.setImage(playImage, for: .normal)
            
            let indexPath = NSIndexPath(item: audioCount, section: 0)
                           
            tblSurat.selectRow(at: indexPath as IndexPath, animated: true, scrollPosition: .bottom)
            print(audioCount)
            
        }
        else if lang.elementsEqual("English") {
            print(englishUrlArray)
            new = siparaArrEng.sorted()
            print(current)
            print(new)
            var Dict:NSURL!
            if index == new.count{
                Dict = NSURL(string: new.last!)
                
            }
            else{
                Dict = NSURL(string: new[index])
                
            }
            let playerItem =  AVPlayerItem(url: Dict! as URL)
            print(playerItem)
            audioPlayer = AVPlayer(playerItem: playerItem)
            audioPlayer.volume = 100.0
            audioPlayer.play()
            self.playPauseBtn.setImage(playImage, for: .normal)
            
            let indexPath = NSIndexPath(item: audioCount, section: 0)
                           
            tblSurat.selectRow(at: indexPath as IndexPath, animated: true, scrollPosition: .bottom)
            print(audioCount)
            
            
            
        }
        //        else{
        //
        //            for i in 0..<2{
        //            new = combinedArr
        //                 let item = new[i]
        //
        //            let Dict = NSURL(string: item)
        //                      let playerItem =  AVPlayerItem(url: Dict! as URL)
        //                      print(playerItem)
        //                      audioPlayer = AVPlayer(playerItem: playerItem)
        //                      audioPlayer.volume = 300.0
        //                      audioPlayer.play()
        //                      print(audioCount)
        //            }
        
        
        
        //        }
        
        
        
        
    }
    func playMultiple(index: Int,strAyat:String){
        //        print(combinedArr)
        //   print(combinedArr[index])
        
        //        let item  = index + 2
        print(cArr)
        print(index)
        
        //        for i in index..<item{
        
      
        let Dict = NSURL(string: strAyat)
        let playerItem =  AVPlayerItem(url: Dict! as URL)
        print(playerItem)
        audioPlayer = AVPlayer(playerItem: playerItem)
        audioPlayer.volume = 100.0
        audioPlayer.play()
        self.playPauseBtn.setImage(playImage, for: .normal)
        
        let indexPath = NSIndexPath(item: selectedInd, section: 0)
                       
        tblSurat.selectRow(at: indexPath as IndexPath, animated: true, scrollPosition: .bottom)
        if isArbPlaying == true{
            selectedInd = index
            
        }
        isMulti = true
        //        }
        
        
        
    }
    func combiningArr(arrArabic:[String] , arrEng : [String]){
        print(arrArabic)
        print(arrEng)
        
        
        //        if self.current! == 1{
        //            self.combinedArr.append("http://anaxdesigns.website/quran/arabic/\(self.arbQariName!)/001/001-001.mp3")
        //            self.combinedArr.append("http://anaxdesigns.website/quran/english/default/001/001-001.mp3")
        //        }
        print(arrArabic.count)
        for item in 0..<arrArabic.count{
            let Arr = CombineArr(strArb: arrArabic[item], strEng: arrEng[item])
            
            //            cArr.append(arrArabic[item])
            cArr.append(Arr)
            
        }
        
        
        
        
        
        print(cArr[0])
        
        
        
        
    }
    /// @brief this method is use to create alert
    func createAlert(title:String,message:String,ind:Int){
        if self.fromVC.elementsEqual("Surat"){
            
            self.combiningArr(arrArabic: self.urlArray.sorted(), arrEng: self.englishUrlArray.sorted())
            
        }
        else{
            self.combiningArr(arrArabic: self.siparaArrArabic.sorted(), arrEng: self.siparaArrEng.sorted())
            
            
        }
        //        playArabicWithTrans(str: cArr[0].strArb)
        print(ind)
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        //        self.present(alert, animated: true, completion: nil)
        alert.addAction(UIAlertAction(title: "Arabic", style: .default, handler: {(action) in
            print(self.urlArray)
            self.isMulti = false
            self.isPlayArabic = true
            self.isPlayEnglish = false
            if self.fromVC.elementsEqual("Surat"){
                //                self.playOneSurat(ind, lang: "Arabic")
                self.playAudioFileSurat(ind)
            }
            else{
                self.playOne(ind, lang: "Arabic")
                
            }
            return
            //            }
            
            
        }))
        alert.addAction(UIAlertAction(title: "Translation", style: .default, handler: {(action) in
            
            //            if ind == 0 {
            
            //            }
            self.isMulti = false
            self.isPlayArabic = false
            self.isPlayEnglish = true
            if self.fromVC.elementsEqual("Surat"){
                self.playOneSurat(ind, lang: "English")
                
            }
            else{
                self.playSiparaEng(index: ind)
                //                self.playOne(ind, lang: "English")
            }
            return     }))
        alert.addAction(UIAlertAction(title: "Arabic with translation", style: .default, handler: {(action) in
            print(self.cArr)
            self.isMulti = true
            DispatchQueue.main.asyncAfter(deadline: (.now() + 0.6)) {
                print(ind)
                print(self.cArr[ind])
                self.isArbPlaying = true
                self.playMultiple(index: ind, strAyat: self.cArr[ind].strArb)
                
            }
            
            //            self.playOne(ind, lang: "translation")
            //                    self.playOne(ind, lang: "English")
            return     }))
        self.present(alert, animated: true, completion:{
            alert.view.superview?.isUserInteractionEnabled = true
            alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutside)))
        })
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(self.current)
        print(siparaArrArabic)
        print(curr)
        self.current = curr
        self.currentEng = currEng
        selectedInd = indexPath.row
        isArbPlaying = true
        
        //        if indexPath.row == 0{
        
        createAlert(title: "", message: "",ind:selectedInd)
        //        }else{
        //            createAlert(title: "", message: "", ind:selectedInd)
        
        //        }
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        print(row)
        let dict = qariArrArb[row] as! NSDictionary
        print(dict)
        let name = dict["reader"] as! String
        self.arbQariName = name
        self.qaraiLbl.text = name
    }
    func getUrlEnglish(){
        var url :String
        //count =  1
        
        
        //[7,141]
        //
        for i in 0..<ArrSurah.count {
            //0..141+7
            //        for j in 0..<total{
            //1..
            
            
            
            print(ArrSurah[i])
            
            for  k in startAyat..<ArrSurah[i] + 1{
                
                if k < 10 {
                    if currentEng < 10{
                        
                        url = "http://anaxdesigns.website/quran/?get=verse&lang=english&reader=default&chapter=00\(currentEng!)&verse=00\(k)"
                        englishUrlArray.append(url)
                    }
                    if currentEng > 10 && currentEng < 100{
                        url = "http://anaxdesigns.website/quran/?get=verse&lang=english&reader=default&chapter=0\(currentEng!)&verse=00\(k)"
                        englishUrlArray.append(url)
                        
                    }
                    if currentEng >= 100{
                        url = "http://anaxdesigns.website/quran/?get=verse&lang=english&reader=default&chapter=\(currentEng!)&verse=00\(k)"
                        englishUrlArray.append(url)
                    }
                    
                    
                }
                if k > 10 && k < 100{
                    if currentEng < 10{
                        url = "http://anaxdesigns.website/quran/?get=verse&lang=english&reader=default&chapter=00\(currentEng!)&verse=0\(k)"
                        englishUrlArray.append(url)
                    }
                    if currentEng > 10{
                        url = "http://anaxdesigns.website/quran/?get=verse&lang=english&reader=default&chapter=0\(currentEng!)&verse=0\(k)"
                        englishUrlArray.append(url)
                    }
                    if currentEng >= 100{
                        url = "http://anaxdesigns.website/quran/?get=verse&lang=english&reader=default&chapter=\(currentEng!)&verse=00\(k)"
                        englishUrlArray.append(url)
                    }
                    
                    
                }
                
                if k >= 100 {
                    if currentEng < 10{
                        url = "http://anaxdesigns.website/quran/?get=verse&lang=english&reader=default&chapter=00\(currentEng!)&verse=\(k)"
                        englishUrlArray.append(url)
                    }
                    if currentEng > 10{
                        url = "http://anaxdesigns.website/quran/?get=verse&lang=english&reader=default&chapter=0\(currentEng!)&verse=\(k)"
                        englishUrlArray.append(url)
                    }
                    if currentEng >= 100{
                        url = "http://anaxdesigns.website/quran/?get=verse&lang=english&reader=default&chapter=\(currentEng!)&verse=00\(k)"
                        englishUrlArray.append(url)
                        
                    }  }
                
                
            }
            
            
            currentEng = currentEng + 1
            startAyat = 1
            
            
            
            
        }
        
        
        
        
        
        print(englishUrlArray)
        
        
        
        
    }
    func getUrls(suratNumber: String, ayatCounts: Int) {
        print(startAyat)
        var t:Int
        if self.suratNumber == "001"{
            t = 1
        }
        else{
            t = 0
        }
        for i in t..<ayatCounts + 1 {
            
            var url :String
            if i < 10{
                url =  "http://anaxdesigns.website//quran//arabic//\(arbQariName!)//\(suratNumber)//\(suratNumber)-00\(i).mp3"
                
                
                urlArray.append(url)
                print(urlArray)
            }
            if i>=10 && i<100{
                url =  "http://anaxdesigns.website//quran//arabic//\(arbQariName!)//\(suratNumber)//\(suratNumber)-0\(i).mp3"
                
                
                
                urlArray.append(url)
                print(urlArray)
            }
            
            if i >= 100{
                
                url =  "http://anaxdesigns.website//quran//arabic//\(arbQariName!)//\(suratNumber)//\(suratNumber)-\(i).mp3"
                
                
                
                urlArray.append(url)
                print(urlArray)
            }
            
            
            //              urlArray.append(url)
            //              print(urlArray)
        }
        print(urlArray)
    }
    
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        englishAudioArray.removeAll()
        audioArray.removeAll()
        
        urlArray.removeAll()
        englishUrlArray.removeAll()
        
        isAlertStatus = false
    }
    
    
    // MARK:- Bookmark Create Data Function
    func createData(name: String, ayat: String,title:String, cellTag : Int, from : String , surahNumber: String) {
        
        //create data
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let userEntity = NSEntityDescription.entity(forEntityName: "Bookmarks",
                                                    in: managedContext)!
        
        let bookmark = NSManagedObject(entity: userEntity,
                                       insertInto: managedContext)
        
        bookmark.setValue(title, forKey: "title")
        bookmark.setValue(name, forKeyPath: "name")
        bookmark.setValue(ayat, forKeyPath: "ayat")
        bookmark.setValue(cellTag, forKey: "tag")
        bookmark.setValue(from, forKey: "fromVC")
        bookmark.setValue(surahNumber, forKey: "surahOrSiparahNumber")
        
//        bookmark.setValue(surah, forKeyPath: "surah")
        
        
        
        
        
        
        do {
            try managedContext.save()
            
            let vc = BookmarkViewController()
            vc.bookmarkArray.append(bookmark)
            showToast(message: "BookMarked", viewController: self)
            //  bookmark.append(person)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
    }
    
    var isPlayOn = false
    var isPlayArabic = false
    var isPlayEnglish = false
    var audioCount = 0
    var fromVC:String!
    
    
    var audioArray = [String]()
    var englishAudioArray = [String]()
    
    var urlArray = [String]()
    var englishUrlArray = [String]()
    
    var audioPlayer = AVPlayer()
    var audioFile:String!
    
    @IBOutlet weak var playPauseBtn: UIButton!
    
    @IBOutlet weak var forwardBtn: UIButton!
    
    
    
    @IBOutlet weak var backwardBtn: UIButton!
    
    
    @IBAction func playBtn(_ sender: Any) {
        if  isAlertStatus {
            if self.fromVC.elementsEqual("Surat"){
                
                self.combiningArr(arrArabic: self.urlArray.sorted(), arrEng: self.englishUrlArray.sorted())
                
            }
            else{
                self.combiningArr(arrArabic: self.siparaArrArabic.sorted(), arrEng: self.siparaArrEng.sorted())
                
                
            }
            showAlert()
            //
            isAlertStatus = false
        }
        else{
            
            
//            let image = UIImage(named: "iconPause")
            
            if let myButtonImage = playPauseBtn.image(for: .normal),
                let buttonAppuyerImage = pauseImage,
                myButtonImage.pngData() == buttonAppuyerImage.pngData()
            {
                
                audioPlayer.pause()
                playPauseBtn.setImage(playImage, for: .normal)
                isPlayOn = false
                print("YES")
            } else {
                
                playPauseBtn.setImage(pauseImage, for: .normal)
                isPlayOn = true
                if self.fromVC.elementsEqual("Surat"){
                    
                    playAudioFileSurat(audioCount)
                    
                }
                else{
                    playSipara(index: audioCount)
                }
                print("NO")
            }
            
        }
        
        
        
        
        
        
        // let image = UIImage(systemName: <#T##String#>)
        
        
        //
        //        if playPauseBtn.currentImage == pauseImage{
        //            audioPlayer.pause()
        //            playPauseBtn.setImage(playImage, for: .normal)
        //
        //        }
        //        else if playPauseBtn.currentImage == playImage{
        //            playPauseBtn.setImage(pauseImage, for: .normal)
        //         isPlayOn = true
        //            playAudioFile(audioCount)
        //        }
        //playPauseBtn.setImage(UIImage(named: "pause.fill"), for: .normal)
        
        
        
    }
    
    
    @IBAction func forwardBtn(_ sender: Any) {
            audioPlayer.pause()
        if isPlayArabic {
            
                audioCount += 1
            
                if (audioCount < audioArray.count && audioCount >= 0){
                    // audioCount += 1
                    playAudioFileSurat(audioCount)
                
                
            }
        }
        if isPlayEnglish{
            if isPlayOn {
                audioCount += 1
                if (audioCount < englishAudioArray.count && audioCount >= 0){
                    // audioCount += 1
                    playSiparaEng(index: audioCount)
                }
                
            }
        }
        
        
    }
    
    
    @IBAction func backwardBtn(_ sender: Any) {
        
        if isPlayArabic {
            if isPlayOn{
                
                if audioCount > 0 {
                    
                    audioCount -= 1
                    
                    if (audioCount < audioArray.count && audioCount >= 0){
                        // audioCount += 1
                        playAudioFileSurat(audioCount)
                    }
                }
                
                
            }
        }
        
        if isPlayEnglish {
            if isPlayOn{
                
                if audioCount > 0 {
                    
                    audioCount -= 1
                    
                    if (audioCount < englishAudioArray.count && audioCount >= 0){
                        // audioCount += 1
                        playSiparaEng(index: audioCount)
                    }
                }
                
                
            }
        }
        
        
        
        
        
        
    }
    func createData() {
        
        
        //create data
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let userEntity = NSEntityDescription.entity(forEntityName: "Bookmarks",
                                                    in: managedContext)!
        
        let bookmark = NSManagedObject(entity: userEntity,
                                       insertInto: managedContext)
        
        for i in 1...3 {
            
            bookmark.setValue("\(i) bookmark", forKeyPath: "name")
            //  bookmark.setValue("second bookmark", forKeyPath: "name")
        }
        
        
        
        do {
            try managedContext.save()
            
            let abc = BookmarkViewController()
            abc.bookmarkArray.append(bookmark)
            //  bookmark.append(person)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
    }
    
    
    
    
    
    func getQari(){
        let url = "http://anaxdesigns.website/quran/?get=languages"
        let request = AF.request(url)
        // 2
        
        if Reachability().isInternetAvailable() {
            request.responseJSON { (data) in
                print(data)
                
                if data == nil{
                    
                }
                else{
                    let responseData = data.value as! NSDictionary
                    print(responseData)
                    let data = responseData["data"] as! NSDictionary
                    self.qariArrArb = data["arabic"] as! [Any]
                }
                
                
            }
        }
        
        else{
            showToast(message: "No Internet!", viewController: self)
        }
        
        
        
        
        
        
        
        
    }
    struct surahArabic {
        var ayat:String!
        var surahNum:Int!
        var ayatNum:Int!
    }
    
    var bookMarkName = String()
    // var bookMarkArray : [Bookmark] = []
    
    
    var surahArrA : [String] = []
    var surahArrE : [String] = []
    var startIndexForEnglish = 0
    var endIndexForEnglish = 7
    var startInd = 0
    var endInd = 12
    @IBOutlet weak var tblSurat: UITableView!
    var ayatCount:Int = 0
    var suratNumber = String()
    
    var siparaNumber = [String]()
    var siparaAyatCount = Int()
    
    
    
    
    public func shareLinkToAllApps(eng:String ,link : String, msg : String, reference : String , vc: UIViewController)  {
        if let myWebsite = NSURL(string: link) {
            let objectsToShare = [eng,msg,reference, myWebsite] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            vc.present(activityVC, animated: true, completion: nil)
        }
    }
    func setData(){
        
        if let path = Bundle.main.path(forResource: "quran-simple-enhanced ", ofType: "txt") {
            do {
                let data = try String(contentsOfFile: path, encoding: .utf8)
                
                let myStringsA = data.components(separatedBy: "\r\n")
                //  print(myStrings[startInd ])
                //  print(myStrings[endInd])
                
                for  i in startInd..<endInd + 1{
                    
                    if myStringsA[i].isEmpty{
                        print("gap")
                        
                        
                    }
                    else{
                     let a =  myStringsA[i].split(separator: "|")
                        print(a[2])
                     
//                        let newObj = myStringsA[i].split(separator: "\"")
                        surahArrA.append(String(a[2]))
//                        print(newObj)
//
                          print(surahArrA)
                        //
                        
                        
                    }}
                
            } catch {
                print(error)
            }
            print(surahArrA.count)
            
        }
        
        if let path = Bundle.main.path(forResource: "en.wahiduddin", ofType: "txt") {
            do {
                let data = try String(contentsOfFile: path, encoding: .utf8)
                
                let myStrings = data.components(separatedBy: "\n")
                
                //  print(myStrings)
                //                endInd += 1
                
                
                for  i in startInd..<endInd + 1{
                    print(i)
                    // print(myStrings[startInd])
                    if myStrings[i].isEmpty{
                        print("gap")
                    }
                    else{
//
//                        let newObj = myStrings[i].split(separator: "\"")
//                        surahArrE.append(newObj)
//                        print(newObj)
                        let a =  myStrings[i].split(separator: ":")
                        print(a[1])
                     
//                        let newObj = myStringsA[i].split(separator: "\"")
                        surahArrE.append(String(a[1]))
//                        print(newObj)
//
                          print(surahArrA)
                        
                        
                    }}
                print(surahArrE)
                
            } catch {
                print(error)
            }
            print(surahArrE.count)
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return surahArrE.count
    }
    
    
    func yourFunctionName(finished: () -> Void) {
        
        //   print("Doing something!")
        
        finished()
        
    }
    
    func showBookMarkAlert() -> String {
        let alertController = UIAlertController(title: "Add new bookmark", message: nil, preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Add", style: .default) { (_) in
            if let txtField = alertController.textFields?.first, let text = txtField.text {
                // operations
                print("Text==>" + text)
                self.bookMarkName = text
                
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        alertController.addTextField { (textField) in
            textField.placeholder = "Start typing"
        }
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
        return self.bookMarkName
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //        if indexPath.row%2 == 0{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSurah", for: indexPath) as! cellSurah
        print(surahArrA[0])
        print(surahArrE[0])
        
//        if let cell = tableView.cellForRowAtIndexPath(someIndexPath) {
//          if cell.selected {
//            print("This cell is selected")
//          }
//        }
        
        
        
        
        var a = surahArrE[indexPath.row]
//        print(String(describing: a))
//
//        print(a)
//        print(a.description)
        cell.lblEng.text = surahArrE[indexPath.row]
//        cell.lblEng.text  = String(describing: a).replacingOccurrences(of: "[", with: "").replacingOccurrences(of: "]", with: "")
      var b = surahArrA[indexPath.row]
        print(surahArrA)
//        if indexPath.row == surahArrA.count{
//            b = surahArrA[indexPath.row - 1] as! [Any]
//
//        }
//        else{
//            b = surahArrA[indexPath.row] as! [Any]
//
//        }
        cell.lblArabic.text = surahArrA[indexPath.row]
//        cell.lblArabic.text  = String(describing: b).replacingOccurrences(of: "[", with: "").replacingOccurrences(of: "]", with: "")
        cell.btnShare.tag = indexPath.row
        print(b)
        
       
        print(b.description)
        cell.btnShare.accessibilityValue = a.description
        cell.btnShare.accessibilityLabel = b.description
        cell.btnShare.tag = indexPath.row
        cell.btnShare.addTarget(self, action:#selector(specSuratViewController.ShareClick(sender:)), for: .touchUpInside)
        
        cell.btnBookMark.accessibilityValue = a.description
        cell.btnBookMark.accessibilityLabel = b.description
        
        cell.btnBookMark.tag = indexPath.row
        cell.btnBookMark.addTarget(self, action: #selector(self.BookMarkClick(sender:)), for: .touchUpInside)
        return cell
        //        }
        
        
        
        
    }
    
  
    
}

extension specSuratViewController{
    
    @objc func ShareClick(sender : UIButton){
        let link = "www.quranaapp.com"
        let des = sender.accessibilityLabel!
        let engText = sender.accessibilityValue
        let surah_SiparaName = self.navigationItem.title
        let referenceOfSurahORSiparah = "\(surah_SiparaName!) verse \(sender.tag)"
        
        print("\(surah_SiparaName!) verse \(sender.tag)")
        
        print(sender)
        
        print(des)
        print(engText)
        shareLinkToAllApps(eng: engText!, link: link, msg: des + engText!, reference: referenceOfSurahORSiparah, vc: self)
        //           SharedHelper().shareLinkToAllApps(link : link, msg : des, vc: self)
        
    }
    
    
    
    @objc func BookMarkClick(sender : UIButton){
        //        createData()
        //      //  var bookmark = Bookmark()
        //        print(bookMarkName)
        //        print(sender.accessibilityLabel)
        //        print(sender.accessibilityValue)
        //   bookmark.Name = "second BookMark"
        // bookmark.Ayat = "ayat"
        // bookMarkArray.append(bookmark)
        
        
        var bookMarkName = String()
        //   print(bookMarkName)
        var des = sender.accessibilityLabel!
        
        createAlert(sender:sender)
        //        createData(name: sender.accessibilityValue!, ayat: des)
        
    }
    @objc func dismissOnTapOutside(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func createAlert(sender:UIButton){
        let alert = UIAlertController(title: "Bookmark Name", message: "", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.placeholder = "Type text here....."
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            print("Text field: \(textField!.text)")
            
            if(!(textField?.text?.isEmpty)!)
            {
                var bookMarkName = String()
                var des = sender.accessibilityLabel!
                
                //   print(bookMarkName)
                print(sender.tag)
                print(self.fromVC)
                print(self.suratNumber)
                print(self.navigationItem.title!)
                
                if self.fromVC == "Sipary" {
                    self.suratNumber = self.paraNumber
                }
                
                print(self.suratNumber)
                
                
                self.createData(name: sender.accessibilityValue!, ayat: des,title:textField!.text!, cellTag: sender.tag, from: self.fromVC!, surahNumber: self.suratNumber)
               
                
            }
        }))
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion:{
            alert.view.superview?.isUserInteractionEnabled = true
            alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutside)))
        })
    }
}




//UserDefaults.standard.set(try? PropertyListEncoder().encode(bookMarkArray), forKey:"bookmark")
//       UserDefaults.standard.synchronize()
extension specSuratViewController{
    
    func getEnglishUrls(suratNumber: String, ayatCounts: Int) {
        var t :Int!
        if self.suratNumber == "001"{
            t = 1
        }
        else{
            t = 0
        }
        for i in t..<ayatCounts + 1 {
            
            var url :String
            if i < 10{
                url = "http://anaxdesigns.website/quran/english/default/\(self.suratNumber)/\(self.suratNumber)-00\(i).mp3"
                
                englishUrlArray.append(url)
                print(englishUrlArray)
            }
            if i>=10 && i<100{
                url = "http://anaxdesigns.website/quran/english/default/\(self.suratNumber)/\(self.suratNumber)-0\(i).mp3"
                englishUrlArray.append(url)
            }
            
            if i >= 100{
                url = "http://anaxdesigns.website/quran/english/default/\(self.suratNumber)/\(self.suratNumber)-\(i).mp3"
                englishUrlArray.append(url)
            }
            
        }
        print(englishUrlArray)
    }
    
    
    
    
    func showAlert() {
        
        let alert = UIAlertController(title: "Choose one of them to listen", message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Arabic", style: .default, handler: { (_) in
            print("You've pressed default")
            
            self.isPlayEnglish = false
            self.isPlayArabic = true
//
//            let pauseImage = UIImage(systemName: "pause.fill")
//            let playImage = UIImage(systemName: "play.fill")
//            let image = UIImage(named: "iconPause")
            
            if let myButtonImage = self.playPauseBtn.image(for: .normal),
                let buttonAppuyerImage = self.pauseImage,
                myButtonImage.pngData() == buttonAppuyerImage.pngData()
            {
                
                self.audioPlayer.pause()
                self.playPauseBtn.setImage(self.pauseImage, for: .normal)
                self.isPlayOn = false
                print("YES")
            } else {
                
                self.playPauseBtn.setImage(self.pauseImage, for: .normal)
                self.isPlayOn = true
                self.playAudioFileSurat(self.audioCount)
                print("NO")
            }
            
            
            
            
            
        }))
        
        //           alert.addAction(UIAlertAction(title: "Arabic with translation", style: .default, handler: { (_) in
        //               print("You've pressed cancel")
        //
        //           }))
        
        
        alert.addAction(UIAlertAction(title: "English", style: .default, handler: { (_) in
            print("You've pressed cancel")
            
            self.isPlayEnglish = true
            self.isPlayArabic = false
            
           
//            let image = UIImage(named: "iconPause")
            
            if let myButtonImage = self.playPauseBtn.image(for: .normal),
                let buttonAppuyerImage = self.pauseImage,
                myButtonImage.pngData() == buttonAppuyerImage.pngData()
            {
                
                self.audioPlayer.pause()
                self.playPauseBtn.setImage(self.pauseImage, for: .normal)
                self.isPlayOn = false
                print("YES")
            } else {
                
                self.playPauseBtn.setImage(self.pauseImage, for: .normal)
                self.isPlayOn = true
                self.playAudioFileEng(self.audioCount)
                //                self.playSiparaEng(index: self.audioCount)
                print("NO")
            }
            
            
        }))
        
        
        alert.addAction(UIAlertAction(title: "Arabic with translation", style: .default, handler: {(action) in
            print(self.cArr)
            self.isMulti = true
            self.bBtn = true
            DispatchQueue.main.asyncAfter(deadline: (.now() + 0.6)) {
                
                
                self.isArbPlaying = true
                self.playArabicWithTrans(str: self.cArr[0].strArb)
                //               self.playMultiple(index: ind, strAyat: self.cArr[ind].strArb)
                //
            }
            
            //            self.playOne(ind, lang: "translation")
            //                    self.playOne(ind, lang: "English")
            return     }))
        self.present(alert, animated: true, completion:{
            alert.view.superview?.isUserInteractionEnabled = true
            alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissOnTapOutside)))
        }
        ) }
    
    
    func getFullSiparaArb(){
        var newCurrentAyat = self.currVerse
        print(current)
        print(currVerse)
        //        http://anaxdesigns.website//quran//arabic//abdul-rahman-al-sudais//001//001-001.mp3
        //            if self.current! == 1{
        //            self.siparaArrArabic.append("http://anaxdesigns.website//quran//arabic//\(self.arbQariName!)//001//001-001.mp3")
        //        }
        //        else{
        //            if self.current! < 10{
        //                self.siparaArrArabic.append("http://anaxdesigns.website//quran//arabic//\(self.arbQariName!)//001//001-001.mp3")
        //            }
        //            else if self.current >= 10 {
        //                self.siparaArrArabic.append("http://anaxdesigns.website//quran//arabic//\(self.arbQariName!)//001//001-001.mp3")
        //            }
        //
        //        }
        //
        var url:String!
        // 2
        
        
        
        //                        self.audioArray.append(self.audioFile)
        
        
        for i in 0..<self.ArrSurah.count {
            print(self.ArrSurah.count)
            //0..141+7
            //        for j in 0..<total{
            //1..
            if self.current! == 1{
                self.startAyat = 1
            }
            else{
                self.startAyat = newCurrentAyat
                
            }
            
            print(self.startAyat)
            print(self.ArrSurah[i] + 1)
            //1..<41
            //1..<
            for  k in self.startAyat..<self.ArrSurah[i] + 1{
                
                
                if k < 10 {
                    if self.current < 10{
                        url = "http://anaxdesigns.website//quran//arabic//\(self.arbQariName!)//00\(self.current!)//00\(self.current!)-00\(k).mp3"
                        
                        
                        
                        self.siparaArrArabic.append(url)
                        
                    }
                    else if self.current >= 10 && self.current < 100{
                        //                                        http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=\(self.arbQariName!)&chapter=0\(self.current!)&verse=00\(k)
                        
                        url = "http://anaxdesigns.website//quran//arabic//\(self.arbQariName!)//0\(self.current!)//0\(self.current!)-00\(k).mp3"
                        self.siparaArrArabic.append(url)
                        
                    }
                        
                        
                    else if self.current >= 100{
                        
                        //http://anaxdesigns.website//quran//arabic//\(self.arbQariName!)//00\(self.current!)//00\(self.current!)-00\(k).mp3
                        url = "http://anaxdesigns.website//quran//arabic//\(self.arbQariName!)//\(self.current!)//\(self.current!)-00\(k).mp3"
                        self.siparaArrArabic.append(url)
                    }
                    
                    
                }
                else  if k >= 10 && k < 100{
                    if self.current < 10{
                        url = "http://anaxdesigns.website//quran//arabic//\(self.arbQariName!)//00\(self.current!)//00\(self.current!)-0\(k).mp3"
                        self.siparaArrArabic.append(url)
                    }
                    else if self.current >= 10 && self.current < 100{
                        url = "http://anaxdesigns.website//quran//arabic//\(self.arbQariName!)//0\(self.current!)//0\(self.current!)-0\(k).mp3"
                        self.siparaArrArabic.append(url)
                    }
                    if self.current >= 100{
                        url = "http://anaxdesigns.website//quran//arabic//\(self.arbQariName!)//\(self.current!)//\(self.current!)-0\(k).mp3"
                        self.siparaArrArabic.append(url)
                    }
                    
                    
                }
                
                if k >= 100 {
                    if self.current < 10{
                        url = "http://anaxdesigns.website//quran//arabic//\(self.arbQariName!)//00\(self.current!)//00\(self.current!)-\(k).mp3"
                        self.siparaArrArabic.append(url)
                    }
                    if self.current >= 10 && self.current < 100{
                        url = "http://anaxdesigns.website//quran//arabic//\(self.arbQariName!)//0\(self.current!)//0\(self.current!)-\(k).mp3"
                        self.siparaArrArabic.append(url)
                    }
                    if self.current >= 100{
                        url = "http://anaxdesigns.website//quran//arabic//\(self.arbQariName!)//\(self.current!)//\(self.current!)-\(k).mp3"
                        self.siparaArrArabic.append(url)
                        
                    }  }
                
                
            }
            
            self.current = self.current + 1
            self.startAyat = 0
            newCurrentAyat = 0
            
            
            
        }
        
        
        
        print(self.siparaArrArabic)
        
        
        
        
        
    }
    
    
    
    
    
    func getFullSiparaEng(){
        
        print(current)
        print(currentEng)
        
        //        http://anaxdesigns.website//quran//arabic//abdul-rahman-al-sudais//001//001-001.mp3
        //            if self.currentEng! == 1 || self.currentEng! == 9{
        //            self.siparaArrEng.append("http://anaxdesigns.website//quran//english//default//001//001-001.mp3")
        //        }
        //        else{
        //            if self.currentEng! < 10{
        //                self.siparaArrEng.append("http://anaxdesigns.website/quran/?get=verse&lang=english&reader=default&chapter=00\(self.currentEng!)&verse=000")
        
        //            }
        //            else if self.currentEng >= 10 {
        //                self.siparaArrEng.append("http://anaxdesigns.website/quran/?get=verse&lang=english&reader=default&chapter=0\(self.currentEng!)&verse=000")
        //            }
        
        //        }
        //        print(siparaArrEng[0])
        var url:String!
        // 2
        //        request.responseJSON { (data) in
        //            print(data)
        //            let responseData = data.value as! NSDictionary
        //            print(responseData)
        //
        //            let audioDict = responseData["data"] as! NSDictionary
        //            let audio = audioDict["audio"] as! String
        //
        //            self.siparaArrEng.remove(at: 0)
        //            self.audioFile = audio
        //                        self.audioArray.append(self.audioFile)
        if self.currentEng! == 1{
            self.startAyat = 1
        }
        else{
            self.startAyat = self.currVerse
            
        }
        print(self.startAyat)
        for i in 0..<self.ArrSurah.count {
            print(self.ArrSurah.count)
            //0..141+7
            //        for j in 0..<total{
            //1..
            
            print(self.startAyat)
            print(self.ArrSurah[i] + 1)
            //1..<41
            //1..<
            for  k in self.startAyat..<self.ArrSurah[i] + 1{
                
                
                if k < 10 {
                    if self.currentEng < 10{
                        url = "http://anaxdesigns.website/quran/english/default/00\(self.currentEng!)/00\(self.currentEng!)-00\(k).mp3"
                        
                        
                        
                        self.siparaArrEng.append(url)
                        
                    }
                    else if self.currentEng >= 10 && self.currentEng < 100{
                        //                                        http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=\(self.arbQariName!)&chapter=0\(self.current!)&verse=00\(k)
                        
                        url = "http://anaxdesigns.website/quran/english/default/0\(self.currentEng!)/0\(self.currentEng!)-00\(k).mp3"
                        self.siparaArrEng.append(url)
                        
                    }
                        
                        
                    else if self.currentEng >= 100{
                        
                        //http://anaxdesigns.website//quran//arabic//\(self.arbQariName!)//00\(self.current!)//00\(self.current!)-00\(k).mp3
                        url = "http://anaxdesigns.website/quran/english/default/\(self.currentEng!)/\(self.currentEng!)-00\(k).mp3"
                        self.siparaArrEng.append(url)
                    }
                    
                    
                }
                else  if k >= 10 && k < 100{
                    if self.currentEng < 10{
                        url = "http://anaxdesigns.website/quran/english/default/00\(self.currentEng!)/00\(self.currentEng!)-0\(k).mp3"
                        self.siparaArrEng.append(url)
                    }
                    else if self.currentEng >= 10 && self.currentEng < 100{
                        url = "http://anaxdesigns.website/quran/english/default/0\(self.currentEng!)/0\(self.currentEng!)-0\(k).mp3"
                        self.siparaArrEng.append(url)
                    }
                    if self.currentEng >= 100{
                        url = "http://anaxdesigns.website/quran/english/default/\(self.currentEng!)/\(self.currentEng!)-0\(k).mp3"
                        self.siparaArrEng.append(url)
                    }
                    
                    
                }
                
                if k >= 100 {
                    if self.currentEng < 10{
                        url = "http://anaxdesigns.website/quran/english/default/00\(self.currentEng!)/00\(self.currentEng!)-\(k).mp3"
                        self.siparaArrEng.append(url)
                    }
                    if self.currentEng >= 10 && self.currentEng < 100{
                        url = "http://anaxdesigns.website/quran/english/default/0\(self.currentEng!)/0\(self.currentEng!)-\(k).mp3"
                        self.siparaArrEng.append(url)
                    }
                    if self.currentEng >= 100{
                        url = "http://anaxdesigns.website/quran/english/default/\(self.currentEng!)/\(self.currentEng!)-\(k).mp3"
                        self.siparaArrEng.append(url)
                        
                    }  }
                
                
            }
            
            self.currentEng = self.currentEng + 1
            self.startAyat = 0
            
            
            
            
        }
        
        //                        self.siparaArrArabic = self.urlArray
        //surat number 0..<1
        //                        for i in 0..<self.ArrSurah.count {
        //                            print(i)
        //
        //                            print(self.currVerse)
        //
        //                            for  k in self.currVerse..<self.ArrSurah[i] {
        //                                self.siparaArrArabic.append("http://anaxdesigns.website/quran/arabic/abdul-rahman-al-sudais/00\(self.current!)/00\(self.current!)-\(self.currVerse + 1).mp3")
        //
        //                                 print(self.siparaArrArabic)
        //
        //
        //
        //
        //
        //                            }
        //
        //
        //                            self.currVerse = 1
        //
        //                        }
        
        
        print(self.siparaArrEng)
        
        //
        
        
        
        
        
    }
    
    
    
    
    func getArabicAllMedia(){
        print(urlArray.count)
        let urlCount = urlArray.count
        
        
        // let serialQueue = DispatchQueue(label: "serialQueue")
        
        for i in 0..<urlCount{
            //            print(urlArray[i])
            
            self.activityView.color = .black
            self.activityView.center = self.view.center
            self.view.addSubview(self.activityView)
            self.activityView.startAnimating()
            self.playPauseBtn.isEnabled = false
            print(urlArray[0])
            let request = AF.request(urlArray[i])
            // 2
            
            if Reachability().isInternetAvailable() {
                request.responseJSON { (data) in
                    print(data)
                    let responseData = data.value as! NSDictionary
                    print(responseData)
                    
                    let audioDict = responseData["data"] as! NSDictionary
                    let audio = audioDict["audio"] as! String
                    //                let str = URL(string: audio)
                    //                self.fetchImage(image_url: str!.absoluteString) { (data) in
                    
                    //
                    //                      DispatchQueue.main.async {
                    //                        let str = String(decoding: data!, as: UTF8.self)
                    //
                    //
                    //                          self.audioFile = str
                    //                        self.audioArray.append(self.audioFile)
                    //
                    //                    }}
                    
                    self.audioFile = audio
                    self.audioArray.append(self.audioFile)
                    
                }
            }
            
            else{
                showToast(message: "No Internet!", viewController: self)
            }
            
            print(self.audioArray)
            
            
            
            
            
            //
            if self.audioArray.count == urlCount{
                self.playPauseBtn.isEnabled = true
                
                self.activityView.stopAnimating()
                //                }
                
                
                
                
            }
            
            print(self.audioArray.count)
            
        }
        if self.current! == 9{
            
        }
        else   if self.current! == 1 {
            
            //                       self.audioArray.append("http://anaxdesigns.website/quran/arabic/\(self.arbQariName!)/00\(self.current!)/00\(self.current!)-001.mp3")
            //
        }
        else {
            if self.current < 10{
                self.audioArray.append("http://anaxdesigns.website/quran/arabic/\(self.arbQariName!)/00\(self.current!)/00\(self.current!)-000.mp3")
            }
            else if self.current < 100{
                self.audioArray.append("http://anaxdesigns.website/quran/arabic/\(self.arbQariName!)/0\(self.current!)/0\(self.current!)-000.mp3")
            }
            else  {
                self.audioArray.append("http://anaxdesigns.website/quran/arabic/\(self.arbQariName!)/\(self.current!)/\(self.current!)-000.mp3")
            }}
        
        
        print("Audio Files \(self.audioArray)")
        
        //completion()
        
        
        
        //        let url = "http://anaxdesigns.website/quran/?get=verse&lang=english&reader=default&chapter=001&verse=001"
        
    }
    func fetchImage(image_url: String, completionHandler: @escaping (_ data: Data?) -> ()) {
        let session = URLSession.shared
        let url = URL(string: image_url)
        
        let dataTask = session.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                print("Error fetching the image! 😢")
                completionHandler(nil)
            } else {
                completionHandler(data)
            }
        }
        
        dataTask.resume()
    }
    
    func getEnglishAllMedia(){
        
        let urlCount = englishUrlArray.count
        // let serialQueue = DispatchQueue(label: "serialQueue")
        
        for i in 0..<urlCount{
            //             let url =  "http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=001&verse=00\(i)"
            //
            print(englishUrlArray[i])
            
            
            //                    self.activityView.center = self.view.center
            //                    self.view.addSubview(self.activityView)
            //                    self.activityView.startAnimating()
            
            let request = AF.request(englishUrlArray[i])
            // 2
            
            if Reachability().isInternetAvailable() {
                request.responseJSON { (data) in
                    print(data)
                    let responseData = data.value as! NSDictionary
                    print(responseData)
                    
                    //                        else{
                    let audioDict = responseData["data"] as! NSDictionary
                    let audio = audioDict["audio"] as! String
                    self.audioFile = audio
                    print(self.audioFile)
                    self.englishAudioArray.append(self.audioFile)
                    print("Audio Files \(self.englishAudioArray)")
                    
                    
                    if self.englishAudioArray.count == urlCount {
                        self.activityView.stopAnimating()
                        self.playPauseBtn.isEnabled = true
                    }
                    
                }
            }
            else{
                showToast(message: "No Internet!", viewController: self)
            }
            
            
            
            
        }
        if self.current! == 9{
            
        }
        else   if self.current! == 1 {
            
            //                       self.audioArray.append("http://anaxdesigns.website/quran/arabic/\(self.arbQariName!)/00\(self.current!)/00\(self.current!)-001.mp3")
            //
        }
        else {
            if self.current < 10{
                self.englishAudioArray.append("http://anaxdesigns.website/quran/english/default/00\(self.current!)/00\(self.current!)-000.mp3")}
            if self.current < 100{
                self.englishAudioArray.append("http://anaxdesigns.website/quran/english/default/0\(self.current!)/0\(self.current!)-000.mp3")
            }
            else{
                
                self.englishAudioArray.append("http://anaxdesigns.website/quran/english/default/\(self.current!)/\(self.current!)-000.mp3")
            }
            
        }
        //completion()
        
        
        
        //        let url = "http://anaxdesigns.website/quran/?get=verse&lang=english&reader=default&chapter=001&verse=001"
        
    }
    
    func playEnglishAudioFile(_ index: Int)
    {
        // let fileUrl = URL(string: audioArray[index])
        
        //        do
        //        {
        //
        //            action = try AVAudioPlayer(contentsOf: URL(string: audioArray[index])!)
        //            action.delegate = self
        //            action.numberOfLoops = 0
        //            action.prepareToPlay()
        //            action.volume = 1
        //            action.play()
        //        }
        //        catch{print("error")}
        print(audioCount)
        
        if  self.fromVC.elementsEqual("Surat"){
            print(ayatCount)
            
            
        }
        else{
            let sumedArr = ArrSurah.reduce(0, {$0 + $1})
            
            ayatCount = sumedArr
        }
        let new = englishAudioArray.sorted()
        
        
        print(englishAudioArray)
        print(new)
        
        if self.current! == 1{
            
            if audioCount < ayatCount  {
                let Dict = NSURL(string: new[index])
                print(Dict as Any)
                let playerItem =  AVPlayerItem(url: Dict! as URL)
                print(playerItem)
                audioPlayer = AVPlayer(playerItem: playerItem)
                audioPlayer.volume = 100.0
            let playImage = UIImage(systemName: "play.fill")
            self.playPauseBtn.setImage(playImage, for: .normal)
                audioPlayer.play()
                print(audioCount)
            }
            else{
//               let playImage = UIImage(systemName: "play.fill")
               self.playPauseBtn.setImage(pauseImage, for: .normal)
                audioPlayer.pause()
                audioCount = 0
                isPlayOn = false
            }
            //            let playImage = UIImage(systemName: "play.fill")
            //                       self.playPauseBtn.setImage(playImage, for: .normal)
            //                       audioPlayer.pause()
            //                       audioCount = 0
            //                       isPlayOn = false
        }
        else if self.current! != 1{
            if audioCount <= ayatCount  {
                let Dict = NSURL(string: new[index])
                print(Dict as Any)
                let playerItem =  AVPlayerItem(url: Dict! as URL)
                print(playerItem)
                audioPlayer = AVPlayer(playerItem: playerItem)
                audioPlayer.volume = 100.0
                self.playPauseBtn.setImage(playImage, for: .normal)
                
                audioPlayer.play()
                print(audioCount)
            }
            else{
//                let playImage = UIImage(systemName: "play.fill")
                self.playPauseBtn.setImage(pauseImage, for: .normal)
                audioPlayer.pause()
                audioCount = 0
                isPlayOn = false
            }
            //            let playImage = UIImage(systemName: "play.fill")
            //                       self.playPauseBtn.setImage(playImage, for: .normal)
            //                       audioPlayer.pause()
            //                       audioCount = 0
            //                       isPlayOn = false
            //
        }
        
        //        else{
        
        
        //        }
        
        
    }
    
    func playSipara(index: Int){
        let sumedArr = ArrSurah.reduce(0, {$0 + $1})
        print(sumedArr)
        ayatCount = sumedArr
        audioCount = index
        let new = siparaArrArabic.sorted()
        
        if audioCount < ayatCount  {
            let Dict = NSURL(string: new[index])
            let playerItem =  AVPlayerItem(url: Dict! as URL)
            print(playerItem)
            audioPlayer = AVPlayer(playerItem: playerItem)
            audioPlayer.volume = 100.0
            self.playPauseBtn.setImage(playImage, for: .normal)
            
            audioPlayer.play()
            
            let indexPath = NSIndexPath(item: audioCount, section: 0)
                           
            tblSurat.selectRow(at: indexPath as IndexPath, animated: true, scrollPosition: .bottom)
            print(audioCount)
            
        }
        else{
//            let playImage = UIImage(systemName: "play.fill")
            self.playPauseBtn.setImage(pauseImage, for: .normal)
            audioPlayer.pause()
            audioCount = 0
            isPlayOn = false
        }
        
        
        
    }
    func playSiparaEng(index: Int){
        audioCount = index
        let sumedArr = ArrSurah.reduce(0, {$0 + $1})
        print(sumedArr)
        audioCount = index
        ayatCount = sumedArr
        let new = siparaArrEng.sorted()
        
        if audioCount < ayatCount  {
            let Dict = NSURL(string: new[index])
            let playerItem =  AVPlayerItem(url: Dict! as URL)
            print(playerItem)
            audioPlayer = AVPlayer(playerItem: playerItem)
            audioPlayer.volume = 100.0
            self.playPauseBtn.setImage(playImage, for: .normal)
            
            audioPlayer.play()
            
            let indexPath = NSIndexPath(item: audioCount, section: 0)
                           
            tblSurat.selectRow(at: indexPath as IndexPath, animated: true, scrollPosition: .bottom)
            print(audioCount)
            
        }
        else{
//            let playImage = UIImage(systemName: "play.fill")
            self.playPauseBtn.setImage(pauseImage, for: .normal)
            audioPlayer.pause()
            audioCount = 0
            isPlayOn = false
        }
        
        
        
    }
    func playAudioFileSurat(_ index: Int)
    {
        
        print(audioArray)
        
        audioCount = index
        var new = [String]()
        if  self.fromVC.elementsEqual("Surat"){
            print(ayatCount)
            new = urlArray.sorted()
        }
        else{
            let sumedArr = ArrSurah.reduce(0, {$0 + $1})
            
            ayatCount = sumedArr
            new = siparaArrArabic.sorted()
        }
        
        print(siparaArrArabic)
        print(new)
        
        
        if self.current! == 1{
            if audioCount < ayatCount  {
                
                let Dict = NSURL(string: new[index])
                let playerItem =  AVPlayerItem(url: Dict! as URL)
                print(playerItem)
                audioPlayer = AVPlayer(playerItem: playerItem)
                audioPlayer.volume = 300.0
                self.playPauseBtn.setImage(playImage, for: .normal)
                
                audioPlayer.play()
                let indexPath = NSIndexPath(item: audioCount, section: 0)
                
                tblSurat.selectRow(at: indexPath as IndexPath, animated: true, scrollPosition: .bottom)
            
            }
            else{
//                let playImage = UIImage(systemName: "play.fill")
                self.playPauseBtn.setImage(pauseImage, for: .normal)
                audioPlayer.pause()
                audioCount = 0
                isPlayOn = false
            }
        }
        else if self.current! != 1{
            if audioCount <= ayatCount  {
                let Dict = NSURL(string: new[index])
                let playerItem =  AVPlayerItem(url: Dict! as URL)
                print(playerItem)
                audioPlayer = AVPlayer(playerItem: playerItem)
                audioPlayer.volume = 300.0
                self.playPauseBtn.setImage(playImage, for: .normal)
                
                audioPlayer.play()
                print(audioCount)
                
            }
            else{
//                let playImage = UIImage(systemName: "play.fill")
                self.playPauseBtn.setImage(pauseImage, for: .normal)
                audioPlayer.pause()
                audioCount = 0
                isPlayOn = false
            }
            
        }
        
        
        
    }
    
    func playAudioFileEng(_ index: Int)
    {
        
        print(audioArray)
        //         audioCount = index
        
        
        var new = [String]()
        if  self.fromVC.elementsEqual("Surat"){
            print(ayatCount)
            new = englishUrlArray.sorted()
        }
        else{
            let sumedArr = ArrSurah.reduce(0, {$0 + $1})
            
            ayatCount = sumedArr
            new = siparaArrArabic.sorted()
        }
        
        print(siparaArrArabic)
        print(index)
        
        
        if self.current! == 1{
            if audioCount < ayatCount  {
                
                let Dict = NSURL(string: new[index])
                print(Dict)
                let playerItem =  AVPlayerItem(url: Dict! as URL)
                print(playerItem)
                audioPlayer = AVPlayer(playerItem: playerItem)
                audioPlayer.volume = 300.0
                audioPlayer.play()
                self.playPauseBtn.setImage(playImage, for: .normal)
                
                let indexPath = NSIndexPath(item: audioCount, section: 0)
                // autoscroll here!
                tblSurat.selectRow(at: indexPath as IndexPath, animated: true, scrollPosition: .bottom)
                
                print(audioCount)
                
            }
            else{
                let playImage = UIImage(systemName: "play.fill")
                self.playPauseBtn.setImage(pauseImage, for: .normal)
                audioPlayer.pause()
                audioCount = 0
                isPlayOn = false
            }
        }
        else if self.current! != 1{
            if audioCount <= ayatCount  {
                let Dict = NSURL(string: new[index])
                let playerItem =  AVPlayerItem(url: Dict! as URL)
                print(playerItem)
                audioPlayer = AVPlayer(playerItem: playerItem)
                audioPlayer.volume = 300.0
                audioPlayer.play()
                self.playPauseBtn.setImage(playImage, for: .normal)
                
                print(audioCount)
                
            }
            else{
                let playImage = UIImage(systemName: "play.fill")
                self.playPauseBtn.setImage(pauseImage, for: .normal)
                audioPlayer.pause()
                audioCount = 0
                isPlayOn = false
            }
            
        }
        
        
        
    }
    
    
    func playAudioFile(_ index: Int)
    {
        // let fileUrl = URL(string: audioArray[index])
        
        //        do
        //        {
        //
        //            action = try AVAudioPlayer(contentsOf: URL(string: audioArray[index])!)
        //            action.delegate = self
        //            action.numberOfLoops = 0
        //            action.prepareToPlay()
        //            action.volume = 1
        //            action.play()
        //        }
        //        catch{print("error")}
        print(audioCount)
        
        
        
        
        if  self.fromVC.elementsEqual("Surat"){
            print(ayatCount)
            
            
        }
        else{
            let sumedArr = ArrSurah.reduce(0, {$0 + $1})
            print(sumedArr)
            ayatCount = sumedArr
        }
        let new = siparaArrArabic.sorted()
        print(new)
        
        
        if self.current! == 1{
            if audioCount < ayatCount  {
                let Dict = NSURL(string: new[index])
                let playerItem =  AVPlayerItem(url: Dict! as URL)
                print(playerItem)
                audioPlayer = AVPlayer(playerItem: playerItem)
                audioPlayer.volume = 100.0
                audioPlayer.play()
                self.playPauseBtn.setImage(playImage, for: .normal)
                
                print(audioCount)
                
            }
            else{
                let playImage = UIImage(systemName: "play.fill")
                self.playPauseBtn.setImage(pauseImage, for: .normal)
                audioPlayer.pause()
                audioCount = 0
                isPlayOn = false
            }
        }
        else if self.current! != 1{
            if audioCount <= ayatCount  {
                let Dict = NSURL(string: new[index])
                let playerItem =  AVPlayerItem(url: Dict! as URL)
                print(playerItem)
                audioPlayer = AVPlayer(playerItem: playerItem)
                audioPlayer.volume = 300.0
                audioPlayer.play()
                self.playPauseBtn.setImage(playImage, for: .normal)
                
                print(audioCount)
                
            }
            else{
                let playImage = UIImage(systemName: "play.fill")
                self.playPauseBtn.setImage(pauseImage, for: .normal)
                audioPlayer.pause()
                audioCount = 0
                isPlayOn = false
            }
            
        }
        
        
        
    }
    
    
    
    @objc func playerDidFinishPlaying(note: NSNotification) {
        print("Video Finished")
        
        print(audioCount)
        print(urlArray.count)
        print(isPlayArabic)
        if fromVC.elementsEqual("Surat"){
            if isPlayArabic{
                if audioCount <= urlArray.count {
                    
                    audioCount += 1
                    playAudioFileSurat(audioCount)
                    
                }
            }
            
            if isPlayEnglish{
                if audioCount < englishUrlArray.count{
                    audioCount += 1
                    playAudioFileEng(audioCount)
                    //                        playEnglishAudioFile(audioCount)
                }
            }
            else if self.isMulti == true{
                if self.bBtn == true{
                    
                 
                        print(audioCount)
                        print(cArr.count)
                        if isArbPlaying == true{
                            
                            playArabicWithTrans(str: self.cArr[audioCount].strEng)
                            isArbPlaying = false
                        }
                        else{
                            
                            audioCount += 1
                            if audioCount < cArr.count {
                                playArabicWithTrans(str: self.cArr[audioCount].strArb)
                                isArbPlaying = true
                            }
                           
                            else{
                              // let playImage = UIImage(systemName: "play.fill")
                               self.playPauseBtn.setImage(pauseImage, for: .normal)
                               audioPlayer.pause()
                               audioCount = 0
                               isPlayOn = false
                           }
                        }
                        
                        self.isMulti = true
                    
                   
                         
                    
                    
                  
                   
                    
                }
                else{
                    
                    if selectedInd < cArr.count{
                        print(cArr)
                        print(selectedInd)
                        
                        if isArbPlaying == true{
                            playMultiple(index: selectedInd + 1, strAyat: cArr[selectedInd] .strEng)
                            isArbPlaying = false
                        }
                        else{
                            
                            playMultiple(index: selectedInd + 1, strAyat: cArr[selectedInd] .strArb)
                            isArbPlaying = true
                            
                        }
                        
                        self.isMulti = true
                        
                    }
                    else{
                       // let playImage = UIImage(systemName: "play.fill")
                        self.playPauseBtn.setImage(pauseImage, for: .normal)
                        audioPlayer.pause()
                        audioCount = 0
                        isPlayOn = false
                    }
                   
                }
            }
        }
        else{
            if isPlayArabic{
                if audioCount < siparaArrArabic.count{
                    audioCount += 1
                    playSipara(index: audioCount)
                }
            }
            
            if isPlayEnglish{
                if audioCount < siparaArrArabic.count{
                    audioCount += 1
                    playSiparaEng(index: audioCount)
                }
            }
            else if self.isMulti == true{
                //            print(selectedInd )
                
                if isArbPlaying == true{
                    if bBtn == true{
                        print(audioCount)
                        //                                        audioCount += 1
                        playArabicWithTrans(str: self.cArr[audioCount].strEng)
                        isArbPlaying = false
                    }
                    else{
                        playMultiple(index: selectedInd + 1, strAyat: cArr[selectedInd] .strEng)
                        isArbPlaying = false}
                }
                else{
                    if bBtn == true{
                        audioCount += 1
                        playArabicWithTrans(str: self.cArr[audioCount].strArb)
                        isArbPlaying = true
                    }
                    else{
                        playMultiple(index: selectedInd + 1, strAyat: cArr[selectedInd] .strArb)
                        isArbPlaying = true}
                    
                }
                
                self.isMulti = true
                //                playMultiple(index: selectedInd + 1, strAyat: cArr[selectedInd ].strEng)
                
                //                        self.isMulti = false
                
                
            }
            
            
            
            
            
            
            
            
        }
        
        
        //        else{
        //            print("stop")
        //            audioPlayer.pause()
        //        }
        
    }
    
    
    func playArabicWithTrans(str:String){
        print(audioCount)
        //print(cArr)
        print(cArr.count)
        
        if audioCount < cArr.count {
            let Dict = NSURL(string: str)
                   let playerItem =  AVPlayerItem(url: Dict! as URL)
                   print(playerItem)
                   audioPlayer = AVPlayer(playerItem: playerItem)
                   audioPlayer.volume = 100.0
                   audioPlayer.play()
                   self.playPauseBtn.setImage(playImage, for: .normal)
                   
                   let indexPath = NSIndexPath(item: audioCount, section: 0)
                                  
                   tblSurat.selectRow(at: indexPath as IndexPath, animated: true, scrollPosition: .bottom)
                   
                   //                   if isArbPlaying == true{
                   //                       selectedInd = ind
                   //
                   //                   }
                   isMulti = true
        }
        else{
             // let playImage = UIImage(systemName: "play.fill")
              self.playPauseBtn.setImage(pauseImage, for: .normal)
              audioPlayer.pause()
              audioCount = 0
              isPlayOn = false
          }
        
       
        
        
        
        
        
        
        
    }
    
}





//http://anaxdesigns.website/quran/?get=verse&lang=arabic&reader=mishary-rashid-alafasy&chapter=001&verse=001
//
//
//http://anaxdesigns.website/quran/?get=verse&lang=english&reader=default&chapter=001&verse=001
//
//http://anaxdesigns.website/quran/?get=languages
struct CombineArr{
    var strArb:String
    var strEng:String
    
    
    
    
}
