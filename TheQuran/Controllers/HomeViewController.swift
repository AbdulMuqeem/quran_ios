//
//  HomeViewController.swift
//  TheQuran
//
//  Created by engitech on 09/03/2020.
//

import UIKit

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func quranTapButton(_ sender: Any) {
        
        
//        let vc : UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "QuranVC") as ViewController
//
//        vc.modalPresentationStyle = .overFullScreen
//        self.present(vc, animated: true, completion: nil)
        
        performSegue(withIdentifier: "toQuranVC", sender: nil)
        
    }
    
    
    @IBAction func bookmarkTapButton(_ sender: Any) {
        
        performSegue(withIdentifier: "toBookmarkVC", sender: nil)
        
    }
    
    
    
    
}
